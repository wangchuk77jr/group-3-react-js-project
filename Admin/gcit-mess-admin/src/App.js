import React from "react";
import { Login } from "./Pages/Login/Login";
import { Home } from "./Pages/Home/Home";
import WelcomePage from "./Pages/Welcome/WelcomePage";
import Feedback from "./Pages/Feedback/Feedback";
import Contact from "./Pages/Contact/Contact";
import About from "./Pages/About/About";
import GroceryItems from "./Pages/GroceryItems/GroceryItems";
import MenuPlan from "./Pages/MenuPlan/MenuPlan";
import Stipend from "./Pages/Stipend/Stipend";
import CleaningSchedule from "./Pages/CleaningSchedule/CleaningSchedule";
import Announcement from "./Pages/Home/Announcement";
import CurryMenu from "./Pages/Home/CurryMenu";
import { BrowserRouter,Routes,Route } from "react-router-dom";
function App() {
  return (
    <div>
        <BrowserRouter>
          <Routes>
            <Route exact path="/" Component={Login}/>
            <Route path="/admin/home" Component={Home}/>
            <Route path="/admin/welcome" Component={WelcomePage}/>
            <Route path="/admin/feedback" Component={Feedback}/>
            <Route path="/admin/contact" Component={Contact}/>
            <Route path="/admin/about" Component={About}/>
            <Route path="/admin/grocery-items" Component={GroceryItems}/>
            <Route path="/admin/menu-plan" Component={MenuPlan}/>
            <Route path="/admin/stipend" Component={Stipend}/>
            <Route path="/admin/cleaning-schedule" Component={CleaningSchedule}/>
            <Route path="/admin/home/annoucement" Component={Announcement}/>
            <Route path="/admin/home/curry-menu" Component={CurryMenu}/>
          </Routes>
        </BrowserRouter>
    </div>
  
  );
}

export default App;
