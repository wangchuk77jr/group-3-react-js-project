import React,{useState}from 'react'
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer'
import './Home.css'
import NoData from './No data-rafiki.png'
import { Modal, Button, Form } from 'react-bootstrap';
const Announcement = () => {
    const [showModal, setShowModal] = useState(false);
    const [imageFile, setImageFile] = useState(null);
    const [content, setContent] = useState('');
    const [items, setItems] = useState([]); // Initialize items state to an empty array
  
    const handleModalOpen = () => {
      setShowModal(true);
    };
  
    const handleModalClose = () => {
      setShowModal(false);
    };
  
    const handleImageFileChange = (event) => {
      const file = event.target.files[0];
      setImageFile(file);
    };
  
    const handleContentChange = (event) => {
      setContent(event.target.value);
    };
  
    const handleFormSubmit = (event) => {
      event.preventDefault();
      // Store the submitted data in state
      const newItem = { content, imageUrl: URL.createObjectURL(imageFile) };
      setItems([...items, newItem]); // Add the new item to the items array
      // Close the modal after submitting the form
      handleModalClose();
    };
    const handleDelete = (index) => {
        if (window.confirm("Are you sure you want to delete this item?")) {
          const newItems = [...items];
          newItems.splice(index, 1);
          setItems(newItems);
        }
      };
      
    return (
        <MainWrapperContainer>
          <div className='row border-bottom'>
            <div className="col-12 d-flex align-items-center w-100" style={{justifyContent:'space-between'}}>
            <div className=' d-flex align-items-center' style={{overflowY:'hidden'}}>
              <h5 className='p-2 ms-3 goBack__btn'><a href="/admin/home" style={{overflowY:'hidden'}}><i className="fa-solid fa-angles-left pe-2" style={{position:'relative',top:'3px',overflowY:'hidden'}}></i>Go Back</a></h5>
              <h5 className='p-3  text-uppercase' style={{fontWeight:"800"}}>Add New Annoucement</h5>
            </div>
              <div className='pe-5' style={{justifySelf:'flex-end'}}>
                <button  onClick={handleModalOpen} className='px-2' style={{fontSize:'30px',textDecoration:'none',color:'#fff',background:'#0CA91C',borderRadius:'100px'}}><i class="fa-solid fa-plus" style={{position:'relative',top:'3px'}}></i></button>
              </div>
            </div>
          </div>
          {/* Modal */}
          <Modal show={showModal} onHide={handleModalClose} centered className='p-4'>
            <Modal.Header closeButton>
              <Modal.Title className='text-uppercase h5'>Add Annoucement</Modal.Title>
            </Modal.Header>
            <Modal.Body className='pb-5'>
              <Form onSubmit={handleFormSubmit}>
                <Form.Group>
                  <Form.Label>Descriptions:</Form.Label>
                  <Form.Control style={{border:'1px solid #0CA91C'}} as="textarea" placeholder='Enter the announcement or news descriptions' rows={6} value={content} onChange={handleContentChange} required />
                </Form.Group>
                <Form.Group className='mt-2'>
                  <Form.Label>Select Image:</Form.Label>
                  <Form.Control style={{border:'1px solid #0CA91C'}} type="file" onChange={handleImageFileChange} accept="image/*" required />
                </Form.Group>
                <div className='text-end'>
                <Button className='mt-4 w-50' type="submit" style={{backgroundColor:'#0CA91C'}}>ADD</Button>
                </div>
              </Form>
            </Modal.Body>
          </Modal>
    
          {/* Render all items */}
          <div className="container pe-5">
            {items.length > 0 ? (
                items.map((item, index) => (
                <div key={index} className="row">
                    <div className="col-12 addItmesCard">
                    <div className="row">
                        <div className="col-md-6 p-0" style={{display:'flex',alignItems:'center'}}>
                        <img className='p-0 m-0' src={item.imageUrl} alt="item-img" style={{width:'50%',height:'80px',objectFit:'cover',objectPosition:'center'}}/>
                        <p className='ps-4'>{item.content}</p>
                        </div>
                        <div className="col-md-6" style={{display:'flex',alignItems:'center',justifyContent:'flex-end',gap:'20px'}}>
                        <button className='edit-delete-btn bg-success' onClick={handleModalOpen}><i className="fa-solid fa-pencil"></i></button>
                        <button className='edit-delete-btn bg-danger' onClick={() => handleDelete(index)}><i class="fa-solid fa-trash-can"></i></button>
                        </div>
                    </div>
                    </div>
                </div>
                ))
            ) : (
                <div>
                    <h1 className='text-center mt-3' style={{overflowY:'hidden'}}> No Announcement!</h1>
                    <img src={NoData} alt="nodata" style={{width:'100%',height:"70vh",objectFit:'contain'}} />
                </div>
            )}
            </div>

        </MainWrapperContainer>
      )
    
}

export default Announcement