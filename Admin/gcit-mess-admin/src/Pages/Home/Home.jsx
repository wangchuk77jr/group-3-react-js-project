import React from 'react'
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer'
import './Home.css'
export const Home = () => {
  return (
    <>
    <MainWrapperContainer>
      <div className='row'>
        <div className="col-12">
          <h5 className='p-3 border-bottom text-uppercase' style={{fontWeight:"800"}}>Home Content Management</h5>
        </div>
      </div>
      <div className="row ms-2 mt-4">
        <div className="col-md-4">
          <div className='content-card'>
            <a href="/admin/home/annoucement">Announcement</a>
          </div>
        </div>
        <div className="col-md-4">
          <div className='content-card'>
          <a href="/admin/home/curry-menu">Curry Menu</a>
          </div>
        </div>
      </div>
      <div className="row ms-2 mt-3">
        <div className="col-md-4">
          <div className='content-card'>
            <a href="/annoucement">Meal Timing</a>
          </div>
        </div>
        <div className="col-md-4">
          <div className='content-card'>
            <a href="/annoucement">Rules and regulations</a>
          </div>
        </div>
      </div>
      <div className="row ms-2 mt-3">
        <div className="col-md-4">
          <div className='content-card'>
            <a href="/annoucement">MENU COORDINATION DAY</a>
          </div>
        </div>
      </div>
    
    </MainWrapperContainer>  
    </>
  )
}
