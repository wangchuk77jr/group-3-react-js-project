import React from 'react'
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer'

const About = () => {
  return (
    <MainWrapperContainer>
        <h1>About</h1>
    </MainWrapperContainer>
  )
}

export default About