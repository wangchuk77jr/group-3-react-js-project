import React from 'react'
import './Login.css'
import logo from './gm.png'

export const Login = () => {
  return (
    <div className='LoginContainer'>
        <div className="container login-box" style={{width:'500px'}}>
            <div className="row">
                <div className="col-12">
                    <div className='text-center'>
                        <img src={logo} alt="logo" width={120} height={70} />
                    </div>
                    <form action="/admin/welcome">
                        <div className='input__Container mt-4'>
                            <input type="text" name="" id=""placeholder='User Name' required />
                            <input type="password" name="" id="" placeholder='Password' required />
                             <a href="/forgot_password" style={{textAlign:'end',fontWeight:'400',fontSize:'18px'}}>Forgot Password?</a>
                        </div>
                        <div className='text-center'>
                            <button className='login_button' style={{width:'60%'}}>Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
  )
}
