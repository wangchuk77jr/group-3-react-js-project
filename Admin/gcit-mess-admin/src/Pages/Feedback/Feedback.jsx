import React from 'react'
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer'

const Feedback = () => {
  return (
    <>
     <MainWrapperContainer>
     <div className='row'>
        <div className="col-12">
          <h5 className='p-3 border-bottom text-uppercase' style={{fontWeight:"800"}}>Feedback</h5>
        </div>
      </div>
     </MainWrapperContainer>
    </>
  )
}

export default Feedback