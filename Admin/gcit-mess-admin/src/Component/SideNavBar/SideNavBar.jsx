import React from 'react'
import './navbar.css'
import Logo from '../../Asssets/gm.png'
const SideNavBar = () => {
  return (
    <>
    <div className='position-sticky sticky-top top-0'>
        <ul className='navBar position-fixed'>
             <li className='nav-items'>
                <a href="/admin/welcome" ><img src={Logo} alt="logo" width={120} height={70} /></a>
            </li>
            <li className='nav-items'>
                <a href="/admin/home"><i className="fa-solid fa-house pe-2" style={{color: '#fafafa'}}></i>Home</a>
            </li>
            <li className='nav-items'>
                <a href="/admin/feedback"><i className="fa-solid fa-comment pe-2" style={{color: '#fafafa'}}></i>Feed back</a>
            </li>
            <li className='nav-items'>
                <a href="/admin/contact"><i className="fa-solid fa-phone pe-2" style={{color: '#fafafa'}}></i>contact</a>
            </li>
            <li className='nav-items'>
                <a href="/admin/about"><i className="fa-solid fa-address-card pe-2" style={{color: '#fafafa'}}></i>about</a>
            </li>
            <li className='nav-items'>
                <a href="/admin/grocery-items"><i className="fa-solid fa-bag-shopping pe-2" style={{color: '#fafafa'}}></i>Gcrocery Items</a>
            </li>
            <li className='nav-items'>
                <a href="/admin/menu-plan"><i className="fa-solid fa-bowl-food pe-2" style={{color: '#fafafa'}}></i>menu plan</a>
            </li>
            <li className='nav-items'>
                <a href="/admin/stipend"><i className="fa-solid fa-money-check-dollar pe-2" style={{color: '#fafafa'}}></i>Stipend</a>
            </li>
            <li className='nav-items'>
                <a href="/admin/cleaning-schedule"><i className="fa-solid fa-broom pe-2" style={{color: '#fafafa'}}></i>Cleaning Schedule</a>
            </li>
            <li className='nav-items mt-5 pe-4'>
                <a href="/" id='logoutBtn' style={{width:'100%',border:'2px solid #fff',textAlign:'center',padding:'6px'}}>Log Out</a>
            </li>
        </ul>
    </div>
    </>
  )
}

export default SideNavBar