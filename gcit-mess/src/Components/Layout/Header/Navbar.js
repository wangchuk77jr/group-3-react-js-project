import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../../Assets/Images/gm.png'
import './navbar.css'

const Navbar = () => {
  return (
    <div className='navbar-section'>
      <nav className="navbar navbar-expand-lg">
        <div className="container px-md-5">
          <Link className="navbar-brand" to="/">
             <img src={logo} alt="GCIT MESS Logo" width={150} height={60}/>
          </Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavDropdown">
            <ul className="navbar-nav ms-auto d-md-flex gap-md-2">
              <li className="nav-item hover-effect">
                <Link className="nav-link " to="/menu-plan">Menu Plan</Link>
              </li>
              <li className="nav-item hover-effect">
                <Link className="nav-link" to="/grocery-items">Grocery Item</Link>
              </li>
              <li className="nav-item hover-effect">
                <Link className="nav-link" to="/feedback">Feedback</Link>
              </li>
              <li className="nav-item dropdown">
                <Link className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Others
                </Link>
                <ul className="dropdown-menu">
                  <li><Link className="dropdown-item" to="/contact">Contact</Link></li>
                  <li><Link className="dropdown-item" to="/about">About</Link></li>
                  <li><Link className="dropdown-item" to="/stipend">About Stipend</Link></li>
                  <li><Link className="dropdown-item" to="/cleaning-schedule">Cleaning Schedule</Link></li>
                </ul>
              </li>
              <li className="nav-item text-center nav-link ">
                <Link className="ms-md- login-btn px-4 py-2 " to="/login">Login</Link>
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;