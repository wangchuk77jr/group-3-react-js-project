import React from 'react'
import PageHeading from '../../Common/Page Common/PageHeading'
import './Feedback.css'
import feedback from '../../../Assets/Images/Home/feedback.png'


const Feedback = () => {
  return (
    <div className='container px-md-5'>
      <div className='row'>
        <p style={{width:'50%'}}>
        Provide helpful and constructive feedback on your food experiences.
        Keep on note that provide the feedback according your meal timing 
        and your feedback will recorded in our system.
        </p>
      </div>
      <div className='row' style={{marginBottom:'100px'}}>
          <div className='col-md-4'>
            <div className='tab-btn py-2 text-uppercase w-100 text-center' style={{background:'#FE5C00',color:'#fff',fontWeight:'800'}}>
              Break Fast
            </div>
          </div>
          <div className='col-md-4'>
            <div className='tab-btn py-2 text-uppercase w-100 text-center' style={{fontWeight:'800',border:'2px solid #FE5C00'}}>
              Lunch
            </div>
          </div>
          <div className='col-md-4'>
            <div className='tab-btn py-2 text-uppercase  w-100 text-center' style={{fontWeight:'800',border:'2px solid #FE5C00'}}>
              Dinner
            </div>
          </div>
      </div>
      <div className='row'>
        <PageHeading PageHeading='other message'/>
      </div>
      <div className='row px-md-0' style={{marginBottom:'100px'}}>
      <div className='col-md-7'>
        <div>
          <form className='feedbackform'>
            <div className='w-100'>
               <input placeholder='Subject'   className='w-100 input'/>
            </div>
            <div className='w-100'>
              <textarea rows='8' className='w-100 input' placeholder='Your Message'/>
            </div>
            <div className='w-50'>
                <input type='submit' value='Submit' className='w-100 submit-btn' />
            </div>
          </form>
        </div>
        </div>
        <div className='col-md-5'>
          <img src={feedback} alt='feedback.png' style={{width:'100%',height:'100%',objectFit:'cover'}}/>
        </div>

      </div>
   

    </div>
  )
}

export default Feedback