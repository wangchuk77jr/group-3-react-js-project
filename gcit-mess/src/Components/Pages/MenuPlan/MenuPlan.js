import React from 'react'
import PageHeading from '../../Common/Page Common/PageHeading'
import PageSubText from '../../Common/Page Common/PageSubText'
const MenuPlan = () => {
  const Heading = 'MENU PLAN';
  const  text ="The GCIT mess will make an effort to fulfill the menu plan, but we must first check the availability of vegetables."

  const MenuList = [
    {
      day :'Monday',
      bf :'Fried Rice',
      lunch:'Ama Datse',
      dinnner:'Mixed Vegetables',
    },
    {
      day :'Tuesday',
      bf :'Fried Rice',
      lunch:'Ama Datse',
      dinnner:'Mixed Vegetables',
    },
    {
      day :'Wednesday',
      bf :'Fried Rice',
      lunch:'Ama Datse',
      dinnner:'Mixed Vegetables',
    },
    {
      day :'Thursday',
      bf :'Fried Rice',
      lunch:'Ama Datse',
      dinnner:'Mixed Vegetables',
    },
    {
      day :'Friday',
      bf :'Fried Rice',
      lunch:'Ama Datse',
      dinnner:'Mixed Vegetables',
    },
    {
      day :'Saturday',
      bf :'Fried Rice',
      lunch:'Ama Datse',
      dinnner:'Mixed Vegetables',
    },
    {
      day :'Sunday',
      bf :'Fried Rice',
      lunch:'Ama Datse',
      dinnner:'Mixed Vegetables',
    },
  ]
  return (
    <div className='container vh-100 px-md-5 mb-5'>
      <PageHeading PageHeading={Heading}/>
      <PageSubText PageSubText ={text}/>
        <table class="table mt-3 text-center table-bordered" style={{borderColor:'#903604'}}>
            <thead>
              <tr style={{backgroundColor:'#FE8642'}}>
                <th className='text-uppercase text-white' scope="col">Day</th>
                <th className='text-uppercase text-white' scope="col">Break Fast</th>
                <th className='text-uppercase text-white' scope="col">Lunch</th>
                <th className='text-uppercase text-white' scope="col">Dinner</th>
              </tr>
            </thead>
            <tbody>
             
              {MenuList.map((item)=>{
                return(
                  <tr>
                  <td>{item.day}</td>
                  <td>{item.bf}</td>
                  <td>{item.lunch}</td>
                  <td>{item.dinnner}</td>
                  </tr>
                )
              })}
            </tbody>
          </table>
    </div>
  )
}

export default MenuPlan