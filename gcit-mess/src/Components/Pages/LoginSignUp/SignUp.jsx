import React from 'react'
import signUpImage from './SignUp.png'
const SignUp = () => {
  return (
    <div className="container px-md-5">
    <div className="row mb-5" style={{backgroundColor:'#fff'}}>
      <div className="col-md-6">
        <div className='p-5'>
          <h4 style={{textTransform:'uppercase',fontWeight:'900',borderBottom:'2px solid orange',paddingBottom:'10px',paddingTop:'5px'}}>Sign up to GCIT Mess</h4>
          <form action="">
            <div className='input-container mt-3'>
              <label htmlFor="">Name:</label>
              <input required type="text" name='' is='' />
            </div>
            <div className='input-container mt-3'>
            <label htmlFor="">Email:</label>
            <input required type="email" name="" id="" />
            </div>
            <div className='input-container mt-3'>
              <label htmlFor="">Passowrd:</label>
              <input required type="password" name='' is='' />
            </div>
            <div className='input-container mt-3'>
              <label htmlFor="">Confirm Passowrd:</label>
              <input required type="password" name='' is='' />
            </div>
            <div className='mt-3 text-center' >
              <button style={{width:'60%',height:'40px',borderRadius:"100px",backgroundColor:'#FC9845',border:'none',color:'#fff',fontWeight:'900'}}>Sign Up</button>
            </div>
            <div className='text-center mt-3'>
              <h6>Already have an account? <span><a href="/login">Sign In</a></span></h6>
            </div>
          
          </form>
        </div>
      </div>
      <div className="col-md-6">
        <img src={signUpImage} alt="" style={{width:'100%',height:'100%',objectFit:'contain',objectPosition:'center'}} />
      </div>
    </div>
  </div>

  )
}

export default SignUp