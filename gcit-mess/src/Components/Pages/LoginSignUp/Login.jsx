import React from 'react'
import './login.css'
import loginImage from './login.png'

const Login = () => {
  return (
    <div className="container px-md-5">
      <div className="row mb-5" style={{backgroundColor:'#fff'}}>
        <div className="col-md-6">
          <div className='p-5'>
            <h4 style={{textTransform:'uppercase',fontWeight:'900',borderBottom:'2px solid orange',paddingBottom:'10px',paddingTop:'5px'}}>Login to GCIT Mess</h4>
            <form action="">
              <div className='input-container mt-4'>
              <label htmlFor="">Email:</label>
              <input required type="email" name="" id="" />
              </div>
              <div className='input-container mt-3 mb-2'>
                <label htmlFor="">Passowrd:</label>
                <input required type="password" name='' is='' />
              </div>
              <div style={{textAlign:'right',marginTop:'10px'}}><h6> <a href="/"> Forgot Password?</a></h6></div>
              <div style={{textAlign:"center",marginTop:'20px'}}>
                <button style={{width:'60%',height:'40px',borderRadius:"100px",backgroundColor:'#FC9845',border:'none',color:'#fff',fontWeight:'900'}}>Login</button>
              </div>
              <div style={{textAlign:"center",marginTop:"30px"}}>
                <h6>Don't have an account? <span><a href="/signup">Sign Up</a></span></h6>
              </div>
            
            </form>
          </div>
        </div>
        <div className="col-md-6">
          <img src={loginImage} alt="" style={{width:'100%',height:'100%',objectFit:'contain',objectPosition:'center'}} />
        </div>
      </div>
    </div>
  
  )
}

export default Login