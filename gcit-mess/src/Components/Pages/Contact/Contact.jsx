import React from 'react'
import PageHeading from '../../Common/Page Common/PageHeading'
import './contact.css'
import call from '../../../Assets/Images/Home/call.png'
import mail from '../../../Assets/Images/Home/email.png'
import arrow from '../../../Assets/Images/Home/right-arrow.png'
import ContactCard from './ContactCard'
import pemayanzom from '../../../Assets/Images/Home/pema yangzom.jpg'
import tashilhamo from '../../../Assets/Images/Home/tashi lhamo.jpg'
import karmachoda from '../../../Assets/Images/Home/karma choda.jpg'
import sonamyangki from '../../../Assets/Images/Home/sonam yangki.jpg'
import sonamwangchuk1 from '../../../Assets/Images/Home/sonamwangcuk1.jpg'
import sonamwangchuk2 from '../../../Assets/Images/Home/sonam.png'

const ContactDetails = [
  {
    image :sonamwangchuk2,
    name:'Sonam Wangchuk',
    p_number:17227642,
    email:'12210021.gcit@rub.edu.bt',
  },
  {
    image :pemayanzom,
    name:'Sonam Wangchuk',
    p_number:17227642,
    email:'12210021.gcit@rub.edu.bt',
  },
  {
    image :karmachoda,
    name:'Sonam Wangchuk',
    p_number:17227642,
    email:'12210021.gcit@rub.edu.bt',
  },
  {
    image :sonamyangki,
    name:'Sonam Wangchuk',
    p_number:17227642,
    email:'12210021.gcit@rub.edu.bt',
  },
  {
    image :tashilhamo,
    name:'Sonam Wangchuk',
    p_number:17227642,
    email:'12210021.gcit@rub.edu.bt',
  },
  {
    image :sonamwangchuk1,
    name:'Sonam Wangchuk',
    p_number:17227642,
    email:'12210021.gcit@rub.edu.bt',
  },
  {
    image :sonamwangchuk2,
    name:'Sonam Wangchuk',
    p_number:17227642,
    email:'12210021.gcit@rub.edu.bt',
  },
  {
    image :sonamwangchuk2,
    name:'Sonam Wangchuk',
    p_number:17227642,
    email:'12210021.gcit@rub.edu.bt',
  }
]

const Contact = () => {
  return (
    <div className='container px-md-5 mb-5 pb-5'>
      <div className="row text-center">
        <div className='text-center'>
          <PageHeading PageHeading='get in touch with us'/>
        </div>
        <div className="col-md-6">
            <div className="row">
              <div className="col-12 message">
                 <div className='call-icon'>
                <img src={call} alt='call'/>
                 </div>
                 <div className='arrow'><img src={arrow} alt='arrow' width={100}/></div>
                 <div className='ms-5' style={{display:'flex',flexDirection:'column',alignItems:"flex-start",justifyContent:"center"}}>
                  <h6>Call Us </h6>
                  <p>+975 17724323</p>
                 </div>
              </div>
              <div className="col-12 mt-3 call">
                <div className='mail-icon'>
                <img src={mail} alt='mail'/>
                </div>
                <div className='arrow'><img src={arrow} alt='arrow' width={100}/></div>
                <div className='ms-5' style={{display:'flex',flexDirection:'column',alignItems:"flex-start",justifyContent:"center"}}>
                  <h6>Message Us </h6>
                  <p>gcitmess.gcit@rub.edu.bt</p>
                </div>
              </div>
            </div>
        </div>
        <div className="col-md-6 ">
          <div className='contact-left-side'>
            <div className="row">
              <div className="col-6">
                 <img src='https://www.gcit.edu.bt/wp-content/uploads/sites/4/2023/03/Non-Acad_Sangay-Lhaden.jpg' alt='' style={{width:'100%',height:'317px',objectFit:'cover',}}/>
              </div>
              <div className="col-6 pt-5">
                <div style={{display:'flex',flexDirection:'column',alignItems:"flex-start",justifyContent:"center"}}>
                <h5 className='text-capitalize' style={{fontWeight:900,color:'#000',fontKerning:"revert-layer",}}>Meet our in-charge:</h5>
                  <h6>Ms. Sangay Lhaden</h6>
                  <h6>Phone No: 17337527</h6>
                  <h6>Email: sangay.gcit@rub.edu.bt</h6>
                </div>
              </div>
            </div>

          </div>

        </div>
      </div>
      <div className="row"><PageHeading PageHeading='Meet our coordinators:'/></div>
      <div className="row px-2 mb-5">
        {ContactDetails.map((item) =>{
          return(
            <ContactCard key={item.p_number} profile_img={item.image} pnumber={item.p_number} name={item.name} email={item.email}/>
          )
        })}
      </div>
    </div>
  )
}

export default Contact