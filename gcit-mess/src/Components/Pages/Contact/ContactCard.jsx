import React from 'react'
import './contact.css'
import call from '../../../Assets/Images/Home/phone_call.png'
import mail from '../../../Assets/Images/Home/mail.png'
import user from '../../../Assets/Images/Home/user.png'

const ContactCard = ({profile_img,pnumber,email,name}) => {
  return (
    <>
        <div className='col-md-3 p-1'>
            <div className='item-card mt-1'>
                <img src={profile_img} alt='items-img' style={{width:'100%',objectFit:'cover',height:'250px'}}/>
                <div className='p-4' style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'start'}}>
                    <p className='items-list pt-1'><span><img src={user} alt="call" width={20} height={20} /></span><span className='ps-2'>{name}</span></p>
                    <p className='items-list pt-2'><img src={call} alt="mail" width={20} height={20} /> <span></span><span className='ps-2'>{pnumber}</span></p>
                    <p className='items-list pt-2'><img src={mail} alt="user" width={20} height={20} /><span></span><span className='ps-2'>{email}</span></p>
                </div>
            </div>
        </div>
    </>
  )
}

export default ContactCard