import React from 'react'
import PageHeading from '../../Common/Page Common/PageHeading'
import CleaningBox from './CleaningBox'
import './Cleaning.css'

const CleaningSchedule = () => {
  const cleaningTime = [
    {
      time:'coming',
      date:'26/03/23',
      class:'BSc.IT',
      year:3,
    },
    {
      time:'next',
      date:'26/03/23',
      class:'SOC1',
      year:3,
    },
    {
      time:'after next',
      date:'26/03/23',
      class:'BSc.CS',
      year:3,
    }
  ]
  return (
    <div className='container px-md-5 vh-100'>
      <div className='row'>
          <PageHeading PageHeading='DINNING CLEANING SCHEDULE'/>
          <p style={{width:"50%",paddingTop:'10px',lineHeight:'30px'}}>
          Be sure to clean the dining hall once a week in accordance with the dining 
          cleaning schedule. The dining hall will need to be cleaned twice as a 
          punishment for everyone who doesn't clean.
          </p>
      </div>
      <div className='row'>
          {cleaningTime.map((c)=>{
            return(
              <CleaningBox time={c.time} date ={c.date} Class={c.class} year={c.year}/>
            )
          })}
      
      </div>
    </div>
  )
}

export default CleaningSchedule