import React from 'react'
import ItemsCard from './ItemsCard'
import PageHeading from '../../Common/Page Common/PageHeading'
import PageSubText from '../../Common/Page Common/PageSubText'

const GroceryItem = () => {
  const SubText = "Every grocery item purchased for this month is available with its information. You can speak with the product's provider for more information to ensure openness.";
  const GroceryItems = [
    {
      id :1,
      item_img:'https://images.pexels.com/photos/2255920/pexels-photo-2255920.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2',
      Item_name:'Chilly',
      Amount:20,
      Total_Money:1500,
      Supplier_Name:'Sonam Tenzin',
      Phone_No:17337143,
      date:'25/04/23',
    },
    {
      id :2,
      item_img:'https://www.thespruce.com/thmb/Y-hs5FxVMUt13P5BlGznUBJjUzk=/5750x0/filters:no_upscale():max_bytes(150000):strip_icc()/top-tomato-growing-tips-1402587-11-c6d6161716fd448fbca41715bbffb1d9.jpg',
      Item_name:'Chilly',
      Amount:20,
      Total_Money:1500,
      Supplier_Name:'Sonam Tenzin',
      Phone_No:17337143,
      date:'25/04/23',
    },
    {
      id :3,
      item_img:'https://healthyfamilyproject.com/wp-content/uploads/2020/05/Cabbage-background.jpg',
      Item_name:'Chilly',
      Amount:20,
      Total_Money:1500,
      Supplier_Name:'Sonam Tenzin',
      Phone_No:17337143,
      date:'25/04/23',
    },
    {
      id :4,
      item_img:'https://images.pexels.com/photos/11287047/pexels-photo-11287047.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2',
      Item_name:'Chilly',
      Amount:20,
      Total_Money:1500,
      Supplier_Name:'Sonam Tenzin',
      Phone_No:17337143,
      date:'25/04/23',
    },
    {
      id :5,
      item_img:'https://images.pexels.com/photos/7511862/pexels-photo-7511862.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2',
      Item_name:'Chilly',
      Amount:20,
      Total_Money:1500,
      Supplier_Name:'Sonam Tenzin',
      Phone_No:17337143,
      date:'25/04/23',
    },
    {
      id :6,
      item_img:'https://www.deccanherald.com/sites/dh/files/article_images/2018/05/18/chilli.jpg',
      Item_name:'Chilly',
      Amount:20,
      Total_Money:1500,
      Supplier_Name:'Sonam Tenzin',
      Phone_No:17337143,
      date:'25/04/23',
    },
    {
      id :7,
      item_img:'https://images.pexels.com/photos/3650647/pexels-photo-3650647.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2',
      Item_name:'Chilly',
      Amount:20,
      Total_Money:1500,
      Supplier_Name:'Sonam Tenzin',
      Phone_No:17337143,
      date:'25/04/23',
    },
    {
      id :8,
      item_img:'https://images.pexels.com/photos/1093837/pexels-photo-1093837.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=2',
      Item_name:'Chilly',
      Amount:20,
      Total_Money:1500,
      Supplier_Name:'Sonam Tenzin',
      Phone_No:17337143,
      date:'25/04/23',
    },
  ]
  return (
    <div className='container px-md-5 mb-5'>
      <PageHeading PageHeading='Grocery Items'/>
      <PageSubText PageSubText={SubText} />
      <div className='row'>
      {GroceryItems.map((item,id)=>{
        return(
          <ItemsCard key={id.id} 
           item_img={item.item_img}
           Item_name={item.Item_name} 
           Amount={item.Amount} 
           Total_Money={item.Total_Money} 
           Supplier_Name ={item.Supplier_Name}
           Phone_No ={item.Phone_No}
           date ={item.date} />
        )

      })}
   
      </div>

    </div>
  
  )
}

export default GroceryItem