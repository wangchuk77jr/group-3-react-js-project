import React from 'react'
import PageHeading from '../../Common/Page Common/PageHeading'

const About = () => {
  return (
    <div className='container px-md-5'>
    <div className="row">
     <div className="col-md-6">
       <PageHeading PageHeading='About gcit mess'/>
       <p className='pt-2' style={{width:'70%',lineHeight:'33px'}}>
         Our mission is to provide high-quality food
         and dining services to the students of 
         Gyalposhing College of Technology. 
         We strive to create a welcoming and 
         inclusive environment that promotes
         healthy eating habits and fosters 
         social interaction among our community 
         members.
       </p>
       <p className='mt-2' style={{width:'70%',lineHeight:'33px'}}>
       Our team of skilled cooks are dedicated 
       to preparing each dish with care and 
       attention to detail, and to continually 
       improving and expanding our 
       food quality.
       </p>
     </div>
     <div className="col-md-6">
       <img className='mt-2' src='https://lh3.googleusercontent.com/p/AF1QipPoy9RYnTrXZ1yv3yetpPQvpLcXuxipl5LMIhiS=s1360-w1360-h1020' alt='gcit-mess' style={{width:"100%" ,height:'400px',objectFit:'cover'}}/>
     </div>
    </div>
    <div className="row">
     <div className='py-3'> <PageHeading PageHeading='MEET OUR MESS COMMITTEE MEMBERS'/></div>
       <div className="col-md-3">
         <div className='bg-success'>
            <img src='' alt='profile' style={{width:'100%',height:'250px',objectFit:"cover"}}/>
         </div>
       </div>
       <div className="col-md-3">
       <div className='bg-success'>
         <img src='' alt='profile' style={{width:'100%',height:'250px',objectFit:"cover"}}/>
         </div>
       </div>
       <div className="col-md-3">
       <div className='bg-success'>
         <img src='' alt='profile' style={{width:'100%',height:'250px',objectFit:"cover"}}/>
         </div>
       </div>
       <div className="col-md-3">
       <div className='bg-success'>
           <img src='' alt='profile' style={{width:'100%',height:'250px',objectFit:"cover"}}/>
         </div>
       </div>
    </div>
    <div className="row mb-5 pb-5">
     <div className='py-3'> <PageHeading PageHeading='MEET OUR SKILLED COOKS'/></div>
       <div className="col-md-3">
         <div className='bg-success'>
            <img src='' alt='profile' style={{width:'100%',height:'250px',objectFit:"cover"}}/>
         </div>
       </div>
       <div className="col-md-3">
       <div className='bg-success'>
         <img src='' alt='profile' style={{width:'100%',height:'250px',objectFit:"cover"}}/>
         </div>
       </div>
       <div className="col-md-3">
       <div className='bg-success'>
         <img src='' alt='profile' style={{width:'100%',height:'250px',objectFit:"cover"}}/>
         </div>
       </div>
       <div className="col-md-3">
       <div className='bg-success'>
           <img src='' alt='profile' style={{width:'100%',height:'250px',objectFit:"cover"}}/>
         </div>
       </div>
    </div>
 </div>
  )
}

export default About