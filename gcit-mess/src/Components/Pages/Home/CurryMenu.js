import React,{useEffect} from 'react'
import './CurryMenu.css'
import CurryMenuCard from '../../Common/CurryMenuCard'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRightLong, faLeftLong } from '@fortawesome/free-solid-svg-icons'

const CurryMenu = () => {
  useEffect(() => {
    const $progressBar = document.querySelector('.progress-bar');
    const $prevBtn = document.querySelector('.prev-btn');
    const $nextBtn = document.querySelector('.next-btn');
    const $cols = document.querySelectorAll('.col-md-3');
    const totalCols = $cols.length;
    let currentCol = 1;
    const colWidth = 25;

    function updateProgressBar() {
      const percent = currentCol * colWidth;
      $progressBar.style.width = `${percent}%`;
      $progressBar.setAttribute('aria-valuenow', percent);
    }

    function toggleButtons() {
      if (currentCol === 1) {
        $prevBtn.disabled = true;
      } else {
        $prevBtn.disabled = false;
      }

      if (currentCol === totalCols) {
        $nextBtn.disabled = true;
      } else {
        $nextBtn.disabled = false;
      }
    }

    function showCols() {
      $cols.forEach((col) => {
        col.style.display = 'none';
      });

      const startCol = (currentCol - 1) * 4;
      const endCol = startCol + 4;
      for (let i = startCol; i < endCol; i++) {
        if ($cols[i]) {
          $cols[i].style.display = 'block';
        }
      }
    }

    $prevBtn.addEventListener('click', () => {
      if (currentCol > 1) {
        currentCol--;
        updateProgressBar();
        toggleButtons();
        showCols();
      }
    });

    $nextBtn.addEventListener('click', () => {
      if (currentCol < totalCols) {
        currentCol++;
        updateProgressBar();
        toggleButtons();
        showCols();
      }
    });

    toggleButtons();
    showCols();
  }, []);
  return (
    <>  
        <div className='container px-md-5'>
            <div className='row'>
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/man-holding-copper-pan-sauteed-chicken-with-green-red-bell-peppers_141793-2316.jpg?w=1380&t=st=1682028764~exp=1682029364~hmac=9559f57e1a53156f87d0f2431b3f932c57129f2e3e9fe495266cbd01733a583b'/> 
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/indian-butter-chicken-black-bowl-black-background_123827-20757.jpg?w=1380&t=st=1682030575~exp=1682031175~hmac=c5f76a5e9488680d73520709124ac67aad82c7cadbedd12bcf29f14904c02438'/> 
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/stir-frying-beef-with-sweet-peppers-green-beans-rice_2829-20120.jpg?w=1380&t=st=1682030589~exp=1682031189~hmac=995f92e31804913a1f7474718d2c57ed3a9d13deddf41851dba843b40a308f66'/>
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/high-angle-indian-food-arrangement_23-2148734737.jpg?w=1380&t=st=1682030626~exp=1682031226~hmac=fbb792465b39e657fd958f72051cdaf5fd7cabdb94420e112cf7e16ddeeb0c12'/>
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/pork-green-curry-brown-bowl-with-spices-black-cement-background_1150-35206.jpg?w=1380&t=st=1682030638~exp=1682031238~hmac=295be159065004544d854644ae1c29b7a4683227b9f1170eca6c7d72ead7316f'/>
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/massaman-curry-frying-pan-with-spices-cement-floor_1150-20781.jpg?w=1380&t=st=1682030654~exp=1682031254~hmac=9a7e558027dc4ca85e50a129bd788e0bb1259a39df5123b6805120ab96c72f1a'/>
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/juicy-piece-meat-wooden-board-generative-ai_169016-28803.jpg?w=1480&t=st=1682030681~exp=1682031281~hmac=036f1c08a4bc2ed4b70d9286ce174f931c91bcad2218e53fc0b81d8cfbed6574'/>
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/man-holding-copper-pan-sauteed-chicken-with-green-red-bell-peppers_141793-2316.jpg?w=1380&t=st=1682028764~exp=1682029364~hmac=9559f57e1a53156f87d0f2431b3f932c57129f2e3e9fe495266cbd01733a583b'/> 
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/man-holding-copper-pan-sauteed-chicken-with-green-red-bell-peppers_141793-2316.jpg?w=1380&t=st=1682028764~exp=1682029364~hmac=9559f57e1a53156f87d0f2431b3f932c57129f2e3e9fe495266cbd01733a583b'/> 
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/indian-butter-chicken-black-bowl-black-background_123827-20757.jpg?w=1380&t=st=1682030575~exp=1682031175~hmac=c5f76a5e9488680d73520709124ac67aad82c7cadbedd12bcf29f14904c02438'/> 
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/stir-frying-beef-with-sweet-peppers-green-beans-rice_2829-20120.jpg?w=1380&t=st=1682030589~exp=1682031189~hmac=995f92e31804913a1f7474718d2c57ed3a9d13deddf41851dba843b40a308f66'/>
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/high-angle-indian-food-arrangement_23-2148734737.jpg?w=1380&t=st=1682030626~exp=1682031226~hmac=fbb792465b39e657fd958f72051cdaf5fd7cabdb94420e112cf7e16ddeeb0c12'/>
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/pork-green-curry-brown-bowl-with-spices-black-cement-background_1150-35206.jpg?w=1380&t=st=1682030638~exp=1682031238~hmac=295be159065004544d854644ae1c29b7a4683227b9f1170eca6c7d72ead7316f'/>
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/massaman-curry-frying-pan-with-spices-cement-floor_1150-20781.jpg?w=1380&t=st=1682030654~exp=1682031254~hmac=9a7e558027dc4ca85e50a129bd788e0bb1259a39df5123b6805120ab96c72f1a'/>
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/juicy-piece-meat-wooden-board-generative-ai_169016-28803.jpg?w=1480&t=st=1682030681~exp=1682031281~hmac=036f1c08a4bc2ed4b70d9286ce174f931c91bcad2218e53fc0b81d8cfbed6574'/>
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/man-holding-copper-pan-sauteed-chicken-with-green-red-bell-peppers_141793-2316.jpg?w=1380&t=st=1682028764~exp=1682029364~hmac=9559f57e1a53156f87d0f2431b3f932c57129f2e3e9fe495266cbd01733a583b'/> 
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/pork-green-curry-brown-bowl-with-spices-black-cement-background_1150-35206.jpg?w=1380&t=st=1682030638~exp=1682031238~hmac=295be159065004544d854644ae1c29b7a4683227b9f1170eca6c7d72ead7316f'/>
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/massaman-curry-frying-pan-with-spices-cement-floor_1150-20781.jpg?w=1380&t=st=1682030654~exp=1682031254~hmac=9a7e558027dc4ca85e50a129bd788e0bb1259a39df5123b6805120ab96c72f1a'/>
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/juicy-piece-meat-wooden-board-generative-ai_169016-28803.jpg?w=1480&t=st=1682030681~exp=1682031281~hmac=036f1c08a4bc2ed4b70d9286ce174f931c91bcad2218e53fc0b81d8cfbed6574'/>
              <CurryMenuCard  imgLink='https://img.freepik.com/free-photo/man-holding-copper-pan-sauteed-chicken-with-green-red-bell-peppers_141793-2316.jpg?w=1380&t=st=1682028764~exp=1682029364~hmac=9559f57e1a53156f87d0f2431b3f932c57129f2e3e9fe495266cbd01733a583b'/> 
              

            </div>
            <div className="progress" style={{height:'10px',borderRadius:'2px'}}>
              <div className="progress-bar" role="progressbar" aria-valuenow="25" aria-valuemin="0" aria-valuemax="100" style={{width: '25%',backgroundColor:'#000'}}></div>
            </div>
            <div className="controls d-flex gap-5 justify-content-center">
              <div className="prev-btn"><FontAwesomeIcon icon={faLeftLong} /></div>
              <div className="next-btn"><FontAwesomeIcon icon={faRightLong} /></div>
            </div>
        </div>
    </>
  )
}

export default CurryMenu