import React from 'react'
import Heading from '../../Common/Heading'
import MenuCoordinationCard from '../../Common/MenuCoordinationCard'
import pemayanzom from '../../../Assets/Images/Home/pema yangzom.jpg'
import tashilhamo from '../../../Assets/Images/Home/tashi lhamo.jpg'
import karmachoda from '../../../Assets/Images/Home/karma choda.jpg'
import sonamyangki from '../../../Assets/Images/Home/sonam yangki.jpg'
import sonamwangchuk1 from '../../../Assets/Images/Home/sonamwangcuk1.jpg'
import sonamwangchuk2 from '../../../Assets/Images/Home/sonam.png'
import './MenuCoordination.css'

const MenuCoordination = () => {
  const CoordinationDay = [
    {
      profile_img: pemayanzom,
      day:'Monday',
      name:'Pema Yanzom',

    },
    {
      profile_img:tashilhamo,
      day:'Tuesday',
      name:'Tashi Lhamo',

    },
    {
      profile_img:karmachoda,
      day:'Wednesday',
      name:'Karma Choeda',

    },
    {
      profile_img:sonamyangki,
      day:'Thursday',
      name:'Sonam yangki',

    },
    {
      profile_img:sonamyangki,
      day:'Friday',
      name:'Pema',

    },
    {
      profile_img:sonamwangchuk1,
      day:'Saturday',
      name:'Sonam Wangchuk(Year 1)',

    },
    {
      profile_img:sonamwangchuk2,
      day:'Sunday',
      name:'Sonam Wancghuk(Year 3)',

    },
    {
      profile_img:karmachoda,
      day:'Monday',
      name:'Pema Yanzom',

    },
  ]

  return (
    <>
    <div className='container px-md-5'>
      <Heading title='Menu Coordination day'/>
        <div className='row pt-3'>
            <p  style={{width:'85%',lineHeight:'30px'}}>
        The Menu Coordination Day at GCIT's mess is a day of delicious 
        planning and organizing! The team in charge ensures a balanced and
        nutritious menu, taking into consideration dietary needs and preferences.
        They coordinate with suppliers to bring healthy and delicious food options
        that cater to the students' tastes. This day ensures that students have access
        to a variety of mouth-watering food options that meet their dietary needs and preferences.
            </p>
        </div>
        <div class="row card-container px-2">  
          {
            CoordinationDay.map((cd) => {
              return <MenuCoordinationCard profile_img={cd.profile_img} day={cd.day} name={cd.name} />
            })
          }
        </div>
 
    </div>
    </>
  )
}

export default MenuCoordination