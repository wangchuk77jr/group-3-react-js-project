import React from 'react'
import Heading from '../../Common/Heading'
import RuleCard from '../../Common/RuleCard'

const Rules = () => {
  const rules =[
    {rule_title:'Dress Code',
    rule_content : `All students are required to wear Gho and Kira, the 
    national dress of Bhutan, during weekdays while attending 
    classes or any official school events. This is to promote 
    the country's culture and tradition, and to instill a sense 
    of pride and respect among the students towards their heritage.
    However, during weekends or non-school events, students 
    are allowed to wear casual dress as long as it is appropriate
     and modest. The school administration reserves the right to 
     determine the appropriateness of the clothing and 
    can request students to change if it does not meet the school's dress code policy.
    Any student found violating the dress code policy will be 
    subject to disciplinary action, which may include warning, suspension,
     or expulsion depending on the severity of the violation. It is the 
     responsibility of the students to comply with the dress code policy and
    to present themselves in a manner that upholds the values and standards of the school.
    `},

    {rule_title:'Meal Timing',
    rule_content : `All students  are expected to be present 
    at the designated time for meals, with the mess hall opening 15 minutes before the scheduled
     mealtime.Latecomers will not be served meals after the designated 
     mealtime has ended, and those who miss a meal will not receive an alternative meal or a refund. 
     Mess staff will ensure that all meals are served on time and in a hygienic manner, with any complaints or 
    concerns brought to the attention of the mess manager or in-charge. The mess hall is a common area,
     and all students  must maintain cleanliness and discipline. Any violation of the mess rules may result in disciplinary action or suspension of mess privileges.`},

    {rule_title:'Cleanliness',
    rule_content : `
    After finishing their meal, students must clear their plates and 
    dispose of waste, ensuring that the table and chairs are clean and 
    free from any food particles or stains. Students should avoid putting 
    personal belongings on the table and refrain from playing with food to
    prevent creating a mess. The mess staff is responsible for cleaning the tables,
    chairs, and floors after each meal, and students should report any large or 
    difficult spills to them. Any damage caused to the dining furniture or equipment
    due to negligence or intentional behavior will be the responsibility of the student
    or students involved. By following these rules and regulations, students can contribute 
    to a clean and hygienic dining environment, and ensure a pleasant dining experience for all.`},

    {rule_title:'Mess Cleaning',
    rule_content : `The weekly dining cleaning activity is an important 
    event that takes place on either Saturday or Sunday, where all students 
    are required to participate and rotate the responsibility among different
    class sections. The mess staff will provide the necessary cleaning supplies, 
    instructions, and guidance to maintain hygiene during the cleaning activity, 
    which involves cleaning the dining hall, kitchen, and other common areas used 
    for dining. Any student who fails to attend the cleaning activity will be marked
    absent, and the mess staff will inspect the cleanliness of the dining area and 
    request additional cleaning if necessary. The activity aims to promote a sense 
    of responsibility and discipline among students in maintaining a clean and hygienic dining environment.`},

  ]
  return (
    <>
        <div className='container px-md-5'>
          <Heading title ='RULES AND REGULATIONS'/>
          </div>
          <div className='container px-md-5 pt-3'>
            <div className='row gap-3 px-md-2'>
              {rules.map((rule,index)=>(
                  <RuleCard key={rule.rule_title} number= {Number(`${index}`)+1} ruleTitle={rule.rule_title} ruleContent={rule.rule_content}/>
              ))}
              </div>
        </div>
    </>
  )
}

export default Rules