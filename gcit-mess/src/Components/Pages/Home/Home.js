import React from 'react'
import './home.css'
import HeroSlider from '../../Common/HeroSlider'
import MealTime from './MealTime'
import CurryMenu from './CurryMenu'
import Rules from './Rules'
import MenuCoordination from './MenuCoordination'
import Heading from '../../Common/Heading'

const Home = () => {
  return (
    <>
      <div className='hero-section'>
        <div className="container px-md-5">
           <HeroSlider/>
        </div>
      </div>
      <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#fc9845" fill-opacity="1" d="M0,192L60,181.3C120,171,240,149,360,165.3C480,181,600,235,720,240C840,245,960,203,1080,192C1200,181,1320,203,1380,213.3L1440,224L1440,0L1380,0C1320,0,1200,0,1080,0C960,0,840,0,720,0C600,0,480,0,360,0C240,0,120,0,60,0L0,0Z"></path></svg>
      <div className='meal-time-section'>
         <MealTime/>
      </div>
      <div className='container px-md-5 mb-5'>
        <Heading title='CURRY MENU'/>
      </div>
      <div className='curry-menu-section'>
        <div className='curry-menu-section-content'>
          <CurryMenu/>
        </div>
      </div>
      <div className='rules-and-regulation-section'>
        <Rules/>
      </div>
   
      <div className='Mess-coordination-section'>
          <MenuCoordination/>
      </div>
    </>
  )
}

export default Home