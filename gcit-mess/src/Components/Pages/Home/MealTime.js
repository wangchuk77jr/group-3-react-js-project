import React from 'react'
import MealTimeCard from '../../Common/MealTimeCard'
import Heading from '../../Common/Heading'

const MealTime = () => {
  const BreakfastBg = {
    backgroundImage: 'url("https://img.freepik.com/free-photo/sunset-saucer-caffeine-liquid-hot_1203-5658.jpg?w=1380&t=st=1682024715~exp=1682025315~hmac=9b90e315ea3a9e68878bc707f5876b77cbc1ea4c7d6bf22dcaeec5ea4f65383a")',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    backgroundSize: 'cover', 
  }
  const LunchBg = {
    backgroundImage: 'url("https://img.freepik.com/free-photo/cloud-blue-sky_1232-3108.jpg?w=1380&t=st=1682024872~exp=1682025472~hmac=289f1f0cd36448cb31828174c9ace9140a8e54253282f70a3e5aa7ff532d8108")',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
  }
  const DinnerBg = {
    backgroundImage: 'url("https://img.freepik.com/free-photo/horizontal-view-sunrise-monument-valley-usa_268835-2845.jpg?w=1380&t=st=1682025796~exp=1682026396~hmac=7f32fbcdc8e3e12a91699d30129ce71c1faf3b1bbf5df1770f63b684b8a69aa2")',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    backgroundSize: 'cover',
  }
  return (
    <>
     <div className='container px-md-5'>
          <Heading title ='Meal Timing'/>
           <div class="row pt-3">
            <MealTimeCard timing='6:30 AM TO 8:30 AM' time='Break Fast' bgImg ={BreakfastBg} />
            <MealTimeCard timing='12:00 PM TO 2:00 AM' time='Lunch' bgImg ={LunchBg}/>
            <MealTimeCard timing='6:00 PM TO 8:30 AM' time='Dinner' bgImg ={DinnerBg} />
          </div>
    </div>
    </>
  )
}

export default MealTime