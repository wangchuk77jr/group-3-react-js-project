import React from 'react'
import './Stipend.css'
const PreviousStipend = ({money_used,money_left,money_type1,money_type2,month}) => {
  return (
    <div className='col-md-6'>
      <h6 className='mt-3'>{month}</h6>
        <div className='row'>
          <div className='col-md-6'>
            <div className='CurrentMonthCard' style={{height:'200px'}}>
            <h1 style={{fontSize:'50px'}}>{money_used}</h1>
            <h6 style={{fontSize:'17px'}}>{money_type1}</h6>
            </div>
        </div>
        <div className='col-md-6'>
            <div className='CurrentMonthCard' style={{height:'200px'}}>
            <h1 style={{fontSize:'50px'}}>{money_left}</h1>
            <h6 style={{fontSize:'17px'}}>{money_type2}</h6>
            </div>
        </div>
      </div>
    </div>
  )
}

export default PreviousStipend