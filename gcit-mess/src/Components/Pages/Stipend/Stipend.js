import React from 'react'
import PageHeading from '../../Common/Page Common/PageHeading'
import CurrentStipendCard from './CurrentStipendCard'
import PreviousStipend from './PreviousStipend'

const Stipend = () => {
  const currentStipend = [
    {
      money : '57,000',
      money_type:'Total Money used'
    },
    {
      money : '34,000',
      money_type:'Total Money Left'
    }
  ]
  const PreviousMoney =[
    {
      month :"Month: December 2023",
      money_used : '57,000',
      money_type1:'Total Money used',
      money_left:'32,4343',
      money_type2:'Total money left'
    },
    {
      month :"Month: October 2023",
      money_used : '57,000',
      money_type1:'Total Money used',
      money_left:'32,4343',
      money_type2:'Total money left'
    },
      {
      month :"Month: January 2023",
      money_used : '57,000',
      money_type1:'Total Money used',
      money_left:'32,4343',
      money_type2:'Total money left'
    },
  ]
  return (
    <div className='container px-md-5'>
    <div className='row'>
      <PageHeading PageHeading='This month Stipend'/>
      <p style={{width:'50%',paddingTop:'10px'}}>
      In order to provide transparency, the total stipend for a month 
      will be shown here, both spent and leftovers.
      </p>
      <h6 className='text-uppercase' style={{fontWeight:'900',color:'#038C11'}}>Month: March 2023</h6>
    </div>
    <div className='row mb-5'>
      {
        currentStipend.map((m)=>{
          return(
            <CurrentStipendCard money={m.money} money_type={m.money_type}/>
          )
        })
      }
    </div>
    <div className='row  mt-5 mb-5'>
    <h6 className='text-uppercase' style={{fontWeight:'900'}}>Previous Month</h6>
    {
        PreviousMoney.map((m)=>{
          return(
            <PreviousStipend month={m.month} money_used={m.money_used} money_left={m.money_left} money_type1={m.money_type1} money_type2={m.money_type2}/>
          )
        })
      }
  
  
    </div>

  </div>
  )
}

export default Stipend