import React from 'react'
import './Stipend.css'
const CurrentStipendCard = ({money,money_type}) => {
  return (
    <div className='col-md-4'>
        <div className='CurrentMonthCard'>
            <h1>{money}</h1>
            <h6>{money_type}</h6>
        </div>
    </div>
  )
}

export default CurrentStipendCard