import React from 'react';
import './MealTimeCard.css'
const MealTimeCard = ({timing,time,bgImg}) => {
  return (
    <div class="col-md-4" >
      <div class="card" style={bgImg}>
        <div class="card-body">
          <div>
            <h5 class="card-title">{timing}</h5>
            <h5>{time}</h5>
          </div>
      </div>
     </div>
  </div>
  )
}

export default MealTimeCard;