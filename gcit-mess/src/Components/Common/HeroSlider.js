import React from 'react'
import './slider.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRightLong, faLeftLong} from '@fortawesome/free-solid-svg-icons'
const HeroSlider = () => {
  return (
    <>
      <div id="myCarousel" className="carousel slide position-relative pt-3"  data-bs-ride="carousel"data-bs-interval="3000">
              {/* <!-- Wrapper for slides --> */}
              <div className="carousel-inner pb-3">
                <div className="carousel-item active">
                  <div className="row">
                    <div className="col-md-6 d-flex align-items-center">
                      <h2 className='hero-heading'>Get Special Meals On <span>Tuesday</span></h2>
                    </div>
                    <div className="col-md-6" style={{height:'55vh'}}>
                    <img className='SliderImage' src="https://masalabox.co.in/wp-content/uploads/2015/05/IMG_18091-1024x768.jpg" alt="Slide 1"  style={{objectFit:'cover',width:'100%',height:'100%',borderRadius:'5px'}}/>
                      {/* <img src="https://www.vhv.rs/dpng/d/397-3970632_thumb-image-goat-curry-hd-png-download.png" alt="Slide 1"  style={{objectFit:'cover',width:'100%',height:'100%',borderRadius:'20px',}}/> */}
                    </div>
                  </div>
                </div>

                <div className="carousel-item">
                  <div className="row">
                    <div className="col-md-6 d-flex align-items-center">
                      <h2 className='hero-heading'>Get Special Meals On <span>Friday</span></h2>
                    </div>
                    <div className="col-md-6" style={{height:'55vh'}}>
                    <img className='SliderImage' src="https://www.topasiatour.com/pic/bhutan/guide/food-etiquette-01.jpg" alt="Slide 1"  style={{objectFit:'cover',width:'100%',height:'100%',borderRadius:'5px'}}/>
                      {/* <img src="https://p7.hiclipart.com/preview/979/767/649/massaman-curry-peking-duck-zhangcha-duck-gomi-duck.jpg" alt="Slide 2" style={{objectFit:'cover',width:'100%',height:'100%',borderRadius:'20px',}}/> */}
                    </div>
                  </div>
                </div>

                <div className="carousel-item">
                  <div className="row">
                    <div className="col-md-6 d-flex align-items-center">
                     <h2 className='hero-heading'>Get Egg's Once In A <br/> <span>Week</span></h2>
                    </div>
                    <div className="col-md-6" style={{height:'55vh'}}>
                    <img className='SliderImage' src="https://cdn.britannica.com/94/151894-050-F72A5317/Brown-eggs.jpg" alt="Slide 1"  style={{objectFit:'cover',width:'100%',height:'100%',borderRadius:'5px'}}/>
                      {/* <img src="https://banner2.cleanpng.com/20180404/bae/kisspng-malabar-indian-cuisine-biryani-south-asian-cuisine-biriyani-5ac4600d29b740.9556220715228190851709.jpg" alt="Slide 3" style={{objectFit:'cover',width:'100%',height:'100%',borderRadius:'20px',}} /> */}
                    </div>
                  </div>
                </div>
              </div>

              {/* <!-- Indicators --> */}
              <ol className="carousel-indicators  mt-5">
              <li className='slider-btn mx-3'>
                  <a className="carousel-control" href="#myCarousel" role="button" data-bs-slide="prev">
                    <FontAwesomeIcon icon={faLeftLong} />
                    <span className="visually-hidden">Previous</span>
                  </a>
                </li>
                <li data-bs-target="#myCarousel" data-bs-slide-to="0" className="active dot"></li>
                <li data-bs-target="#myCarousel" data-bs-slide-to="1" className='dot'></li>
                <li data-bs-target="#myCarousel" data-bs-slide-to="2" className='dot'></li>
                <li className='slider-btn mx-3'>
                  <a className="carousel-control" href="#myCarousel" role="button" data-bs-slide="next">
                    <FontAwesomeIcon icon={faRightLong} />
                    <span className="visually-hidden">Next</span>
                  </a>
                </li>
              </ol>
      </div> 
    </>
  )
}

export default HeroSlider