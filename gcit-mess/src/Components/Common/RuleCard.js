import React, { useState } from 'react';
import './RuleCard.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDoubleDown,faAngleDoubleUp } from '@fortawesome/free-solid-svg-icons';

function RuleCard({number,ruleTitle,ruleContent}) {
  const [isContentVisible, setIsContentVisible] = useState(false);

  const toggleContent = () => {
    setIsContentVisible(!isContentVisible);
  };

  return (
    <div className='card-wrapper'>
    <div className='col-12 rule-card'>
      <div className='number'>
        <h5>{number}.</h5>
        <h5>{ruleTitle}</h5>
      </div>
      <div onClick={toggleContent}>
        {isContentVisible ? <span style={{fontSize:'20px',cursor:'pointer'}}> <FontAwesomeIcon icon={faAngleDoubleUp} /> </span> : <span style={{fontSize:'20px',cursor:'pointer'}}><FontAwesomeIcon icon={faAngleDoubleDown} /></span>}
      </div>
    </div>
    <div style={{ display: isContentVisible ? 'block' : 'none' }}>
            <p style={{lineHeight:'30px',paddingTop:"10px"}}>{ruleContent}</p>
        </div>
    </div>
  );
}

export default RuleCard;
