import React from 'react'

const CurryMenuCard = ({imgLink}) => {
  return (
        <div className="col-md-3 text-center">
            <img src={imgLink} height={200} width={300} style={{borderRadius:'10px',objectFit:'cover'}}/>
        </div>
  )
}

export default CurryMenuCard