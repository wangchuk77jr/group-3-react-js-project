const express = require('express');
const cors = require('cors');
const app = express();
const PORT = 5000;

// Import routes
const announcementsRoutes = require('./routes/announcementsRoutes');
const mealtimingRoutes = require('./routes/mealtimingRoutes');
const rulesRoute = require('./routes/rulesRoute');
const menucoordinationROute = require('./routes/menucoordinations')
const menuRouter = require('./routes/menugalleryRoute');
const menuplanRouter = require('./routes/menuplanRoute');
const ItemInfoRouter = require('./routes/ItemInfo');
const CleaningScheduleRouter = require('./routes/CleaningSchedule');
const StipendRouter = require('./routes/Stipend');
const ComitteeRoute = require('./routes/ContactRoute');
const CooksDetail = require('./routes/CooksRoute');
const authRouter = require('./routes/UserRegistrationRoutes');
const adminLogin = require('./routes/AdminLoginRoute');
const UserFeedback = require('./routes/FeedbackRoute');


// Middleware
app.use(cors());
app.use(express.json());

// Use routes
app.use('/profile',express.static('profile'));
app.use('/uploads', express.static('uploads')); // Serve uploaded images
app.use('/announcements', announcementsRoutes);
app.use('/meal-timings',mealtimingRoutes);
app.use('/rules',rulesRoute);
app.use('/menucoordination',menucoordinationROute);
app.use('/menugallery', menuRouter);
app.use('/menuplan',menuplanRouter);
app.use('/itemInfo',ItemInfoRouter);
app.use('/cleaning-schedule',CleaningScheduleRouter);
app.use('/stipend',StipendRouter);
app.use('/api/contacts',ComitteeRoute);
app.use('/cooks',CooksDetail);
app.use('/auth', authRouter);
app.use('/feedback',UserFeedback)
app.use('/admin', adminLogin);

// Start the server
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}`);
});
