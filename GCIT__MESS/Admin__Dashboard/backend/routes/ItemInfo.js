const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const pool = require('../db');

// File upload configuration
const storage = multer.diskStorage({
  destination: './uploads/',
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const fileExtension = path.extname(file.originalname);
    cb(null, file.fieldname + '-' + uniqueSuffix + fileExtension);
  },
});

const upload = multer({ storage });

// Create item route
router.post('/', upload.single('item_image'), async (req, res) => {
  try {
    const { item_name, amount, total_money, supplier_name, phone_no, date } = req.body;
    const item_image = req.file;

    // Insert the item into the database
    const query = 'INSERT INTO item_info (item_image_url, item_name, amount, total_money, supplier_name, phone_no, date) VALUES ($1, $2, $3, $4, $5, $6, $7) RETURNING *';
    const values = [item_image.filename, item_name, amount, total_money, supplier_name, phone_no, date];

    const result = await pool.query(query, values);
    const createdItem = result.rows[0];

    res.status(201).json(createdItem);
  } catch (error) {
    console.error('Error creating item:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Get all items sorted by date (latest on top)
router.get('/', async (req, res) => {
  try {
    // Retrieve all items from the database and sort by date in descending order
    const query = 'SELECT * FROM item_info ORDER BY date DESC';

    const result = await pool.query(query);
    const items = result.rows;

    res.status(200).json(items);
  } catch (error) {
    console.error('Error retrieving items:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});


// Update an item
router.put('/:id', upload.single('item_image'), async (req, res) => {
  try {
    const { id } = req.params;
    const { item_name, amount, total_money, supplier_name, phone_no, date } = req.body;
    const item_image = req.file;

    // Check if the item with the specified ID exists
    const checkQuery = 'SELECT * FROM item_info WHERE gitems_id = $1';
    const checkResult = await pool.query(checkQuery, [id]);
    const existingItem = checkResult.rows[0];
    if (!existingItem) {
      return res.status(404).json({ error: 'Item not found' });
    }

    // Update the item in the database
    let itemImageFilename = existingItem.item_image_url; // Keep the current item image filename by default

    if (item_image) {
      // If a new item image is provided, update the item image filename
      itemImageFilename = item_image.filename;
    }

    const updateQuery = 'UPDATE item_info SET item_image_url = $1, item_name = $2, amount = $3, total_money = $4, supplier_name = $5, phone_no = $6, date = $7 WHERE gitems_id = $8 RETURNING *';
    const updateValues = [itemImageFilename, item_name, amount, total_money, supplier_name, phone_no, date, id];

    const updateResult = await pool.query(updateQuery, updateValues);
    const updatedItem = updateResult.rows[0];

    res.status(200).json(updatedItem);
  } catch (error) {
    console.error('Error updating item:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Delete an item
router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;

    // Check if the item with the specified ID exists
    const checkQuery = 'SELECT * FROM item_info WHERE gitems_id = $1';
    const checkResult = await pool.query(checkQuery, [id]);
    const existingItem = checkResult.rows[0];
    if (!existingItem) {
      return res.status(404).json({ error: 'Item not found' });
    }

    // Delete the item from the database
    const deleteQuery = 'DELETE FROM item_info WHERE gitems_id = $1';
    await pool.query(deleteQuery, [id]);

    res.status(200).json({ message: 'Item deleted successfully' });
  } catch (error) {
    console.error('Error deleting item:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

module.exports = router;
