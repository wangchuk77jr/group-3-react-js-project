const express = require('express');
const router = express.Router();
const pool = require("../db");

// CREATE - Insert a new record
router.post('/', async (req, res) => {
  try {
    const { meal_choice, meal_time, meal_time_end } = req.body;

    // Check if the number of existing rows is already three
    const rowCountQuery = 'SELECT COUNT(*) FROM mealtiming';
    const rowCountResult = await pool.query(rowCountQuery);
    const rowCount = rowCountResult.rows[0].count;

    if (rowCount >= 3) {
      return res.status(400).json({ error: 'Only three rows can be inserted.' });
    }

    // Proceed with the insertion
    const insertQuery = 'INSERT INTO mealtiming (meal_choice, meal_time, meal_time_end) VALUES ($1, $2, $3) RETURNING *';
    const values = [meal_choice, meal_time, meal_time_end];

    const result = await pool.query(insertQuery, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error inserting record:', error);
    res.status(500).json({ error: 'An error occurred while inserting the record.' });
  }
});

// READ - Get all records
router.get('/', async (req, res) => {
  try {
    const query = "SELECT * FROM mealtiming ORDER BY CASE meal_choice WHEN 'Breakfast' THEN 1 WHEN 'Lunch' THEN 2 WHEN 'Dinner' THEN 3 ELSE 4 END";
    const result = await pool.query(query);
    res.json(result.rows);
  } catch (error) {
    console.error('Error retrieving records:', error);
    res.status(500).json({ error: 'An error occurred while retrieving the records.' });
  }
});

// UPDATE - Update a record
router.put('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const { meal_choice, meal_time, meal_time_end } = req.body;
    const query = 'UPDATE mealtiming SET meal_choice = $1, meal_time = $2, meal_time_end = $3 WHERE meal_id = $4 RETURNING *';
    const values = [meal_choice, meal_time, meal_time_end, id];

    const result = await pool.query(query, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error updating record:', error);
    res.status(500).json({ error: 'An error occurred while updating the record.' });
  }
});

// DELETE - Delete a record
router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const query = 'DELETE FROM mealtiming WHERE meal_id = $1 RETURNING *';
    const values = [id];

    const result = await pool.query(query, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error deleting record:', error);
    res.status(500).json({ error: 'An error occurred while deleting the record.' });
  }
});

module.exports = router;
