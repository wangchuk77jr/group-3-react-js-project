const express = require('express');
const router = express.Router();
const pool = require('../db');
const bcrypt = require('bcrypt');
const jwtGenerator = require("../utils/jwtGenerator");

// User Registration
router.post('/register', async (req, res) => {
  try {
    const { username, email, password } = req.body;
    // Check if user already exists
    const user = await pool.query('SELECT * FROM users WHERE email = $1', [email]);
    if (user.rowCount > 0) {
      return res.status(400).json({ message: 'Email already in use' });
    }

    // Validate email format
    const emailRegex = /^(\d{8})\.gcit@rub\.edu\.bt$/;
    const emailMatches = email.match(emailRegex);
    if (!emailMatches) {
      return res.status(400).json({ message: 'Invalid email format,We accet only GCIT mail' });
    }

    // Check if number digits are the same
    const number = emailMatches[1];
    if (new Set(number).size === 1) {
      return res.status(400).json({ message: 'Number cannot be the same' });
    }

    // Check password strength
    const passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
    if (!passwordRegex.test(password)) {
      return res.status(400).json({ message: 'Invalid password. Must contain at least 8 characters, one lowercase letter, one uppercase letter, one number, and one special character' });
    }

    // Hash the password
    const hashedPassword = await bcrypt.hash(password, 10);

    // Insert user into the database
    await pool.query('INSERT INTO users (username, email, password) VALUES ($1, $2, $3)', [username, email, hashedPassword]);

    res.status(201).json({ message: 'User registered successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
});

// GET user by ID
router.get('/', async (req, res) => {
  try {
    const users = await pool.query('SELECT * FROM users');

    res.status(200).json(users.rows);
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
});


// DELETE user by ID
router.delete('/:id', async (req, res) => {
  try {
    const userId = req.params.id;
    const user = await pool.query('DELETE FROM users WHERE id = $1 RETURNING *', [userId]);

    if (user.rowCount === 0) {
      return res.status(404).json({ message: 'User not found' });
    }

    res.status(200).json({ message: 'User deleted successfully' });
  } catch (error) {
    console.error(error);
    res.status(500).json({ message: 'Server error' });
  }
});

// User Login
router.post('/login', async (req, res) => {
  try {
    const { email, password } = req.body;

    // Check if user exists
    const user = await pool.query('SELECT * FROM users WHERE email = $1', [email]);
    if (user.rowCount === 0) {
      throw new Error('Email not found');
    }

    // Compare password
    const isValidPassword = await bcrypt.compare(password, user.rows[0].password);
    if (!isValidPassword) {
      throw new Error('Wrong password');
    }

    // Generate JWT token

    const token = jwtGenerator(user.rows[0].id);
    const user_id = user.rows[0].id;
     

    res.status(200).json({ message: 'Login successful', token, userId: user_id, email: email});
  } catch (error) {
    console.error(error); 
    if (error.message === 'Email not found') {
      res.status(401).json({ message: 'Email not found' });
    } else if (error.message === 'Wrong password') {
      res.status(401).json({ message: 'Wrong password' });
    } else {
      res.status(500).json({ message: 'Server error' });
    }
  }
});

// User Logout
router.post('/logout', (req, res) => {
  // Perform logout logic here

  // Clear session, token, or any other authentication data
  // ...
  res.json({ message: 'Logged out successfully' });
});

module.exports = router;

