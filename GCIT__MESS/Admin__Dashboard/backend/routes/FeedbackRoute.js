const express = require('express');
const router = express.Router();
const pool = require("../db");

// CREATE - Insert a new record
router.post('/', async (req, res) => {
  try {
    const { meal_time, date_time, message,email } = req.body;

    const insertQuery = 'INSERT INTO user_feedback (meal_time, date_time, message, email) VALUES ($1, $2, $3,$4) RETURNING *';
    const values = [meal_time, date_time, message,email];

    const result = await pool.query(insertQuery, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error inserting record:', error);
    res.status(500).json({ error: 'An error occurred while inserting the record.' });
  }
});

// READ - Get all records
router.get('/', async (req, res) => {
  try {
    const query = "SELECT * FROM user_feedback";
    const result = await pool.query(query);
    res.json(result.rows);
  } catch (error) {
    console.error('Error retrieving records:', error);
    res.status(500).json({ error: 'An error occurred while retrieving the records.' });
  }
});

// DELETE - Delete a record
router.delete('/:id', async (req, res) => {
    try {
      const { id } = req.params;
      const query = 'DELETE FROM user_feedback WHERE id = $1 RETURNING *';
      const values = [id];
  
      const result = await pool.query(query, values);
      res.json(result.rows[0]);
    } catch (error) {
      console.error('Error deleting record:', error);
      res.status(500).json({ error: 'An error occurred while deleting the record.' });
    }
  });

module.exports = router;
