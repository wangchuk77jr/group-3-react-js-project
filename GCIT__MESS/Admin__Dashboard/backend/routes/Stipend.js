const express = require('express');
const router = express.Router();
const pool = require('../db');

// CREATE - Insert a new record
router.post('/', async (req, res) => {
  try {
    const { month, money_left, money_used } = req.body;

    const insertQuery = 'INSERT INTO stipend (month, money_left, money_used) VALUES ($1, $2, $3) RETURNING *';
    const values = [month, money_left, money_used];

    const result = await pool.query(insertQuery, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error inserting record:', error);
    res.status(500).json({ error: 'An error occurred while inserting the record.' });
  }
});

// READ - Get all records
router.get('/', async (req, res) => {
    try {
      const query = 'SELECT * FROM stipend ORDER BY month DESC';
      const result = await pool.query(query);
      res.json(result.rows);
    } catch (error) {
      console.error('Error retrieving records:', error);
      res.status(500).json({ error: 'An error occurred while retrieving the records.' });
    }
  });
  
  

// UPDATE - Update a record
router.put('/:stipend_id', async (req, res) => {
  try {
    const { stipend_id } = req.params;
    const { month, money_left, money_used } = req.body;
    const query = 'UPDATE stipend SET month = $1, money_left = $2, money_used = $3 WHERE stipend_id = $4 RETURNING *';
    const values = [month, money_left, money_used, stipend_id];

    const result = await pool.query(query, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error updating record:', error);
    res.status(500).json({ error: 'An error occurred while updating the record.' });
  }
});

// DELETE - Delete a record
router.delete('/:stipend_id', async (req, res) => {
  try {
    const { stipend_id } = req.params;
    const query = 'DELETE FROM stipend WHERE stipend_id = $1 RETURNING *';
    const values = [stipend_id];

    const result = await pool.query(query, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error deleting record:', error);
    res.status(500).json({ error: 'An error occurred while deleting the record.' });
  }
});

module.exports = router;
