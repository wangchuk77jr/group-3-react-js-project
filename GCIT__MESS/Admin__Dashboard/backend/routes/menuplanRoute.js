const express = require('express');
const router = express.Router();
const pool = require("../db");

// CREATE - Insert a new record
router.post('/', async (req, res) => {
  try {
    const { menu_day, menu_bf, menu_lunch, menu_dinner } = req.body;

    // Check if the menu_day already exists
    const checkQuery = 'SELECT COUNT(*) FROM menuplan WHERE menu_day = $1';
    const checkResult = await pool.query(checkQuery, [menu_day]);
    const rowCount = parseInt(checkResult.rows[0].count);

    if (rowCount > 0) {
      return res.status(400).json({ error: 'The menu_day already exists.' });
    }

    const insertQuery = 'INSERT INTO menuplan (menu_day, menu_bf, menu_lunch, menu_dinner) VALUES ($1, $2, $3, $4) RETURNING *';
    const values = [menu_day, menu_bf, menu_lunch, menu_dinner];

    const result = await pool.query(insertQuery, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error inserting record:', error);
    res.status(500).json({ error: 'An error occurred while inserting the record.' });
  }
});


router.get('/', async (req, res) => {
  try {
    const query = "SELECT * FROM menuplan ORDER BY CASE WHEN menu_day = 'Monday' THEN 1 WHEN menu_day = 'Tuesday' THEN 2 WHEN menu_day = 'Wednesday' THEN 3 WHEN menu_day = 'Thursday' THEN 4 WHEN menu_day = 'Friday' THEN 5 WHEN menu_day = 'Saturday' THEN 6 WHEN menu_day = 'Sunday' THEN 7 END";
    const result = await pool.query(query);
    res.json(result.rows);
  } catch (error) {
    console.error('Error retrieving records:', error);
    res.status(500).json({ error: 'An error occurred while retrieving the records.' });
  }
});




// UPDATE - Update a record
router.put('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const { menu_day, menu_bf, menu_lunch, menu_dinner } = req.body;

    // Check if the updated menu_day already exists
    const checkQuery = 'SELECT COUNT(*) FROM menuplan WHERE menu_day = $1 AND menuplan_id != $2';
    const checkResult = await pool.query(checkQuery, [menu_day, id]);
    const rowCount = parseInt(checkResult.rows[0].count);

    if (rowCount > 0) {
      return res.status(400).json({ error: 'The updated menu_day already exists.' });
    }

    const query = 'UPDATE menuplan SET menu_day = $1, menu_bf = $2, menu_lunch = $3, menu_dinner = $4 WHERE menuplan_id = $5 RETURNING *';
    const values = [menu_day, menu_bf, menu_lunch, menu_dinner, id];

    const result = await pool.query(query, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error updating record:', error);
    res.status(500).json({ error: 'An error occurred while updating the record.' });
  }
});


// DELETE - Delete a record
router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const query = 'DELETE FROM menuplan WHERE menuplan_id = $1 RETURNING *';
    const values = [id];

    const result = await pool.query(query, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error deleting record:', error);
    res.status(500).json({ error: 'An error occurred while deleting the record.' });
  }
});

module.exports = router;
