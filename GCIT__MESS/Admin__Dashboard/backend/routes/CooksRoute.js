const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');

const pool = require("../db");
// File upload configuration
const storage = multer.diskStorage({
  destination: './uploads/',
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const fileExtension = path.extname(file.originalname);
    cb(null, file.fieldname + '-' + uniqueSuffix + fileExtension);
  },
});

const upload = multer({ storage });

// Create cook route
router.post('/', upload.single('cooks_image'), async (req, res) => {
  try {
    const { name } = req.body;
    const cooks_image = req.file;

    // Insert the cook into the cooks_detail table
    const query = 'INSERT INTO cooks_detail (name, cooks_image_url) VALUES ($1, $2) RETURNING *';
    const values = [name, cooks_image.filename];
    const result = await pool.query(query, values);
    const createdCook = result.rows[0];
    res.status(201).json(createdCook);
  } catch (error) {
    console.error('Error creating cook:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Get all cooks
router.get('/', async (req, res) => {
  try {
    // Retrieve all cooks from the cooks_detail table
    const query = 'SELECT * FROM cooks_detail';
    const result = await pool.query(query);
    const cooks = result.rows;
    res.status(200).json(cooks);
  } catch (error) {
    console.error('Error retrieving cooks:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Delete a cook
router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;

    // Check if the cook with the specified ID exists
    const checkQuery = 'SELECT * FROM cooks_detail WHERE cook_id = $1';
    const checkResult = await pool.query(checkQuery, [id]);
    const existingCook = checkResult.rows[0];
    if (!existingCook) {
      return res.status(404).json({ error: 'Cook not found' });
    }

    // Delete the cook from the cooks_detail table
    const deleteQuery = 'DELETE FROM cooks_detail WHERE cook_id = $1';
    await pool.query(deleteQuery, [id]);

    res.status(200).json({ message: 'Cook deleted successfully' });
  } catch (error) {
    console.error('Error deleting cook:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Update a cook
router.put('/:id', upload.single('cooks_image'), async (req, res) => {
  try {
    const { id } = req.params;
    const { name } = req.body;
    const cooks_image = req.file;

    // Check if the cook with the specified ID exists
    const checkQuery = 'SELECT * FROM cooks_detail WHERE cook_id = $1';
    const checkResult = await pool.query(checkQuery, [id]);
    const existingCook = checkResult.rows[0];
    if (!existingCook) {
      return res.status(404).json({ error: 'Cook not found' });
    }

    // Update the cook in the cooks_detail table
    let cooksImageFilename = existingCook.cooks_image_url; // Keep the current image filename by default

    if (cooks_image) {
      // If a new image is provided, update the image filename
      cooksImageFilename = cooks_image.filename;
    }

    const updateQuery = 'UPDATE cooks_detail SET name = $1, cooks_image_url = $2 WHERE cook_id = $3 RETURNING *';
    const updateValues = [name, cooksImageFilename, id];
    const updateResult = await pool.query(updateQuery, updateValues);

    const updatedCook = updateResult.rows[0];
    res.status(200).json(updatedCook);
  } catch (error) {
    console.error('Error updating cook:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

module.exports = router;
