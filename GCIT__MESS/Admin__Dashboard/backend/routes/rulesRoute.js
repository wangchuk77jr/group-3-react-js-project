const express = require('express');
const router = express.Router();
const pool = require('../db');

// CREATE - Insert a new record
router.post('/', async (req, res) => {
  try {
    const { rule_title, rule_description } = req.body;

    const insertQuery = 'INSERT INTO rules (rule_title, rule_description) VALUES ($1, $2) RETURNING *';
    const values = [rule_title, rule_description];

    const result = await pool.query(insertQuery, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error inserting record:', error);
    res.status(500).json({ error: 'An error occurred while inserting the record.' });
  }
});

// READ - Get all records
router.get('/', async (req, res) => {
  try {
    const query = 'SELECT * FROM rules';
    const result = await pool.query(query);
    res.json(result.rows);
  } catch (error) {
    console.error('Error retrieving records:', error);
    res.status(500).json({ error: 'An error occurred while retrieving the records.' });
  }
});

// UPDATE - Update a record
router.put('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const { rule_title, rule_description } = req.body;
    const query = 'UPDATE rules SET rule_title = $1, rule_description = $2 WHERE rule_id = $3 RETURNING *';
    const values = [rule_title, rule_description, id];

    const result = await pool.query(query, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error updating record:', error);
    res.status(500).json({ error: 'An error occurred while updating the record.' });
  }
});

// DELETE - Delete a record
router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;
    const query = 'DELETE FROM rules WHERE rule_id = $1 RETURNING *';
    const values = [id];

    const result = await pool.query(query, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error deleting record:', error);
    res.status(500).json({ error: 'An error occurred while deleting the record.' });
  }
});

module.exports = router;
