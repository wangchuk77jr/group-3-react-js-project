const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');

const pool = require("../db"); // Assuming you have a separate file for the database connection

// File upload configuration
const storage = multer.diskStorage({
  destination: './uploads/',
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const fileExtension = path.extname(file.originalname);
    cb(null, file.fieldname + '-' + uniqueSuffix + fileExtension);
  },
});

const upload = multer({ storage });

// Create menu coordination
router.post('/', upload.single('profile_image'), async (req, res) => {
  try {
    const { captain_name, coordination_day } = req.body;
    const profile_image = req.file;

    // Insert the menu coordination into the database
    const query = 'INSERT INTO menucoordination (captain_name, coordination_day, profile_image_url) VALUES ($1, $2, $3) RETURNING *';
    const values = [captain_name, coordination_day, profile_image.filename];
    const result = await pool.query(query, values);
    const createdMenuCoordination = result.rows[0];
    res.status(201).json(createdMenuCoordination);
  } catch (error) {
    console.error('Error creating menu coordination:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Get all menu coordinations
router.get('/', async (req, res) => {
  try {
    // Retrieve all menu coordinations from the database
    const query = 'SELECT * FROM menucoordination';
    const result = await pool.query(query);
    const menuCoordinations = result.rows;
    res.status(200).json(menuCoordinations);
  } catch (error) {
    console.error('Error retrieving menu coordinations:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Delete a menu coordination
router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;

    // Check if the menu coordination with the specified ID exists
    const checkQuery = 'SELECT * FROM menucoordination WHERE menu_coordination_id = $1';
    const checkResult = await pool.query(checkQuery, [id]);
    const existingMenuCoordination = checkResult.rows[0];
    if (!existingMenuCoordination) {
      return res.status(404).json({ error: 'Menu coordination not found' });
    }

    // Delete the menu coordination from the database
    const deleteQuery = 'DELETE FROM menucoordination WHERE menu_coordination_id = $1';
    await pool.query(deleteQuery, [id]);

    res.status(200).json({ message: 'Menu coordination deleted successfully' });
  } catch (error) {
    console.error('Error deleting menu coordination:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Update a menu coordination
router.put('/:id', upload.single('profile_image'), async (req, res) => {
  try {
    const { id } = req.params;
    const { captain_name, coordination_day } = req.body;
    const profile_image = req.file;

    // Check if the menu coordination with the specified ID exists
    const checkQuery = 'SELECT * FROM menucoordination WHERE menu_coordination_id = $1';
    const checkResult = await pool.query(checkQuery, [id]);
    const existingMenuCoordination = checkResult.rows[0];
    if (!existingMenuCoordination) {
      return res.status(404).json({ error: 'Menu coordination not found' });
    }

    // Update the menu coordination in the database
    let profileImageFilename = existingMenuCoordination.profile_image_url; // Keep the current profile image filename by default

    if (profile_image) {
      // If a new profile image is provided, update the profile image filename
      profileImageFilename = profile_image.filename;
    }

    const updateQuery = 'UPDATE menucoordination SET captain_name = $1, coordination_day = $2, profile_image_url = $3 WHERE menu_coordination_id = $4 RETURNING *';
    const updateValues = [
      captain_name || existingMenuCoordination.captain_name,
      coordination_day || existingMenuCoordination.coordination_day,
      profileImageFilename,
      id
    ];
    const updateResult = await pool.query(updateQuery, updateValues);

    const updatedMenuCoordination = updateResult.rows[0];
    res.status(200).json(updatedMenuCoordination);
  } catch (error) {
    console.error('Error updating menu coordination:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

module.exports = router;
