const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');

const pool = require('../db');

// File upload configuration
const storage = multer.diskStorage({
  destination: './uploads/',
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const fileExtension = path.extname(file.originalname);
    cb(null, file.fieldname + '-' + uniqueSuffix + fileExtension);
  },
});

const upload = multer({ storage });

// Create menu item
router.post('/', upload.single('menuImage'), async (req, res) => {
  try {
    const { menuName } = req.body;
    const menuImageUrl = req.file ? req.file.filename : null;

    const query = 'INSERT INTO menugallery (menu_name, menu_image_url) VALUES ($1, $2) RETURNING *';
    const values = [menuName, menuImageUrl];

    const result = await pool.query(query, values);
    const createdMenu = result.rows[0];

    res.status(201).json(createdMenu);
  } catch (error) {
    console.error('Error creating menu item:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Get all menu items
router.get('/', async (req, res) => {
  try {
    const query = 'SELECT * FROM menugallery ORDER BY menu_id DESC';
    const result = await pool.query(query);
    const menuItems = result.rows;
    res.status(200).json(menuItems);
  } catch (error) {
    console.error('Error retrieving menu items:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Update a menu item
router.put('/:id', upload.single('menuImage'), async (req, res) => {
    try {
      const { id } = req.params;
      const { menuName } = req.body;
      const menuImageUrl = req.file ? req.file.filename : null;
  
      const checkQuery = 'SELECT * FROM menugallery WHERE menu_id = $1';
      const checkResult = await pool.query(checkQuery, [id]);
      const existingMenuItem = checkResult.rows[0];
      if (!existingMenuItem) {
        return res.status(404).json({ error: 'Menu item not found' });
      }
  
      const updateQuery = 'UPDATE menugallery SET menu_name = $1, menu_image_url = $2 WHERE menu_id = $3 RETURNING *';
      const updateValues = [menuName, menuImageUrl, id];
  
      const updateResult = await pool.query(updateQuery, updateValues);
      const updatedMenuItem = updateResult.rows[0];
  
      res.status(200).json(updatedMenuItem);
    } catch (error) {
      console.error('Error updating menu item:', error);
      res.status(500).json({ error: 'Internal server error' });
    }
  });
  
// Delete a menu item
router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;

    const checkQuery = 'SELECT * FROM menugallery WHERE menu_id = $1';
    const checkResult = await pool.query(checkQuery, [id]);
    const existingMenuItem = checkResult.rows[0];
    if (!existingMenuItem) {
      return res.status(404).json({ error: 'Menu item not found' });
    }

    const deleteQuery = 'DELETE FROM menugallery WHERE menu_id = $1';
    await pool.query(deleteQuery, [id]);

    res.status(200).json({ message: 'Menu item deleted successfully' });
  } catch (error) {
    console.error('Error deleting menu item:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

module.exports = router;
