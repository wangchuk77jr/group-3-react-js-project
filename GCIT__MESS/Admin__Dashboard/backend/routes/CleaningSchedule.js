const express = require('express');
const router = express.Router();
const pool = require('../db');

// Create cleaning schedule route
router.post('/', async (req, res) => {
    try {
      const { date, course, year, status } = req.body;
  
      // Check if the status is one of the allowed values
      if (!['coming', 'next', 'afternext'].includes(status)) {
        return res.status(400).json({ error: 'Invalid status value' });
      }
  
      // Check if a row with the same status already exists
      const checkQuery = 'SELECT COUNT(*) FROM cleaning_schedule WHERE status = $1';
      const checkValues = [status];
      const checkResult = await pool.query(checkQuery, checkValues);
      const rowCount = checkResult.rows[0].count;
  
      if (rowCount > 0) {
        return res.status(409).json({ error: 'A row with the same status already exists' });
      }
  
      // Insert the cleaning schedule row into the database
      const insertQuery = 'INSERT INTO cleaning_schedule (date, course, year, status) VALUES ($1, $2, $3, $4)';
      const insertValues = [date, course, year, status];
  
      await pool.query(insertQuery, insertValues);
  
      res.sendStatus(201);
    } catch (error) {
      console.error('Error creating cleaning schedule row:', error);
      res.status(500).json({ error: 'Internal server error' });
    }
  });
  
// Get all cleaning schedule rows
router.get('/', async (req, res) => {
    try {
      // Retrieve all rows from the cleaning schedule table in the desired order
      const query = 'SELECT * FROM cleaning_schedule ORDER BY CASE status WHEN \'coming\' THEN 1 WHEN \'next\' THEN 2 WHEN \'afternext\' THEN 3 ELSE 4 END';
  
      const result = await pool.query(query);
      const rows = result.rows;
  
      res.status(200).json(rows);
    } catch (error) {
      console.error('Error retrieving cleaning schedule rows:', error);
      res.status(500).json({ error: 'Internal server error' });
    }
  });
  
// Update a cleaning schedule row
router.put('/:id', async (req, res) => {
    try {
      const { id } = req.params;
      const { date, course, year, status } = req.body;
  
      // Check if the status is one of the allowed values
      if (!['coming', 'next', 'afternext'].includes(status)) {
        return res.status(400).json({ error: 'Invalid status value' });
      }
  
      // Check if a row with the same status already exists (excluding the current row being updated)
      const checkQuery = 'SELECT * FROM cleaning_schedule WHERE status = $1 AND s_id <> $2';
      const checkValues = [status, id];
      const checkResult = await pool.query(checkQuery, checkValues);
      const existingRows = checkResult.rows;
  
      if (existingRows.length > 0) {
        return res.status(409).json({ error: 'A row with the same status already exists' });
      }
  
      // Update the cleaning schedule row in the database
      const updateQuery = 'UPDATE cleaning_schedule SET date = $1, course = $2, year = $3, status = $4 WHERE s_id = $5';
      const updateValues = [date, course, year, status, id];
  
      await pool.query(updateQuery, updateValues);
  
      res.sendStatus(200);
    } catch (error) {
      console.error('Error updating cleaning schedule row:', error);
      res.status(500).json({ error: 'Internal server error' });
    }
  });
  

// Delete a cleaning schedule row
router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;

    // Delete the cleaning schedule row from the database
    const deleteQuery = 'DELETE FROM cleaning_schedule WHERE s_id = $1';
    const deleteValues = [id];

    await pool.query(deleteQuery, deleteValues);

    res.sendStatus(200);
  } catch (error) {
    console.error('Error deleting cleaning schedule row:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

module.exports = router;
