const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');

const app = express(); // Create a new Express app

const pool = require("../db");
// File upload configuration
const storage = multer.diskStorage({
  destination: './uploads/',
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const fileExtension = path.extname(file.originalname);
    cb(null, file.fieldname + '-' + uniqueSuffix + fileExtension);
  },
});

const upload = multer({ storage });


// Create announcement route
router.post('/', upload.single('image'), async (req, res) => {
  try {
    const { description } = req.body;
    const image = req.file;
    // Insert the announcement into the database
    const query = 'INSERT INTO announcement (description, image_url) VALUES ($1, $2) RETURNING *';
    const values = [description, image.filename];
    const result = await pool.query(query, values);
    const createdAnnouncement = result.rows[0];
    res.status(201).json(createdAnnouncement);
  } catch (error) {
    console.error('Error creating announcement:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Get all announcements
router.get('/', async (req, res) => {
  try {
    // Retrieve all announcements from the database
    const query = 'SELECT * FROM announcement';
    const result = await pool.query(query);
    const announcements = result.rows;
    res.status(200).json(announcements);
  } catch (error) {
    console.error('Error retrieving announcements:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Delete an announcement
router.delete('/:id', async (req, res) => {
  try {
    const { id } = req.params;

    // Check if the announcement with the specified ID exists
    const checkQuery = 'SELECT * FROM announcement WHERE announcement_id = $1';
    const checkResult = await pool.query(checkQuery, [id]);
    const existingAnnouncement = checkResult.rows[0];
    if (!existingAnnouncement) {
      return res.status(404).json({ error: 'Announcement not found' });
    }

    // Delete the announcement from the database
    const deleteQuery = 'DELETE FROM announcement WHERE announcement_id = $1';
    await pool.query(deleteQuery, [id]);

    res.status(200).json({ message: 'Announcement deleted successfully' });
  } catch (error) {
    console.error('Error deleting announcement:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Update an announcement
router.put('/:id', upload.single('image'), async (req, res) => {
  try {
    const { id } = req.params;
    const { description } = req.body;
    const image = req.file;

    // Check if the announcement with the specified ID exists
    const checkQuery = 'SELECT * FROM announcement WHERE announcement_id = $1';
    const checkResult = await pool.query(checkQuery, [id]);
    const existingAnnouncement = checkResult.rows[0];
    if (!existingAnnouncement) {
      return res.status(404).json({ error: 'Announcement not found' });
    }

    // Update the announcement in the database
    let imageFilename = existingAnnouncement.image_url; // Keep the current image filename by default

    if (image) {
      // If a new image is provided, update the image filename
      imageFilename = image.filename;
    }

    const updateQuery = 'UPDATE announcement SET description = $1, image_url = $2 WHERE announcement_id = $3 RETURNING *';
    const updateValues = [description, imageFilename, id];
    const updateResult = await pool.query(updateQuery, updateValues);

    const updatedAnnouncement = updateResult.rows[0];
    res.status(200).json(updatedAnnouncement);
  } catch (error) {
    console.error('Error updating announcement:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

module.exports = router;
