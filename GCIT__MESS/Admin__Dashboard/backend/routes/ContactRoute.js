const express = require('express');
const router = express.Router();
const pool = require('../db');
const multer = require('multer');
const path = require('path');

// File upload configuration
const storage = multer.diskStorage({
  destination: './uploads/',
  filename: (req, file, cb) => {
    const uniqueSuffix = Date.now() + '-' + Math.round(Math.random() * 1E9);
    const fileExtension = path.extname(file.originalname);
    cb(null, file.fieldname + '-' + uniqueSuffix + fileExtension);
  },
});

const upload = multer({ storage });

// CREATE - Insert a new record with file upload
router.post('/', upload.single('profile_image'), async (req, res) => {
  try {
    const { role, name, phone_no } = req.body;
    const profile_image_url = req.file.filename;

    const insertQuery = 'INSERT INTO mess_committee (role, name, phone_no, profile_image_url) VALUES ($1, $2, $3, $4) RETURNING *';
    const values = [role, name, phone_no, profile_image_url];

    const result = await pool.query(insertQuery, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error inserting record:', error);
    res.status(500).json({ error: 'An error occurred while inserting the record.' });
  }
});

// READ - Get all records
router.get('/', async (req, res) => {
  try {
    const query = 'SELECT * FROM mess_committee';
    const result = await pool.query(query);
    res.json(result.rows);
  } catch (error) {
    console.error('Error retrieving records:', error);
    res.status(500).json({ error: 'An error occurred while retrieving the records.' });
  }
});

router.put('/:c_id', async (req, res) => {
    try {
      const { c_id } = req.params;
      const { role, name, phone_no } = req.body;
      const query = 'UPDATE mess_committee SET role = $1, name = $2, phone_no = $3 WHERE c_id = $4 RETURNING *';
      const values = [role, name, phone_no, c_id];
      const result = await pool.query(query, values);
      res.json(result.rows[0]);
    } catch (error) {
      console.error('Error updating record:', error);
      res.status(500).json({ error: 'An error occurred while updating the record.' });
    }
  });
  

// DELETE - Delete a record
router.delete('/:c_id', async (req, res) => {
  try {
    const { c_id } = req.params;
    const query = 'DELETE FROM mess_committee WHERE c_id = $1 RETURNING *';
    const values = [c_id];

    const result = await pool.query(query, values);
    res.json(result.rows[0]);
  } catch (error) {
    console.error('Error deleting record:', error);
    res.status(500).json({ error: 'An error occurred while deleting the record.' });
  }
});

module.exports = router;
