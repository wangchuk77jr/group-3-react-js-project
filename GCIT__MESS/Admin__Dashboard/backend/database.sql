-- To CREATE database 
CREATE DATABASE "gcit_mess_admin";

--Creating announcement table inside the database
CREATE TABLE announcement(
    announcement_id SERIAL PRIMARY KEY,
    description VARCHAR(500),
    image_url VARCHAR(255),
);
--Creating mealtime table inside the database
CREATE TABLE mealtiming (
    meal_id SERIAL PRIMARY KEY,
    meal_choice VARCHAR(500) UNIQUE,
    meal_time TIME,
    meal_time_end TIME
    );


--Creating rule table inside the database
CREATE TABLE rules (
    rule_id SERIAL PRIMARY KEY,
    rule_title VARCHAR(500),
    rule_descriptions VARCHAR(1000)
);

-- Menucoordinaton table
CREATE TABLE menucoordination (
    menu_coordination_id SERIAL PRIMARY KEY,
    captain_name VARCHAR(500),
    coordination_day VARCHAR(500),
    profile_image_url VARCHAR(255)
);

-- Menu gallery
CREATE TABLE menugallery (
    menu_id SERIAL PRIMARY KEY,
    menu_name VARCHAR(500),
    menu_image_url VARCHAR(255)
);

-- Menu plan
CREATE TABLE menuplan (
    menuplan_id SERIAL PRIMARY KEY,
    menu_day VARCHAR(500),
    menu_bf VARCHAR(255),
    menu_lunch VARCHAR(255),
    menu_dinner VARCHAR(255)
);

-- Item table
CREATE TABLE item_info (
    gitems_id SERIAL PRIMARY KEY,
    item_image_url VARCHAR(255),
    item_name VARCHAR(100),
    amount VARCHAR(20),
    total_money NUMERIC(10, 2),
    supplier_name VARCHAR(100),
    phone_no VARCHAR(20),
    date DATE
);

-- Cleaning Schedule
CREATE TABLE cleaning_schedule (
  s_id SERIAL PRIMARY KEY,
  date DATE NOT NULL,
  course VARCHAR(50) NOT NULL,
  year INTEGER NOT NULL,
  status VARCHAR(20) NOT NULL
);

-- Stipend Table
CREATE TABLE stipend (
  stipend_id SERIAL PRIMARY KEY,
  month VARCHAR(50),
  money_left DECIMAL,
  money_used VARCHAR(50)
);

-- Mess Committee Table
CREATE TABLE mess_committee (
  c_id SERIAL PRIMARY KEY,
  role VARCHAR(50),
  name VARCHAR(50),
  phone_no NUMERIC,
  profile_image_url VARCHAR(255)
  );

-- Cooks Details Table
CREATE TABLE cooks_detail(
    cook_id SERIAL PRIMARY KEY,
    name VARCHAR(500),
    cooks_image_url VARCHAR(255),
    );

-- User Table
CREATE TABLE users (
  id SERIAL PRIMARY KEY,
  username VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL
);

-- Admin Table for Registraion

-- Feedback Table
CREATE TABLE user_feedback (
    id SERIAL PRIMARY KEY,
    meal_time VARCHAR(1000),
    date_time VARCHAR(1000),
    message TEXT
);



CREATE TABLE admin_users (
  id SERIAL PRIMARY KEY,
  username VARCHAR(255) NOT NULL,
  email VARCHAR(255) NOT NULL,
  password VARCHAR(255) NOT NULL
  );

