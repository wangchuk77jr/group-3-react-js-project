import React, { useState } from 'react';
import { Link ,useNavigate} from 'react-router-dom';
import './navbar.css';
import Logo from '../../Asssets/gm.png';


const SideNavBar = () => {
  const [activeItem, setActiveItem] = useState('/admin/home');

  const handleItemClick = (path) => {
    setActiveItem(path);
  };
  const navigate = useNavigate();
  const handleLogout = () => {
    // Clear the token from local storage
    localStorage.removeItem('token');
    // Navigate to the login page
    navigate('/', { replace: true });
  };

  return (
    <div className='position-sticky sticky-top top-0'>
      <ul className='navBar position-fixed'>
        <li className='nav-items'>
          <Link to='/admin/welcome'>
            <img src={Logo} alt='logo' width={120} height={70} />
          </Link>
        </li>
        <li className={`nav-items  nav-items-hover ${activeItem === '/admin/home' ? 'active' : ''}`}>
          <Link to='/admin/home' onClick={() => handleItemClick('/admin/home')}>
            <i className='fa-solid fa-house pe-2' style={{ color: '#fafafa' }}></i>Home
          </Link>
        </li>
        <li className={`nav-items  nav-items-hover ${activeItem === '/admin/feedback' ? 'active' : ''}`}>
          <Link to='/admin/feedback' onClick={() => handleItemClick('/admin/feedback')}>
            <i className='fa-solid fa-comment pe-2' style={{ color: '#fafafa' }}></i>Feedback
          </Link>
        </li>
        <li className={`nav-items nav-items-hover ${activeItem === '/admin/contact' ? 'active' : ''}`}>
          <Link to='/admin/contact' onClick={() => handleItemClick('/admin/contact')}>
            <i className='fa-solid fa-phone pe-2' style={{ color: '#fafafa' }}></i>Contact
          </Link>
        </li>
        <li className={`nav-items nav-items-hover ${activeItem === '/admin/about' ? 'active' : ''}`}>
          <Link to='/admin/about' onClick={() => handleItemClick('/admin/about')}>
            <i className='fa-solid fa-address-card pe-2' style={{ color: '#fafafa' }}></i>About
          </Link>
        </li>
        <li className={`nav-items nav-items-hover ${activeItem === '/admin/grocery-items' ? 'active' : ''}`}>
          <Link to='/admin/grocery-items' onClick={() => handleItemClick('/admin/grocery-items')}>
            <i className='fa-solid fa-bag-shopping pe-2' style={{ color: '#fafafa' }}></i>Grocery Items
          </Link>
        </li>
        <li className={`nav-items nav-items-hover ${activeItem === '/admin/menu-plan' ? 'active' : ''}`}>
          <Link to='/admin/menu-plan' onClick={() => handleItemClick('/admin/menu-plan')}>
            <i className='fa-solid fa-bowl-food pe-2' style={{ color: '#fafafa' }}></i>Menu Plan
          </Link>
        </li>
        <li className={`nav-items nav-items-hover ${activeItem === '/admin/stipend' ? 'active' : ''}`}>
          <Link to='/admin/stipend' onClick={() => handleItemClick('/admin/stipend')}>
            <i className='fa-solid fa-money-check-dollar pe-2' style={{ color: '#fafafa' }}></i>Stipend
          </Link>
        </li>
        <li className={`nav-items nav-items-hover ${activeItem === '/admin/cleaning-schedule' ? 'active' : ''}`}>
          <Link to='/admin/cleaning-schedule' onClick={() => handleItemClick('/admin/cleaning-schedule')}>
            <i className='fa-solid fa-broom pe-2' style={{ color: '#fafafa' }}></i>Cleaning Schedule
          </Link>
        </li>
        <li className={`nav-items nav-items-hover ${activeItem === '/admin/user' ? 'active' : ''}`}>
          <Link to='/admin/user' onClick={() => handleItemClick('/admin/user')}>
            <i className='fa-solid fa-user pe-2' style={{ color: '#fafafa' }}></i>User(Students)
          </Link>
        </li>
        <li className='nav-items mt-5 pe-4'>
          <Link to='/' id='logoutBtn' style={{ width: '100%',height:'40px', border: '2px solid #fff', textAlign: 'center',padding:'0px',borderRadius:'10px'}}>
            <button style={{ width: '100%',height:"100%",background:'transparent',border:'none',color:'#fff',padding:"0px",margin:'0px',fontWeight:'900px'}} onClick={handleLogout}> Logout</button>
          </Link>
        </li>
      </ul>
    </div>
  );
};

export default SideNavBar;
