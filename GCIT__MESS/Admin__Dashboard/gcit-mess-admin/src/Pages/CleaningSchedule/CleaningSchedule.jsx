import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Table, Button, Modal, Form } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer';

const CleaningSchedule = () => {
  const [cleaningSchedule, setCleaningSchedule] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [modalData, setModalData] = useState({ date: '', course: '', year: '', status: '' });
  const [modalAction, setModalAction] = useState('');
  const [deleteModalRowId, setDeleteModalRowId] = useState(null);

  const formatDateString = (dateString) => {
    const date = new Date(dateString);
    return date.toLocaleDateString('en-US');
  };

  useEffect(() => {
    fetchCleaningSchedule();
  }, []);

  const fetchCleaningSchedule = async () => {
    try {
      const response = await axios.get('/cleaning-schedule');
      setCleaningSchedule(response.data);
    } catch (error) {
      console.error('Error fetching cleaning schedule:', error);
      toast.error('Failed to fetch cleaning schedule');
    }
  };

  const handleModalToggle = () => {
    setShowModal(!showModal);
    setModalData({ date: '', course: '', year: '', status: '' });
    setModalAction('');
  };

  const handleInputChange = (e) => {
    setModalData({ ...modalData, [e.target.name]: e.target.value });
  };

  const handleAdd = () => {
    setModalAction('add');
    setShowModal(true);
  };

  const handleEdit = (rowData) => {
    setModalAction('edit');
    setModalData(rowData);
    setShowModal(true);
  };

  const handleDelete = async (id) => {
    try {
      await axios.delete(`/cleaning-schedule/${id}`);
      fetchCleaningSchedule();
      toast.success('Cleaning schedule deleted successfully');
    } catch (error) {
      console.error('Error deleting cleaning schedule:', error);
      toast.error('Failed to delete cleaning schedule');
    } finally {
      setDeleteModalRowId(null);
    }
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      if (modalAction === 'add') {
        await axios.post('/cleaning-schedule', modalData);
        toast.success('Cleaning schedule added successfully');
      } else if (modalAction === 'edit') {
        await axios.put(`/cleaning-schedule/${modalData.s_id}`, modalData);
        toast.success('Cleaning schedule updated successfully');
      }

      setShowModal(false);
      setModalData({ date: '', course: '', year: '', status: '' });
      fetchCleaningSchedule();
    } catch (error) {
      console.error('Error saving cleaning schedule:', error);
      setShowModal(false);
      toast.error('The status already exists. Please try updating it instead');
    }
  };

  const openDeleteModal = (id) => {
    setDeleteModalRowId(id);
  };

  const closeDeleteModal = () => {
    setDeleteModalRowId(null);
  };

  return (
    <MainWrapperContainer>
      <div className='row' style={{ borderBottom: '2px solid green' }}>
        <div className="col-12 d-flex align-items-center w-100" style={{ justifyContent: 'space-between' }}>
          <div className='d-flex align-items-center' style={{ overflowY: 'hidden' }}>
            <h5 className='p-3  text-uppercase' style={{ fontWeight: "800" }}>Add Cleaning Schedule</h5>
          </div>
          <div className='pe-5' style={{ justifySelf: 'flex-end' }}>
            <button onClick={handleAdd} className='px-2' style={{ fontSize: '30px', textDecoration: 'none', color: '#fff', background: '#0CA91C', borderRadius: '100px' }}><i className="fa-solid fa-plus" style={{ position: 'relative', top: '3px' }}></i></button>
          </div>
        </div>
      </div>
      <div className='row' style={{ padding: '20px' }}>
        <div className="col-12">
          <div className="table-responsive" style={{ boxShadow: '0px 2px 4px rgb(0 0 0 / 8%)' }}>
            <Table striped bordered hover mt-4>
              <thead>
                <tr>
                  <th>Date</th>
                  <th>Class</th>
                  <th>Year</th>
                  <th>Status</th>
                  <th className='text-center'>Actions</th>
                </tr>
              </thead>
              <tbody>
                {cleaningSchedule.map((row) => (
                  <tr key={row.s_id}>
                    <td>{formatDateString(row.date)}</td>
                    <td>{row.course}</td>
                    <td>{row.year}</td>
                    <td>{row.status}</td>
                    <td className='text-center'>
                      <button onClick={() => handleEdit(row)} className='btn btn-success me-3'><i className="fa-solid fa-pencil"></i></button>
                      <button onClick={() => openDeleteModal(row.s_id)} className='btn btn-danger'><i className="fa-solid fa-trash"></i></button>
                      <Modal show={deleteModalRowId === row.s_id} onHide={closeDeleteModal}>
                        <Modal.Header closeButton>
                          <Modal.Title>Delete Confirmation</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                          Are you sure you want to delete this item?
                        </Modal.Body>
                        <Modal.Footer>
                          <Button variant="secondary" onClick={closeDeleteModal}>
                            Cancel
                          </Button>
                          <Button variant="danger" onClick={() => handleDelete(row.s_id)}>
                            Delete
                          </Button>
                        </Modal.Footer>
                      </Modal>
                    </td>
                  </tr>
                ))}
              </tbody>
            </Table>
          </div>
        </div>
      </div>

      <Modal show={showModal} onHide={handleModalToggle}>
        <Modal.Header className='bg-warning' closeButton>
          <Modal.Title style={{ fontSize: '20px', fontWeight: '900' }}>{modalAction === 'add' ? 'Add' : 'Edit'} Cleaning Schedule</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit}>
            <Form.Group controlId="formDate">
              <Form.Label>Date</Form.Label>
              <Form.Control type="date" name="date" value={modalData.date} onChange={handleInputChange} required />
            </Form.Group>
            <Form.Group controlId="formClass">
              <Form.Label>Course</Form.Label>
              <Form.Control as='select' type="text" name="course" value={modalData.course} onChange={handleInputChange} required>
                <option value="" disabled>----Select Course----</option>
                <option value="AI">AI</option>
                <option value="Block Chain">Block Chain</option>
                <option value="Fullstack">Fullstack</option>
                <option value="SOC(A)">SOC(A)</option>
                <option value="SOC(B)">SOC(B)</option>
                <option value="SMD">SMD</option>
              </Form.Control>
            </Form.Group>
            <Form.Group controlId="formYear">
              <Form.Label>Year</Form.Label>
              <Form.Control as="select" type="text" name="year" value={modalData.year} onChange={handleInputChange} required>
                <option value="" disabled>----Select Year----</option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
              </Form.Control>
            </Form.Group>
            <Form.Group controlId="formStatus">
              <Form.Label>Status</Form.Label>
              <Form.Control as="select" name="status" value={modalData.status} onChange={handleInputChange} required>
                <option value="" disabled>----Select status----</option>
                <option value="coming">Coming</option>
                <option value="next">Next</option>
                <option value="afternext">After Next</option>
              </Form.Control>
            </Form.Group>
            <div className='text-end mt-3 mb-3'>
              <Button style={{ width: '200px' }} variant="success" type="submit">{modalAction === 'add' ? 'Add' : 'UPDATE'}</Button>{' '}
            </div>
          </Form>
        </Modal.Body>
      </Modal>
      <ToastContainer position="bottom-right"
        autoClose={8000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored" style={{ width: '400px' }} />
    </MainWrapperContainer>
  );
};

export default CleaningSchedule;
