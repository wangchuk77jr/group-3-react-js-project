import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Modal, Button, Form, Table } from 'react-bootstrap';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer';

axios.defaults.baseURL = 'http://localhost:5000';

const About = () => {
  const [showAddModal, setShowAddModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [cooks, setCooks] = useState([]);
  const [name, setName] = useState('');
  const [file, setFile] = useState(null);
  const [editCook, setEditCook] = useState(null);
  const [deleteCook, setDeleteCook] = useState(null);

  useEffect(() => {
    fetchCooks();
  }, []);

  const fetchCooks = async () => {
    try {
      const response = await axios.get('/cooks');
      setCooks(response.data);
    } catch (error) {
      toast.error('Error fetching cooks:', error);
    }
  };

  const handleFormSubmit = async (event) => {
    event.preventDefault();

    try {
      const formData = new FormData();
      formData.append('name', name);
      formData.append('cooks_image', file);

      await axios.post('/cooks', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });

      fetchCooks();
      setName('');
      setFile(null);
      toast.success('Cook added successfully');
      setShowAddModal(false);
    } catch (error) {
      toast.error('Error creating cook:', error);
    }
  };

  const handleEditFormSubmit = async (event) => {
    event.preventDefault();

    try {
      const formData = new FormData();
      formData.append('name', name);
      formData.append('cooks_image', file);

      await axios.put(`/cooks/${editCook.cook_id}`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });

      fetchCooks();
      setName('');
      setFile(null);
      toast.success('Cook updated successfully');
      setShowEditModal(false);
    } catch (error) {
      toast.error('Error updating cook:', error);
    }
  };

  const handleDeleteConfirm = (cook) => {
    setDeleteCook(cook);
    setShowDeleteModal(true);
  };

  const handleDeleteCook = async (cook) => {
    try {
      await axios.delete(`/cooks/${cook.cook_id}`);
      fetchCooks();
      toast.success('Cook deleted successfully');
      setShowDeleteModal(false);
    } catch (error) {
      toast.error('Error deleting cook:', error);
    }
  };

  const handleModalOpen = () => {
    setShowAddModal(true);
  };

  const handleModalClose = () => {
    setShowAddModal(false);
  };

  const handleEditModalOpen = (cook) => {
    setEditCook(cook);
    setName(cook.name);
    setShowEditModal(true);
  };

  const handleEditModalClose = () => {
    setEditCook(null);
    setName('');
    setFile(null);
    setShowEditModal(false);
  };

  const handleFileChange = (event) => {
    setFile(event.target.files[0]);
  };

  return (
    <>
    <MainWrapperContainer>
    <div className='row' style={{borderBottom:'2px solid green'}}>
            <div className="col-12 d-flex align-items-center w-100" style={{justifyContent:'space-between'}}>
            <div className=' d-flex align-items-center' style={{overflowY:'hidden'}}>
              <h5 className='p-3  text-uppercase' style={{fontWeight:"800"}}>Add Cooks Details</h5>
            </div>
              <div className='pe-5' style={{justifySelf:'flex-end'}}>
                <button  onClick={handleModalOpen} className='px-2' style={{fontSize:'30px',textDecoration:'none',color:'#fff',background:'#0CA91C',borderRadius:'100px'}}><i class="fa-solid fa-plus" style={{position:'relative',top:'3px'}}></i></button>
              </div>
            </div>
          </div>
    <div className='row' style={{ padding: '20px' }}>
      <div className="col-12">
        <div className="table-responsive" style={{ boxShadow: '0px 2px 4px rgb(0 0 0 / 8%)' }}>   
          <Table striped bordered hover>
            <thead>
              <tr>
                <th>Cooks Name</th>
                <th>Profile Image</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {cooks.map((cook) => (
                <tr key={cook.cook_id}>
                  <td>{cook.name}</td>
                  <td>
                    <img style={{ width: '100%', height: '40px', objectFit: 'cover' }} src={`http://localhost:5000/uploads/${cook.cooks_image_url}`} alt={cook.name} />
                  </td>
                  <td style={{ display: 'flex', alignItems: 'center', gap: '20px' }}>
                        <button className='edit-delete-btn bg-success' onClick={() => handleEditModalOpen(cook)}>
                                  <i className="fa-solid fa-pencil"></i>
                        </button>
                        <button onClick={() => handleDeleteConfirm(cook)} className='edit-delete-btn bg-danger'>
                                  <i class="fa-solid fa-trash-can"></i>
                          </button>
                      </td>
                </tr>
              ))}
            </tbody>
          </Table>
          </div>
        </div>
      </div>

      <Modal show={showAddModal} onHide={handleModalClose}>
        <Modal.Header className='bg-warning' closeButton>
          <Modal.Title style={{fontSize:'18px',fontWeight:'900'}}>Add Cooks</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleFormSubmit}>
            <Form.Group>
              <Form.Label>Name Of Cook</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter cook name"
                value={name}
                onChange={(event) => setName(event.target.value)}
              />
            </Form.Group>
            <Form.Group className='mt-2'>
              <Form.Label>Profile Image</Form.Label>
              <Form.Control type="file" onChange={handleFileChange} />
            </Form.Group>
            <div className='text-end mt-3 mb-4'>
            <Button style={{width:'200px'}} variant="success" type="submit">
              Add
            </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      <Modal show={showEditModal} onHide={handleEditModalClose}>
        <Modal.Header className='bg-warning' closeButton>
          <Modal.Title style={{fontSize:'18px',fontWeight:'900'}}>Edit Cook</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleEditFormSubmit}>
            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter cook name"
                value={name}
                onChange={(event) => setName(event.target.value)}
              />
            </Form.Group>
            <Form.Group className='mt-2'>
              <Form.Label>Select New Image</Form.Label>
              <Form.Control type="file" onChange={handleFileChange} />
            </Form.Group>
            <div className='text-end mt-3 mb-4'>
            <Button style={{width:'200px'}} variant="success" type="submit">
              Update
            </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      <Modal show={showDeleteModal} onHide={() => setShowDeleteModal(false)}>
        <Modal.Header className='bg-warning' closeButton>
          <Modal.Title style={{fontSize:'18px',fontWeight:'900'}}>Confirm Delete</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure you want to delete the cook: <strong>{deleteCook?.name}</strong>?
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowDeleteModal(false)}>
            Cancel
          </Button>
          <Button variant="danger" onClick={() => handleDeleteCook(deleteCook)}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>

      
      <ToastContainer position="bottom-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
              theme="colored" />
      </MainWrapperContainer>
    </>
  );
};

export default About;
