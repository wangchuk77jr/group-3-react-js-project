import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer';
import { Modal, Button } from 'react-bootstrap';

const User = () => {
  const [users, setUsers] = useState([]);
  const [selectedUser, setSelectedUser] = useState(null);
  const [showModal, setShowModal] = useState(false);

  useEffect(() => {
    fetchUsers();
  }, []);

  const fetchUsers = async () => {
    try {
      const response = await axios.get('/auth');
      setUsers(response.data);
    } catch (error) {
      console.error(error);
      toast.error('Failed to fetch users');
    }
  };

  const deleteUser = async (id) => {
    try {
      await axios.delete(`/auth/${id}`);
      toast.success('User deleted successfully');
      fetchUsers();
    } catch (error) {
      console.error(error);
      toast.error('Failed to delete user');
    }
    closeModal();
  };

  const openModal = (user) => {
    setSelectedUser(user);
    setShowModal(true);
  };

  const closeModal = () => {
    setSelectedUser(null);
    setShowModal(false);
  };

  return (
    <MainWrapperContainer>
      <div className='container'>
        <div className='user-count ps-3 pt-2 mb-2' style={{background:'rgb(255,165,0)',borderBottomLeftRadius:"10px",borderBottomRightRadius:"10px"}}>
          <p style={{fontWeight:'900',color:'#fff',fontSize:"18px"}}>Total Number Users: {users.length}</p>
        </div>
        <table className="table">
          <thead>
            <tr>
              <th>ID</th>
              <th>Username</th>
              <th>Email</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {users.map((user, index) => (
              <tr key={user.id}>
                <td>{index + 1}</td>
                <td>{user.username}</td>
                <td>{user.email}</td>
                <td>
                  <button
                    className="btn btn-danger"
                    onClick={() => openModal(user)}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
      <Modal show={showModal} onHide={closeModal}>
        <Modal.Header closeButton>
          <Modal.Title>Delete User</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>
          Are you sure you want to delete this user: <span style={{fontWeight:'700'}}>{selectedUser && selectedUser.username}?</span> 
          </p>
        </Modal.Body>
        <Modal.Footer>
            <Button variant="secondary" onClick={closeModal}>
            Cancel
          </Button>
          <Button style={{width:'200px'}} variant="danger" onClick={() => deleteUser(selectedUser && selectedUser.id)}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>

      <ToastContainer position="bottom-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored" 
        style={{ width: "400px",padding:'10px' }}
        />
    </MainWrapperContainer>
  );
};

export default User;



// import React, { useState, useEffect } from 'react';
// import axios from 'axios';
// import { toast, ToastContainer } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';
// import MainWrapperContainer from '../../Component/Home/MainWrapperContainer';
// import { Modal, Button } from 'react-bootstrap';
// import { Line } from 'react-chartjs-2';

// const User = () => {
//   const [users, setUsers] = useState([]);
//   const [selectedUser, setSelectedUser] = useState(null);
//   const [showModal, setShowModal] = useState(false);

//   useEffect(() => {
//     fetchUsers();
//   }, []);

//   const fetchUsers = async () => {
//     try {
//       const response = await axios.get('/auth');
//       setUsers(response.data);
//     } catch (error) {
//       console.error(error);
//       toast.error('Failed to fetch users');
//     }
//   };

//   const deleteUser = async (id) => {
//     try {
//       await axios.delete(`/auth/${id}`);
//       toast.success('User deleted successfully');
//       fetchUsers();
//     } catch (error) {
//       console.error(error);
//       toast.error('Failed to delete user');
//     }
//     closeModal();
//   };

//   const openModal = (user) => {
//     setSelectedUser(user);
//     setShowModal(true);
//   };

//   const closeModal = () => {
//     setSelectedUser(null);
//     setShowModal(false);
//   };

//   // Prepare data for chart
//   const chartData = {
//     labels: users.map((user) => user.registrationDate), // Assuming you have a 'registrationDate' property
//     datasets: [
//       {
//         label: 'User Registrations',
//         data: users.map((user, index) => index + 1),
//         backgroundColor: 'rgba(75, 192, 192, 0.2)',
//         borderColor: 'rgba(75, 192, 192, 1)',
//         borderWidth: 1,
//       },
//     ],
//   };

//   return (
//     <MainWrapperContainer>
//       <div className='container'>
//         <div className='user-count ps-3 pt-2 mb-2' style={{ background: 'rgb(255,165,0)', borderBottomLeftRadius: "10px", borderBottomRightRadius: "10px" }}>
//           <p style={{ fontWeight: '900', color: '#fff', fontSize: "18px" }}>Total Number Users: {users.length}</p>
//         </div>
//         <table className="table">
//           <thead>
//             <tr>
//               <th>ID</th>
//               <th>Username</th>
//               <th>Email</th>
//               <th>Action</th>
//             </tr>
//           </thead>
//           <tbody>
//             {users.map((user, index) => (
//               <tr key={user.id}>
//                 <td>{index + 1}</td>
//                 <td>{user.username}</td>
//                 <td>{user.email}</td>
//                 <td>
//                   <button
//                     className="btn btn-danger"
//                     onClick={() => openModal(user)}
//                   >
//                     Delete
//                   </button>
//                 </td>
//               </tr>
//             ))}
//           </tbody>
//         </table>
//         <div className='chart-container'>
//           <Line data={chartData} />
//         </div>
//       </div>
//       <ToastContainer />
//       <Modal show={showModal} onHide={closeModal}>
//         <Modal.Header closeButton>
//           <Modal.Title>Delete User</Modal.Title>
//         </Modal.Header>
//         <Modal.Body>
//           <p>
//             Are you sure you want to delete this user: {selectedUser && selectedUser.username}?
//           </p>
//         </Modal.Body>
//         <Modal.Footer>
//           <Button variant="danger" onClick={() => deleteUser(selectedUser && selectedUser.id)}>
//             Delete
//           </Button>
//           <Button variant="secondary" onClick={closeModal}>
//             Cancel
//           </Button>
//         </Modal.Footer>
//       </Modal>
//     </MainWrapperContainer>
//   );
// };

// export default User;

