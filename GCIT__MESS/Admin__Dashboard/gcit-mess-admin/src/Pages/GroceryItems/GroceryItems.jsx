import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Modal, Form, Button } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer';

const GroceryItems = () => {
  const [showModal, setShowModal] = useState(false);
  const handleModalToggle = () => {
    setShowModal(!showModal);
  };
  const formatDateString = (dateString) => {
    const date = new Date(dateString);
    return date.toLocaleDateString('en-US');
  };
  const [items, setItems] = useState([]);
  const [modalOpen, setModalOpen] = useState(false);
  const [editItemId, setEditItemId] = useState(null); // Track the ID of the item being edited
  const [formData, setFormData] = useState({
    item_name: '',
    amount: '',
    total_money: '',
    supplier_name: '',
    phone_no: '',
    date: '',
    item_image: null,
  });

  useEffect(() => {
    // Fetch all items on component mount
    fetchItems();
  }, []);

  const fetchItems = async () => {
    try {
      const response = await axios.get('/itemInfo');
      setItems(response.data);
    } catch (error) {
      console.error('Error fetching items:', error);
      toast.error('Failed to fetch items');
    }
  };

  const handleInputChange = (e) => {
    const { name, value } = e.target;
    setFormData((prevState) => ({
      ...prevState,
      [name]: value,
    }));
  };

  const handleFileChange = (e) => {
    const file = e.target.files[0];
    setFormData((prevState) => ({
      ...prevState,
      item_image: file,
    }));
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    try {
      const formDataToSend = new FormData();
      formDataToSend.append('item_name', formData.item_name);
      formDataToSend.append('amount', formData.amount);
      formDataToSend.append('total_money', formData.total_money);
      formDataToSend.append('supplier_name', formData.supplier_name);
      formDataToSend.append('phone_no', formData.phone_no);
      formDataToSend.append('date', formData.date);
      formDataToSend.append('item_image', formData.item_image);

      if (editItemId) {
        // If editing an existing item, send a PUT request instead of POST
        await axios.put(`/itemInfo/${editItemId}`, formDataToSend);
        toast.success('Item updated successfully');
      } else {
        // If creating a new item, send a POST request
        await axios.post('/itemInfo', formDataToSend);
        toast.success('Item created successfully');
      }

      setModalOpen(false);
      setEditItemId(null); // Reset editItemId after editing
      setFormData({
        item_name: '',
        amount: '',
        total_money: '',
        supplier_name: '',
        phone_no: '',
        date: '',
        item_image: null,
      });
      fetchItems();
    } catch (error) {
      console.error('Error creating/updating item:', error);
      setModalOpen(true);
      toast.error('Failed to create/update item');
    }
  };

  const handleEdit = (item) => {
    setEditItemId(item.gitems_id);
    setFormData({
      item_name: item.item_name,
      amount: item.amount,
      total_money: item.total_money,
      supplier_name: item.supplier_name,
      phone_no: item.phone_no,
      date: item.date,
      item_image: null,
    });
    setModalOpen(true);
  };

  const handleDelete = async (itemId) => {
    try {
      await axios.delete(`/itemInfo/${itemId}`);
      setShowModal(false)

      toast.success('Item deleted successfully');
      fetchItems();

    } catch (error) {
      console.error('Error deleting item:', error);
      toast.error('Failed to delete item');
    }
  };

  return (
    <div>
    <MainWrapperContainer>
          <div className='row' style={{borderBottom:'2px solid green'}}>
            <div className="col-12 d-flex align-items-center w-100" style={{justifyContent:'space-between'}}>
            <div className=' d-flex align-items-center' style={{overflowY:'hidden'}}>
              <h5 className='p-3  text-uppercase' style={{fontWeight:"800"}}>Add Grocery Items</h5>
            </div>
              <div className='pe-5' style={{justifySelf:'flex-end'}}>
                <button  onClick={() => setModalOpen(true)} className='px-2' style={{fontSize:'30px',textDecoration:'none',color:'#fff',background:'#0CA91C',borderRadius:'100px'}}><i class="fa-solid fa-plus" style={{position:'relative',top:'3px'}}></i></button>
              </div>
            </div>
          </div>
      {/* Modal */}
      <Modal show={modalOpen} onHide={() => setModalOpen(false)}>
        <Modal.Header className='bg-warning' closeButton>
          <Modal.Title  style={{fontSize:'20px',fontWeight:'900'}}>{editItemId ? 'Update Grocery Items' : 'Add Grocery Items'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit}>
            <Form.Group controlId="item_name">
              <Form.Label>Item Name</Form.Label>
              <Form.Control
                required
                type="text"
                name="item_name"
                placeholder="Item Name"
                value={formData.item_name}
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group controlId="amount">
              <Form.Label>Amount(Kg)</Form.Label>
              <Form.Control
                required
                type="number"
                name="amount"
                placeholder="Item Amount (ex.30kg)"
                value={formData.amount}
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group controlId="total_money">
              <Form.Label>Price Per Kg(NU.)</Form.Label>
              <Form.Control
                required
                type="number"
                name="total_money"
                placeholder="Price Per Kg"
                value={formData.total_money}
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group controlId="supplier_name">
              <Form.Label>Supplier Name</Form.Label>
              <Form.Control
                required
                type="text"
                name="supplier_name"
                placeholder="Supplier Name"
                value={formData.supplier_name}
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group controlId="phone_no">
              <Form.Label>Phone Number</Form.Label>
              <Form.Control
                required
                type="number"
                minLength={8}
                maxLength={8}
                name="phone_no"
                placeholder="Phone Number"
                value={formData.phone_no}
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group controlId="date">
              <Form.Label>Date</Form.Label>
              <Form.Control
                required
                type="date"
                name="date"
                placeholder="Date"
                value={formData.date}
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group controlId="item_image">
              <Form.Label>Item Image</Form.Label>
              <Form.Control required type="file" name="item_image" onChange={handleFileChange} />
            </Form.Group>
            <div className='text-end mt-3 mb-3'>
            <Button style={{width:'200px'}} variant="success" type="submit">
              {editItemId ? 'UPDATE' : 'ADD'}
            </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
      {/* Item List */}
      <div className='row' style={{ padding: '20px' }}>
        <div className="col-12">
          <div className="table-responsive" style={{ boxShadow: '0px 2px 4px rgb(0 0 0 / 8%)' }}>
            <table className="table table-hover">
              <thead>
                <tr>
                  <th>Item Image</th>
                  <th>Item Name</th>
                  <th>Amount(kg)</th>
                  <th>Price Per Kg</th>
                  <th>Suplier Name</th>
                  <th>Phone Number</th>
                  <th>Date</th>
                  <th>Total Cost</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              {items.map((item) => (
                  <tr key={item.gitems_id}>
                    <td>
                      {item.item_image_url && (
                          <img
                            className='p-0 m-0'
                            src={`http://localhost:5000/uploads/${encodeURIComponent(
                              item.item_image_url
                            )}`}
                            style={{ width: '80%', height: '40px', objectFit: 'cover', objectPosition: 'center' }}
                            alt="item"
                          />
                        )}</td>
                    
                    <td>{item.item_name}</td>
                    <td>{item.amount}kg</td>
                    <td>Nu.{item.total_money}</td>
                    <td>{item.supplier_name}</td>
                    <td>{item.phone_no}</td>
                    <td>{formatDateString(item.date)}</td>
                    <td>{item.amount*item.total_money}</td>
                    <td>
                      <button onClick={() => handleEdit(item)} className='btn btn-success me-3'><i className="fa-solid fa-pencil"></i></button>
                      <button onClick={handleModalToggle} className='btn btn-danger'>
                        <i className="fa-solid fa-trash"></i>
                      </button>

                      <Modal show={showModal} onHide={handleModalToggle}>
                        <Modal.Header closeButton>
                          <Modal.Title>Delete Confirmation</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                          Are you sure you want to delete this item?
                        </Modal.Body>
                        <Modal.Footer>
                          <Button variant="secondary" onClick={handleModalToggle}>
                            Cancel
                          </Button>
                          <Button variant="danger" onClick={() => handleDelete(item.gitems_id)}>
                            Delete
                          </Button>
                        </Modal.Footer>
                      </Modal>
                      </td>
                    </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <ToastContainer position="bottom-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
              theme="colored" />
      </MainWrapperContainer>
    </div>
  );
};

export default GroceryItems;
