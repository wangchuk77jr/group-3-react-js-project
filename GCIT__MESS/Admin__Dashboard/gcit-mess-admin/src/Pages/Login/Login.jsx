// import React from 'react'
// import './Login.css'
// import logo from './gm.png'

// export const Login = () => {
//   return (
//     <div className='LoginContainer'>
//         <div className="container login-box" style={{width:'500px'}}>
//             <div className="row">
//                 <div className="col-12">
//                     <div className='text-center'>
//                         <img src={logo} alt="logo" width={120} height={70} />
//                     </div>
//                     <p className='text-center' style={{textTransform:'uppercase',fontWeight:'900',color:'#fff'}}>GCIT Mess Admin Panel</p>
//                     <form action="/admin/welcome">
//                         <div className='input__Container mt-3'>
//                             <input type="email" name="" id=""placeholder='Email Address' required />
//                             <input type="password" name="" id="" placeholder='Password' required />
//                              <a href="/forgot_password" style={{textAlign:'end',fontWeight:'400',fontSize:'18px',color:'#eeeeeec1'}}>Forgot Password?</a>
//                         </div>
//                         <div className='text-center'>
//                             <button className='login_button' style={{width:'60%'}}>Login</button>
//                         </div>
//                     </form>
//                 </div>
//             </div>
//         </div>
//     </div>
//   )
// }

import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { useNavigate } from 'react-router-dom'; // Import useNavigate hook
import './Login.css';
import logo from './gm.png';

const API_URL = 'http://localhost:5000';

export const Login = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const navigate = useNavigate(); // Initialize useNavigate

  const handleLogin = async (e) => {
    e.preventDefault();

    try {
      const response = await axios.post(`${API_URL}/admin/login`, {
        email,
        password,
      });

      if (response.data.token) {
        // Save the token to local storage
        localStorage.setItem('token', response.data.token);
        toast.success('Logged in Successfully');

        // Navigate to the welcome page
        navigate('/admin/welcome');
      } else {
        // Handle login error
        toast.error('Email not found or wrong password');
      }
    } catch (error) {
      // Handle login error
      if (error.response && error.response.status === 404) {
        toast.error('Email not found');
      } else if (error.response && error.response.status === 401) {
        toast.error('Wrong password');
      } else {
        toast.error(`Login failed: ${error.message}`);
      }
    }
  };

  useEffect(() => {
    // Check if the user is already logged in
    const token = localStorage.getItem('token');

    if (token) {
      // User is already logged in, navigate to the welcome page
      navigate('/admin/welcome');
    }
  }, [navigate]);

  return (
    <div className="LoginContainer">
      <div className="container login-box" style={{ width: '500px' }}>
        <div className="row">
          <div className="col-12">
            <div className="text-center">
              <img src={logo} alt="logo" width={120} height={70} />
            </div>
            <p className="text-center" style={{ textTransform: 'uppercase', fontWeight: '900', color: '#fff' }}>
              GCIT Mess Admin Panel
            </p>
            <form onSubmit={handleLogin}>
              <div className="input__Container mt-3">
                <input
                  type="email"
                  placeholder="Email Address"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                  required
                />
                <input
                  type="password"
                  placeholder="Password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                  required
                />
                <a href="/forgot_password" style={{ textAlign: 'end', fontWeight: '400', fontSize: '18px', color: '#eeeeeec1' }}>
                  Forgot Password?
                </a>
              </div>
              <div className="text-center">
                <button className="login_button" style={{ width: '60%' }} type="submit">
                  Login
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
      <ToastContainer style={{width:"35%"}} theme="colored"  position="top-center" autoClose={5000} hideProgressBar={false} newestOnTop closeOnClick rtl={false} pauseOnFocusLoss draggable pauseOnHover />
    </div>
  );
};















