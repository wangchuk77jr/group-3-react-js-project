// import React, { useState, useEffect } from 'react';
// import axios from 'axios';
// import { toast } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';
// import MainWrapperContainer from '../../Component/Home/MainWrapperContainer';
// import { Modal, Button } from 'react-bootstrap';

// const Feedback = () => {
//   const [feedbackList, setFeedbackList] = useState([]);
//   const [deleteId, setDeleteId] = useState(null);
//   const [deleteConfirmationModal, setDeleteConfirmationModal] = useState(false);

//   useEffect(() => {
//     fetchFeedback();
//   }, []);

//   const fetchFeedback = async () => {
//     try {
//       const response = await axios.get('/feedback');
//       setFeedbackList(response.data);
//     } catch (error) {
//       console.error('Error retrieving feedback:', error);
//       toast.error('An error occurred while retrieving feedback.');
//     }
//   };

//   const handleDelete = async (id) => {
//     try {
//       const response = await axios.delete(`/feedback/${id}`);
//       if (response.data) {
//         setFeedbackList((prevFeedbackList) =>
//           prevFeedbackList.filter((feedback) => feedback.id !== id)
//         );
//         toast.success('Feedback deleted successfully.');
//       }
//     } catch (error) {
//       console.error('Error deleting feedback:', error);
//       toast.error('An error occurred while deleting feedback.');
//     }
//   };

//   const handleCloseDeleteConfirmationModal = () => {
//     setDeleteConfirmationModal(false);
//   };

//   const handleShowDeleteConfirmationModal = () => {
//     setDeleteConfirmationModal(true);
//   };

//   const handleDeleteMenuItem = () => {
//     handleDelete(deleteId);
//     handleCloseDeleteConfirmationModal();
//   };

//   return (
//     <MainWrapperContainer>
//       <div className="row">
//         <div className="col-12">
//           <h5 className="p-3 border-bottom text-uppercase" style={{ fontWeight: "800" }}>
//             Feedback
//           </h5>
//         </div>
//       </div>
     
//       <div className='row' style={{ padding: '20px' }}>
//         <div className="col-12">
//           <div className="table-responsive" style={{ boxShadow: '0px 2px 4px rgb(0 0 0 / 8%)' }}>
//             <table className="table table-hover">
//               <thead>
//                 <tr>
//                   <th>Meal Time</th>
//                   <th>Date & Time</th>
//                   <th className='w-50'>Message</th>
//                   <th>Action</th>
//                 </tr>
//               </thead>
//               <tbody>
//                 {feedbackList.map((feedback) => (
//                   <tr key={feedback.id}>
//                     <td>{feedback.meal_time}</td>
//                     <td>{feedback.date_time}</td>
//                     <td>{feedback.message}</td>
//                     <td>
//                       <button
//                         className="btn btn-danger"
//                         onClick={() => {
//                           setDeleteId(feedback.id);
//                           handleShowDeleteConfirmationModal();
//                         }}
//                       >
//                         Delete
//                       </button>
//                     </td>
//                   </tr>
//                 ))}
//               </tbody>
//             </table>
//           </div>
//         </div>
//       </div>

//       <Modal show={deleteConfirmationModal} onHide={handleCloseDeleteConfirmationModal}>
//         <Modal.Header className='bg-warning' closeButton>
//           <Modal.Title style={{ fontSize: '20px', fontWeight: '900' }}>Confirm Deletion</Modal.Title>
//         </Modal.Header>
//         <Modal.Body>
//           <p>Are you sure you want to delete this menu item?</p>
//         </Modal.Body>
//         <Modal.Footer>
//           <Button variant="secondary" onClick={handleCloseDeleteConfirmationModal}>
//             Cancel
//           </Button>
//           <Button variant="danger" onClick={handleDeleteMenuItem}>
//             Delete
//           </Button>
//         </Modal.Footer>
//       </Modal>
//     </MainWrapperContainer>
//   );
// };

// export default Feedback;


import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer';
import { Modal, Button } from 'react-bootstrap';

const Feedback = () => {
  const [feedbackData, setFeedbackData] = useState([]);
  const [deleteId, setDeleteId] = useState(null);
  const [deleteConfirmationModal, setDeleteConfirmationModal] = useState(false);
  const [showAllRows, setShowAllRows] = useState(false);

  useEffect(() => {
    fetchFeedback();
  }, []);

  const fetchFeedback = async () => {
    try {
      const response = await axios.get('/feedback');
      setFeedbackData(response.data);
    } catch (error) {
      console.error('Error retrieving feedback:', error);
      toast.error('An error occurred while retrieving feedback.');
    }
  };

  const handleDelete = async (mealTime, id) => {
    try {
      const response = await axios.delete(`/feedback/${id}`);
      if (response.data) {
        const updatedFeedbackData = feedbackData.map((item) => {
          if (item.meal_time === mealTime && item.id === id) {
            return { ...item, deleted: true };
          }
          return item;
        });
        setFeedbackData(updatedFeedbackData);
        toast.success('Feedback deleted successfully. Refresh the page to see the changes!');
      }
    } catch (error) {
      console.error('Error deleting feedback:', error);
      toast.error('An error occurred while deleting feedback.');
    }
  };

  const handleCloseDeleteConfirmationModal = () => {
    setDeleteConfirmationModal(false);
  };

  const handleShowDeleteConfirmationModal = () => {
    setDeleteConfirmationModal(true);
  };

  const handleDeleteMenuItem = () => {
    const [mealTime, itemId] = deleteId.split('-');
    handleDelete(mealTime, itemId);
    handleCloseDeleteConfirmationModal();
  };

  const toggleRows = () => {
    setShowAllRows(!showAllRows);
  };

  const renderFeedbackTable = (mealTime) => {
    const filteredData = feedbackData.filter(
      (item) => item.meal_time === mealTime && !item.deleted
    );

    const rowsToShow = showAllRows ? filteredData.length : 4;

    if (filteredData.length === 0) {
      return <p>No feedback available for {mealTime}</p>;
    }

    return (
      <div className="table-responsive" style={{ boxShadow: '0px 2px 4px rgb(0 0 0 / 8%)' }}>
        <table className="table table-hover" style={{ border: '1px solid rgb(252,152,69)' }}>
          <thead>
            <tr style={{ background: 'rgb(252,152,69)', color: '#fff' }}>
              <th className="w-25">Meal Time</th>
              <th className="w-25">Dat & Time</th>
              <th className="w-100">Message</th>
              <th className="w-25">Email Address</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            {filteredData.slice(0, rowsToShow).map((feedback) => (
              <tr key={`${mealTime}-${feedback.id}`}>
                <td style={{ border: '1px solid rgb(252,152,69)' }}>{feedback.meal_time}</td>
                <td style={{ border: '1px solid rgb(252,152,69)' }}>{feedback.date_time}</td>
                <td style={{ border: '1px solid rgb(252,152,69)' }}>{feedback.message}</td>
                <td style={{ border: '1px solid rgb(252,152,69)' }}>{feedback.email}</td>
                <td style={{ border: '1px solid rgb(252,152,69)' }}>
                  <button
                    className="btn btn-danger"
                    style={{width:'150px'}}
                    onClick={() => {
                      setDeleteId(`${mealTime}-${feedback.id}`);
                      handleShowDeleteConfirmationModal();
                    }}
                  >
                    Delete
                  </button>
                </td>
              </tr>
            ))}
          
             
              
            {filteredData.length > 4 && (
                <tr>
               <td  style={{ border: '1px solid rgb(252,152,69)' }}>------</td>
               <td  style={{ border: '1px solid rgb(252,152,69)' }}>------</td>
               <td  style={{ border: '1px solid rgb(252,152,69)' }}>------</td>
               <td  style={{ border: '1px solid rgb(252,152,69)' }}>------</td>
            <td>
              {showAllRows ? (
                <button className="btn px-4 text-uppercase" style={{background:'#FE5C00',color:'#fff',width:'150px'}} onClick={toggleRows}>
                  See Less
                </button>
              ) : (
                <button className="btn px-4 text-uppercase" style={{background:'#FE5C00',color:'#fff',width:'150px'}} onClick={toggleRows}>
                  See All
                </button>
              )}
              </td>
              </tr>
          )}
           
          </tbody>
        </table>
  
      </div>
    );
  };

  return (
    <MainWrapperContainer>
      <div className="row" style={{ borderBottom: '2px solid green' }}>
        <div className="col-12">
          <h5 className="p-3 text-uppercase" style={{ fontWeight: '800' }}>
             GCIT Mess Feedback - (Student's Feedback)
          </h5>
        </div>
      </div>

      <div className="row" style={{ padding: '20px' }}>
        <div className="col-12">
          <p style={{fontSize:'18px',fontWeight:'700'}}>Breakfast Feedback</p>
          {renderFeedbackTable('Breakfast')}
        </div>
      </div>
      <div className="row" style={{ padding: '20px' }}>
        <div className="col-12">
          <p style={{fontSize:'18px',fontWeight:'700'}}>Lunch Feedback</p>
          {renderFeedbackTable('Lunch')}
        </div>
      </div>
      <div className="row" style={{ padding: '20px' }}>
        <div className="col-12">
          <p style={{fontSize:'18px',fontWeight:'700'}}>Dinner Feedback</p>
          {renderFeedbackTable('Dinner')}
        </div>
      </div>

      <Modal show={deleteConfirmationModal} onHide={handleCloseDeleteConfirmationModal}>
        <Modal.Header className="bg-warning" closeButton>
          <Modal.Title style={{ fontSize: '20px', fontWeight: '900' }}>Confirm Deletion</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Are you sure you want to delete this feedback?</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseDeleteConfirmationModal}>
            Cancel
          </Button>
          <Button variant="danger" onClick={handleDeleteMenuItem}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
      <ToastContainer
        position="bottom-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored"
      />
    </MainWrapperContainer>
  );
};

export default Feedback;

