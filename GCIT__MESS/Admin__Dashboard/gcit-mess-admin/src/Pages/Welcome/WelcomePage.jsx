import React from 'react'
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer'
import welcome from './Welcome-cuate.png'
const WelcomePage = () => {
  return (
    <>
        <MainWrapperContainer>
            <div className='m-0 p-0' style={{background:'#FFA500',width:"100%",borderLeft:'3px dotted #fff'}}>
              <img src={welcome} alt="welcome" style={{width:'100%',height:'100vh',objectFit:'contain',objectPosition:'center'}} />
            </div>
        </MainWrapperContainer>
    </>
  )
}

export default WelcomePage