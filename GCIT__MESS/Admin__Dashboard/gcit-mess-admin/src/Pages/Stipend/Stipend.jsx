import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Button, Modal, Form } from 'react-bootstrap';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer';

const Stipend = () => {
  const [stipends, setStipends] = useState([]);
  const [formData, setFormData] = useState({
    month: '',
    money_left: '',
    money_used: ''
  });
  const [showModal, setShowModal] = useState(false);
  const [editId, setEditId] = useState(null);

  useEffect(() => {
    fetchStipends();
  }, []);

  const fetchStipends = async () => {
    try {
      const response = await axios.get('/stipend');
      setStipends(response.data);
    } catch (error) {
      console.error('Error fetching stipends:', error);
    }
  };

  const handleChange = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      if (editId) {
        await axios.put(`/stipend/${editId}`, formData);
        setStipends((prevStipends) =>
          prevStipends.map((stipend) =>
            stipend.stipend_id === editId ? { ...stipend, ...formData } : stipend
          )
        );
        toast.success('Stipend updated successfully!');
      } else {
        const response = await axios.post('/stipend', formData);
        setStipends([...stipends, response.data]);
        toast.success('Stipend added successfully!');
      }
      setFormData({ month: '', money_left: '', money_used: '' });
      setEditId(null);
      setShowModal(false);
    } catch (error) {
      console.error('Error creating/updating stipend:', error);
      toast.error('Failed to add/update stipend.');
    }
  };

  const handleDelete = async (stipend_id) => {
    try {
      await axios.delete(`/stipend/${stipend_id}`);
      setStipends(stipends.filter((stipend) => stipend.stipend_id !== stipend_id));
      toast.success('Stipend deleted successfully!');
    } catch (error) {
      console.error('Error deleting stipend:', error);
      toast.error('Failed to delete stipend.');
    }
  };

  const handleEdit = (stipend) => {
    setFormData(stipend);
    setEditId(stipend.stipend_id);
    setShowModal(true);
  };

  const getFormattedMonth = (dateString) => {
    const date = new Date(dateString);
    const month = date.toLocaleString('default', { month: 'long' });
    const year = date.getFullYear();
    return `${month} ${year}`;
  };

  const getMinimumDate = () => {
    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    const currentMonth = currentDate.getMonth() + 1; // Months are zero-indexed, so add 1
  
    // Format the minimum date string in the format "YYYY-MM"
    return `${currentYear}-${currentMonth.toString().padStart(2, '0')}`;
  };
  

  return (
    <div>
      <MainWrapperContainer>
      <div className='row' style={{ borderBottom: '2px solid green' }}>
        <div className="col-12 d-flex align-items-center w-100" style={{ justifyContent: 'space-between' }}>
          <div className=' d-flex align-items-center' style={{ overflowY: 'hidden' }}>
            <h5 className='p-3  text-uppercase' style={{ fontWeight: "800" }}>Stipend Balance</h5>
          </div>
          <div className='pe-5' style={{ justifySelf: 'flex-end' }}>
            <button onClick={() => setShowModal(true)} className='px-2' style={{ fontSize: '30px', textDecoration: 'none', color: '#fff', background: '#0CA91C', borderRadius: '100px' }}><i className="fa-solid fa-plus" style={{ position: 'relative', top: '3px' }}></i></button>
          </div>
        </div>
      </div>
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>{editId ? 'Edit Stipend' : 'Add Stipend'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit}>
          <Form.Group controlId="month">
            <Form.Label>Month</Form.Label>
            <Form.Control
              type="month"
              name="month"
              value={formData.month}
              onChange={handleChange}
              min={getMinimumDate()}
              required
            />
          </Form.Group>

            <Form.Group controlId="money_left">
              <Form.Label>Money Left</Form.Label>
              <Form.Control type="number" name="money_left" value={formData.money_left} onChange={handleChange} required />
            </Form.Group>
            <Form.Group controlId="money_used">
              <Form.Label>Money Used</Form.Label>
              <Form.Control type="number" name="money_used" value={formData.money_used} onChange={handleChange} required />
            </Form.Group>
            <div className='text-end mt-3 mt-4'>
               <Button  style={{width:'200px'}} variant="success" type="submit">{editId ? 'UPDATE' : 'ADD'}</Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
      <div className='row' style={{ padding: '20px' }}>
        <div className="col-12">
          <div className="table-responsive" style={{ boxShadow: '0px 2px 4px rgb(0 0 0 / 8%)' }}>
            <table className="table table-hover">
        <thead>
          <tr>
            <th>Month</th>
            <th>Money Used</th>
            <th>Money Left</th>
            <th>Actions</th>
          </tr>
        </thead>
        <tbody>
          {stipends.map((stipend) => (
            <tr key={stipend.stipend_id}>
              <td>{getFormattedMonth(stipend.month)}</td>
              <td>{stipend.money_used}</td>
              <td>{stipend.money_left}</td>
              <td>
                <button onClick={() => handleEdit(stipend)} className='btn btn-success me-3'><i className="fa-solid fa-pencil"></i></button>
                <button onClick={() => handleDelete(stipend.stipend_id)} className='btn btn-danger'><i className="fa-solid fa-trash"></i></button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
      </div>
      </div>
      </div>
      <ToastContainer position="bottom-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored" 
        style={{ width: "400px",padding:'10px' }}
        />
      </MainWrapperContainer>
    </div>
  );
};

export default Stipend;
