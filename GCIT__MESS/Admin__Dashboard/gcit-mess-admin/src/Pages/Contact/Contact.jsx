import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Modal, Form, Button } from 'react-bootstrap';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer';
const Contact = () => {
  const [contacts, setContacts] = useState([]);
  const [modalData, setModalData] = useState({
    role: '',
    name: '',
    phone_no: '',
    profile_image: null,
  });
  const [showModal, setShowModal] = useState(false);
  const [modalAction, setModalAction] = useState('add');
  const [deleteModalRowId, setDeleteModalRowId] = useState(null);
  const [showDeleteModal, setShowDeleteModal] = useState(false);

  useEffect(() => {
    fetchContacts();
  }, []);

  const fetchContacts = async () => {
    try {
      const response = await axios.get('/api/contacts');
      setContacts(response.data);
    } catch (error) {
      console.error('Error fetching contacts:', error);
    }
  };
const handleAdd = () => {
  setModalAction('add');
  setModalData({
    role: '',
    name: '',
    phone_no: '',
    profile_image: null,
  });
  setShowModal(true);
};

const handleEdit = (contact) => {
  setModalAction('edit');
  setModalData({
    role: contact.role,
    name: contact.name,
    phone_no: contact.phone_no,
    profile_image: null,
  });
  setDeleteModalRowId(contact.c_id);
  setShowModal(true);
};

const handleSubmit = async (event) => {
  event.preventDefault();

  try {
    const formData = new FormData();
    formData.append('role', modalData.role);
    formData.append('name', modalData.name);
    formData.append('phone_no', modalData.phone_no);
    if (modalData.profile_image) {
      formData.append('profile_image', modalData.profile_image);
    }

    let response;
    if (modalAction === 'add') {
      response = await axios.post('/api/contacts', formData);
      toast.success('Contact added successfully.');
    } else if (modalAction === 'edit') {
      response = await axios.put(`/api/contacts/${deleteModalRowId}`, formData);
      toast.success('Contact updated successfully.');
    }

    setShowModal(false);
    fetchContacts();
  } catch (error) {
    console.error('Error submitting form:', error);
  }
};
  const handleDelete = async () => {
    try {
      const response = await axios.delete(`/api/contacts/${deleteModalRowId}`);
      toast.success('Contact deleted successfully.');
      setShowDeleteModal(false);
      fetchContacts();
    } catch (error) {
      console.error('Error deleting contact:', error);
    }
  };

  const showDeleteConfirmation = (rowId) => {
    setDeleteModalRowId(rowId);
    setShowDeleteModal(true);
  };

  return (
    <div>
      <MainWrapperContainer>
        <div className='row' style={{borderBottom:'2px solid green'}}>
            <div className="col-12 d-flex align-items-center w-100" style={{justifyContent:'space-between'}}>
            <div className=' d-flex align-items-center' style={{overflowY:'hidden'}}>
              <h5 className='p-3  text-uppercase' style={{fontWeight:"800"}}>Contact Page Management</h5>
            </div>
              <div className='pe-5' style={{justifySelf:'flex-end'}}>
                <button  onClick={handleAdd} className='px-2' style={{fontSize:'30px',textDecoration:'none',color:'#fff',background:'#0CA91C',borderRadius:'100px'}}><i class="fa-solid fa-plus" style={{position:'relative',top:'3px'}}></i></button>
              </div>
            </div>
          </div>
      <div className='row' style={{ padding: '20px' }}>
        <div className="col-12">
          <div className="table-responsive" style={{ boxShadow: '0px 2px 4px rgb(0 0 0 / 8%)' }}>
            <table className="table table-hover">
              <thead>
                <tr>
                  <th>Profile image</th>
                  <th>Name</th>
                  <th>Phone Number</th>
                  <th>Role</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
              {contacts.map((contact) => (
                  <tr key={contact.c_id}>
                    <td>
                      {contact.profile_image_url  && (
                          <img
                            className='p-0 m-0'
                            src={`http://localhost:5000/uploads/${encodeURIComponent(
                              contact.profile_image_url
                            )}`}
                            style={{ width: '80%', height: '40px', objectFit: 'cover', objectPosition: 'center' }}
                            alt="contact"
                          />
                        )}</td>
                    <td>{contact.name}</td>
                    <td>{contact.phone_no}</td>
                    <td>{contact.role}</td>
                    <td>
                      <button onClick={() => handleEdit(contact)} className='btn btn-success me-3'><i className="fa-solid fa-pencil"></i></button>
                      <button onClick={() => showDeleteConfirmation(contact.c_id)} className='btn btn-danger'>
                        <i className="fa-solid fa-trash"></i>
                      </button>
                      </td>
                    </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>{modalAction === 'add' ? 'Add Mess Comittee Member' : 'Edit Mess Comittee Member'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleSubmit}>
            <Form.Group controlId="role">
              <Form.Label>Role</Form.Label>
              <Form.Control
                as="select" type="text"
                value={modalData.role}
                onChange={(e) => setModalData({ ...modalData, role: e.target.value })}>
                <option value="" disabled>----Select Role----</option>
                <option value="Mess Incharge">Mess Incharge</option>
                <option value="Team Lead">Team Lead</option>
                <option value="Deputy">Deputy</option>
                <option value="Secretary">Secretary</option>
                <option value="Tertiary">Tertiary</option>
                <option value="Logistic Team">Logistic Team</option>
                </Form.Control>
            </Form.Group>
            <Form.Group controlId="name">
              <Form.Label>Name</Form.Label>
              <Form.Control
                type="text"
                value={modalData.name}
                onChange={(e) => setModalData({ ...modalData, name: e.target.value })}
              />
            </Form.Group>
            <Form.Group controlId="phone_no">
              <Form.Label>Phone Number</Form.Label>
              <Form.Control
                type="text"
                value={modalData.phone_no}
                onChange={(e) => setModalData({ ...modalData, phone_no: e.target.value })}
              />
            </Form.Group>
            <Form.Group controlId="profile_image">
              <Form.Label>Profile Image</Form.Label>
              <Form.Control
                type="file"
                accept="image/*"
                onChange={(e) => setModalData({ ...modalData, profile_image: e.target.files[0] })}
              />
            </Form.Group>
            <div className='text-end mt-3 mb-4'>
            <Button style={{width:'200px'}} variant="success" type="submit">
              Save
            </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
      <Modal show={showDeleteModal} onHide={() => setShowDeleteModal(false)}>
        <Modal.Header closeButton>
          <Modal.Title>Delete Contact</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure you want to delete this contact?</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={() => setShowDeleteModal(false)}>
            Cancel
          </Button>
          <Button variant="danger" onClick={handleDelete}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
      <ToastContainer position="bottom-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored" 
        style={{ width: "400px",padding:'10px' }}
        />
      </MainWrapperContainer>
    </div>
  );
};

export default Contact;
