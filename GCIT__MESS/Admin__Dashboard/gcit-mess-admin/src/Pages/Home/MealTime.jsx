import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Modal, Button, Form } from 'react-bootstrap';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer'

axios.defaults.baseURL = 'http://localhost:5000';

const MealTime = () => {
  const [showModal, setShowModal] = useState(false);
  const [mealTimings, setMealTimings] = useState([]);
  const [mealChoice, setMealChoice] = useState('');
  const [mealTimeStart, setMealTimeStart] = useState('');
  const [mealTimeEnd, setMealTimeEnd] = useState('');
  const [selectedMealTiming, setSelectedMealTiming] = useState(null);

  // Fetch meal timings from the server
  useEffect(() => {
    fetchMealTimings();
  }, []);

  const fetchMealTimings = async () => {
    try {
      const response = await axios.get('/meal-timings');
      setMealTimings(response.data);
    } catch (error) {
      console.error('Error fetching meal timings:', error);
    }
  };

  const handleFormSubmit = async (event) => {
    event.preventDefault();

    try {
      if (selectedMealTiming) {
        // Update existing meal timing
        await axios.put(`/meal-timings/${selectedMealTiming.meal_id}`, {
          meal_choice: mealChoice,
          meal_time: mealTimeStart,
          meal_time_end: mealTimeEnd,
        });

        setSelectedMealTiming(null);
        toast.success('Meal timing updated successfully');
      } else {
        // Add new meal timing
        await axios.post('/meal-timings', {
          meal_choice: mealChoice,
          meal_time: mealTimeStart,
          meal_time_end: mealTimeEnd,
        });

        toast.success('Meal timing added successfully');
      }

      setMealChoice('');
      setMealTimeStart('');
      setMealTimeEnd('');
      setShowModal(false);

      fetchMealTimings();
    } catch (error) {
      console.error('Error creating/updating meal timing:', error);
      setShowModal(false);
      toast.error('Meal Choice already existed!',error);
    }
  };

  const handleEditClick = (mealTiming) => {
    setSelectedMealTiming(mealTiming);
    setMealChoice(mealTiming.meal_choice);
    setMealTimeStart(mealTiming.meal_time);
    setMealTimeEnd(mealTiming.meal_time_end);
    setShowModal(true);
  };

  const handleDeleteClick = async (mealTiming) => {
    try {
      await axios.delete(`/meal-timings/${mealTiming.meal_id}`);
      toast.success('Meal timing deleted successfully');
      fetchMealTimings();
    } catch (error) {
      console.error('Error deleting meal timing:', error);
      toast.error('An error occurred while deleting the meal timing');
    }
  };

  const handleModalOpen = () => {
    setSelectedMealTiming(null);
    setMealChoice('');
    setMealTimeStart('');
    setMealTimeEnd('');
    setShowModal(true);
  };

  const handleModalClose = () => {
    setSelectedMealTiming(null);
    setShowModal(false);
  };
  const formatTime = (timeString) => {
    const time = new Date(`1970-01-01T${timeString}`);
    return time.toLocaleTimeString([], { hour: 'numeric', minute: '2-digit' });
  };

  return (
    <MainWrapperContainer>
      <div className='row' style={{borderBottom:'2px solid green'}}>
            <div className="col-12 d-flex align-items-center w-100" style={{justifyContent:'space-between'}}>
            <div className=' d-flex align-items-center' style={{overflowY:'hidden'}}>
            <h5 className='p-2 ms-3 goBack__btn'><a href="/admin/home" style={{overflowY:'hidden'}}><i className="fa-solid fa-angles-left pe-2" style={{position:'relative',top:'3px',overflowY:'hidden'}}></i>Go Back</a></h5>
              <h5 className='p-3  text-uppercase' style={{fontWeight:"800"}}>Meal Timing</h5>
            </div>
              <div className='pe-5' style={{justifySelf:'flex-end'}}>
                <button  onClick={handleModalOpen} className='px-2' style={{fontSize:'30px',textDecoration:'none',color:'#fff',background:'#0CA91C',borderRadius:'100px'}}><i class="fa-solid fa-plus" style={{position:'relative',top:'3px'}}></i></button>
              </div>
            </div>
          </div>

      <Modal show={showModal} onHide={handleModalClose}>
        <Modal.Header className='bg-warning' closeButton>
          <Modal.Title style={{fontSize:'18px',fontWeight:'900'}}>{selectedMealTiming ? 'Edit Meal Timing' : 'Add Meal Timing'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleFormSubmit}>
            <Form.Group controlId="mealChoice">
              <Form.Label>Meal Choice</Form.Label>
              <Form.Control
                type="text"
                as='select'
                value={mealChoice}
                onChange={(event) => setMealChoice(event.target.value)}>
                  <option value="" disabled>---Select Meal Timing----</option>
                  <option value="Breakfast" >Breakfast</option>
                  <option value="Lunch" >Lunch</option>
                  <option value="Dinner" >Dinner</option>
                </Form.Control>            </Form.Group>
            <Form.Group controlId="mealTimeStart">
              <Form.Label>Meal Time Start</Form.Label>
              <Form.Control
                type="time"
                value={mealTimeStart}
                onChange={(event) => setMealTimeStart(event.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="mealTimeEnd">
              <Form.Label>Meal Time End</Form.Label>
              <Form.Control
                type="time"
                value={mealTimeEnd}
                onChange={(event) => setMealTimeEnd(event.target.value)}
              />
            </Form.Group>
            <div className='text-end mt-3 mb-4'>
            <Button style={{width:'250px'}} variant="success" type="submit">
              {selectedMealTiming ? 'Update' : 'Add'}
            </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>
      <div className='row' style={{ padding: '20px' }}>
        <div className="col-12">
          <div className="table-responsive" style={{ boxShadow: '0px 2px 4px rgb(0 0 0 / 8%)' }}>
            <table className="table table-hover">
            <thead>
              <tr>
                <th>Meal Choice</th>
                <th>Meal Time Start</th>
                <th>Meal Time End</th>
                <th>Actions</th>
              </tr>
            </thead>
            <tbody>
              {mealTimings.map((mealTiming) => (
                <tr key={mealTiming.meal_id}>
                  <td>{mealTiming.meal_choice}</td>
                  <td>{formatTime(mealTiming.meal_time)}</td>
                  <td>{formatTime(mealTiming.meal_time_end)}</td>
                  <td style={{ display: 'flex', alignItems: 'center', gap: '20px' }}>
                    <button className='edit-delete-btn bg-success' onClick={() => handleEditClick(mealTiming)}>
                              <i className="fa-solid fa-pencil"></i>
                    </button>
                    <button onClick={() => handleDeleteClick(mealTiming)} className='edit-delete-btn bg-danger'>
                              <i class="fa-solid fa-trash-can"></i>
                      </button>
                  </td>
                </tr>
              ))}
            </tbody>
            </table>
            </div>
            </div>
            </div>

      <ToastContainer position="bottom-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
              theme="colored" />
    </MainWrapperContainer>
  );
};

export default MealTime;
