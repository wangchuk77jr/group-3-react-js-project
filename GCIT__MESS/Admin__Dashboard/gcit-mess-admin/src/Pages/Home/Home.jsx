import React from 'react'
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer'
import './Home.css'
export const Home = () => {
  return (
    <>
    <MainWrapperContainer>
      <div className='row'>
        <div className="col-12">
          <h5 className='p-3 border-bottom text-uppercase' style={{fontWeight:"800"}}>Home Content Management</h5>
        </div>
      </div>
      <div className="row ms-2 mt-4">
        <div className="col-md-4">
          <a  href="/admin/home/annoucement" className='content-card'>
            Announcement
          </a>
        </div>
        <div className="col-md-4">
          <a href="/admin/home/curry-menu" className='content-card'>
          Curry Menu
          </a>
        </div>
      </div>
      <div className="row ms-2 mt-3">
        <div className="col-md-4">
          <a className='content-card' href="/admin/home/mealtime">
            Meal Timing
          </a>
        </div>
        <div className="col-md-4">
          <a href="/admin/home/rules" className='content-card'>
            Rules and regulations
          </a>
        </div>
      </div>
      <div className="row ms-2 mt-3">
        <div className="col-md-4">
          <a href="/admin/home/menucoordination" className='content-card'>
            MENU COORDINATION DAY
          </a>
        </div>
      </div>
    
    </MainWrapperContainer>  
    </>
  )
}
