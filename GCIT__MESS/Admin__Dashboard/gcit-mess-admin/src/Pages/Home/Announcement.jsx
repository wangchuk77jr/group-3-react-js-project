import React, { useState, useEffect } from 'react';
import axios from 'axios';
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer'
import './Home.css'
import { Modal, Button, Form } from 'react-bootstrap';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

axios.defaults.baseURL = 'http://localhost:5000';
const Announcement = () => {
      const [showModal, setShowModal] = useState(false);
      const [showEditMoadl,setShowEditModal] = useState(false)
      const [announcements, setAnnouncements] = useState([]);
      
      // Fetch announcements from the server
      useEffect(() => {
        fetchAnnouncements();
      }, []);

      const fetchAnnouncements = async () => {
        try {
          const response = await axios.get('/announcements');
          setAnnouncements(response.data);
        } catch (error) {
          toast.error('Error fetching announcements:', error);
        }
      };

      // add
      const [description, setDescription] = useState('');
      const [image, setImage] = useState(null);
      const handleDescriptionChange = (event) => {
        setDescription(event.target.value);
      };
    
      const handleImageChange = (event) => {
        setImage(event.target.files[0]);
      };
    
      const handleFormSubmit = async (event) => {
        event.preventDefault();
    
        try {
          // Create a new FormData instance
          const formData = new FormData();
          formData.append('description', description);
          formData.append('image', image);
    
          // Send the announcement with the image to the server
          await axios.post('/announcements', formData);
    
          // Fetch the updated announcements
          fetchAnnouncements();
    
          // Clear the input fields
          setDescription('');
          setImage(null);
          toast.success('Announcement added successfully')
          setShowModal(false)
        } catch (error) {
          toast.error('Error creating announcement:', error);
        }
      };
      // Eidt
      const [currentAnnouncement, setCurrentAnnouncement] = useState(null);
      const handleEditClick = (announcement) => {
        setCurrentAnnouncement(announcement);
        setDescription(announcement.description);
        setShowEditModal(true)
      };
      const handleEditFormSubmit = async (event) => {
        event.preventDefault();
    
        try {
          const { announcement_id } = currentAnnouncement;
    
          // Create a new FormData instance
          const formData = new FormData();
          formData.append('description', description);
          formData.append('image', image);
    
          // Send the updated announcement to the server
          await axios.put(`/announcements/${announcement_id}`, formData);
    
          // Fetch the updated announcements
          fetchAnnouncements();
    
          // Close the edit modal
          setShowEditModal(false)
    
          // Clear the input fields
          setDescription('');
          setImage(null);
    
          // Set the success message
          toast.success('Announcement updated successfully!')
        } catch (error) {
          toast.error('Error updating announcement:', error);
        }
      };

      // delete
      const [deleteModalOpen, setDeleteModalOpen] = useState(false);
      const handleDeleteClick = (announcement) => {
        setCurrentAnnouncement(announcement);
        setDeleteModalOpen(true);
      };
      const handleDeleteConfirm = async () => {
        try {
          const { announcement_id } = currentAnnouncement;
    
          // Send the delete request to the server
          await axios.delete(`/announcements/${announcement_id}`);
    
          // Fetch the updated announcements
          fetchAnnouncements();
    
          // Close the delete modal
          setDeleteModalOpen(false);
    
          // Set the success message
          toast.success('Announcement deleted successfully')
        } catch (error) {
          toast.error('Error deleting announcement:', error);
        }
      };
    
      const handleDeleteCancel = () => {
        setDeleteModalOpen(false);
      };
    // Handling modal
    const handleModalOpen = () => {
      setShowModal(true);
    };
    const handleModalClose = () => {
      setShowModal(false);
    };
    const handleEditModalClose = () =>{
      setShowEditModal(false)
    }

      
    return (
        <MainWrapperContainer>
          <div className='row' style={{borderBottom:'2px solid green'}}>
            <div className="col-12 d-flex align-items-center w-100" style={{justifyContent:'space-between'}}>
            <div className=' d-flex align-items-center' style={{overflowY:'hidden'}}>
              <h5 className='p-2 ms-3 goBack__btn'><a href="/admin/home" style={{overflowY:'hidden'}}><i className="fa-solid fa-angles-left pe-2" style={{position:'relative',top:'3px',overflowY:'hidden'}}></i>Go Back</a></h5>
              <h5 className='p-3  text-uppercase' style={{fontWeight:"800"}}>Add New Annoucement</h5>
            </div>
              <div className='pe-5' style={{justifySelf:'flex-end'}}>
                <button  onClick={handleModalOpen} className='px-2' style={{fontSize:'30px',textDecoration:'none',color:'#fff',background:'#0CA91C',borderRadius:'100px'}}><i class="fa-solid fa-plus" style={{position:'relative',top:'3px'}}></i></button>
              </div>
            </div>
          </div>
          {/* Add Modal */}
          <Modal show={showModal} onHide={handleModalClose} centered className='p-4'>
            <Modal.Header className='bg-warning' closeButton>
              <Modal.Title style={{fontSize:'20px',fontWeight:'900'}}>Add Annoucement</Modal.Title>
            </Modal.Header>
            <Modal.Body className='pb-5'>
              <Form action='/' onSubmit={handleFormSubmit}>
                <Form.Group>
                  <Form.Label>Descriptions:</Form.Label>
                  <Form.Control style={{border:'1px solid #0CA91C'}} as="textarea"   value={description}
                   onChange={handleDescriptionChange} placeholder='Enter the announcement or news descriptions' rows={6}  required />
                </Form.Group>
                <Form.Group className='mt-2'>
                  <Form.Label>Select Image:</Form.Label>
                  <Form.Control style={{border:'1px solid #0CA91C'}} type="file"  accept="image/*"  onChange={handleImageChange} required />
                </Form.Group>
                <div className='text-end'>
                <Button className='mt-4 w-50' type="submit" style={{backgroundColor:'#0CA91C'}}>ADD</Button>
                </div>
              </Form>
            </Modal.Body>
          </Modal>
          {/* edit modal */}
          <Modal show={showEditMoadl} onHide={handleEditModalClose} centered className='p-4'>
              <Modal.Header className='bg-warning' closeButton>
                  <Modal.Title style={{fontSize:'20px',fontWeight:'900'}}>Edit Announcement</Modal.Title>
                    </Modal.Header>
                      <Modal.Body className='pb-5'>
                          <Form onSubmit={handleEditFormSubmit}>
                                  <Form.Group>
                                    <Form.Label>Descriptions:</Form.Label>
                                    <Form.Control style={{border:'1px solid #0CA91C'}} as="textarea" value={description}
                                    onChange={handleDescriptionChange} placeholder='Enter the announcement or news descriptions' rows={6}  required />
                                  </Form.Group>
                                  <Form.Group className='mt-2'>
                                    <Form.Label>Select New Image:</Form.Label>
                                    <Form.Control style={{border:'1px solid #0CA91C'}} type="file"  accept="image/*" onChange={handleImageChange} />
                                    {currentAnnouncement && currentAnnouncement.image_url && (
                                        <p className='mt-2'>Current Image URL: <a href={`http://localhost:5000/uploads/${currentAnnouncement.image_url}`} target="_blank" rel="noopener noreferrer">{currentAnnouncement.image_url}</a></p>
                                    )}

                                  </Form.Group>
                              <div className='text-end'>
                           <Button className='mt-4 w-50' type="submit" style={{backgroundColor:'#0CA91C'}}>Update</Button>
                          </div>
                         </Form>
                </Modal.Body>
          </Modal>
          {/* delete modal */}
          <Modal show={deleteModalOpen} onHide={handleDeleteCancel} centered className='p-4'>
              <Modal.Header className='bg-warning' closeButton>
                <Modal.Title style={{fontSize:'20px',fontWeight:'900'}}>Delete Announcement</Modal.Title>
              </Modal.Header>
              <Modal.Body>
                <p>Are you sure you want to delete this announcement?</p>
              </Modal.Body>
              <Modal.Footer>
                <Button variant="secondary" onClick={handleDeleteCancel}>Cancel</Button>
                <Button variant="danger" onClick={handleDeleteConfirm}>Delete</Button>
              </Modal.Footer>
            </Modal>


          {/* Render all items */}
          <div className="container pe-5">
                <div className="row">
                {announcements.reverse().map((announcement) => (
                  <div className="col-12 addItmesCard" key={announcement.announcement_id}>
                    <div className="row">
                      <div className="col-md-8 p-0" style={{ display: 'flex', alignItems: 'center' }}>
                        {announcement.image_url && (
                          <img
                            className='p-0 m-0'
                            src={`http://localhost:5000/uploads/${encodeURIComponent(
                              announcement.image_url
                            )}`}
                            style={{ width: '50%', height: '80px', objectFit: 'cover', objectPosition: 'center' }}
                            alt="Announcement"
                          />
                        )}
                        <p className='ps-4' style={{ width: '50%' }}>{announcement.description}</p>
                      </div>
                      <div className="col-md-4" style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end', gap: '20px' }}>
                        <button className='edit-delete-btn bg-success' onClick={() => handleEditClick(announcement)}>
                          <i className="fa-solid fa-pencil"></i>
                        </button>
                        <button onClick={() => handleDeleteClick(announcement)} className='edit-delete-btn bg-danger'>
                          <i class="fa-solid fa-trash-can"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                ))}

                </div>
      
            </div>
            <ToastContainer position="bottom-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
              theme="colored" />
        </MainWrapperContainer>
      )
    
}

export default Announcement