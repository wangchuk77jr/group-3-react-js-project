import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Modal, Button, Form } from 'react-bootstrap';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer';

const CurryMenu = () => {
  const [menuItems, setMenuItems] = useState([]);
  const [showModal, setShowModal] = useState(false);
  const [menuItemData, setMenuItemData] = useState({
    menuId: '',
    menuName: '',
    menuImage: null,
    currentImage: null,
  });
  const [deleteConfirmationModal, setDeleteConfirmationModal] = useState(false);
  const [deleteMenuItemId, setDeleteMenuItemId] = useState('');

  // Fetch all menu items on component mount
  useEffect(() => {
    fetchMenuItems();
  }, []);

  const fetchMenuItems = async () => {
    try {
      const response = await axios.get('/menugallery');
      setMenuItems(response.data);
    } catch (error) {
      console.error('Error fetching menu items:', error);
    }
  };

  const handleInputChange = (event) => {
    if (event.target.name === 'menuImage') {
      setMenuItemData({
        ...menuItemData,
        menuImage: event.target.files[0],
        currentImage: URL.createObjectURL(event.target.files[0]),
      });
    } else {
      setMenuItemData({
        ...menuItemData,
        [event.target.name]: event.target.value,
      });
    }
  };

  const handleModalClose = () => {
    setShowModal(false);
    setMenuItemData({
      menuId: '',
      menuName: '',
      menuImage: null,
      currentImage: null,
    });
  };

  const handleModalOpen = () => {
    setShowModal(true);
  };

  const handleCreateMenuItem = async () => {
    try {
      const formData = new FormData();
      formData.append('menuName', menuItemData.menuName);
      formData.append('menuImage', menuItemData.menuImage);

      await axios.post('/menugallery', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });

      fetchMenuItems(); // Refresh menu items after creation
      handleModalClose();

      toast.success('Menu item created successfully');
    } catch (error) {
      console.error('Error creating menu item:', error);
      toast.error('Failed to create menu item');
    }
  };

  const handleEditMenuItem = async () => {
    try {
      const formData = new FormData();
      formData.append('menuName', menuItemData.menuName);
      formData.append('menuImage', menuItemData.menuImage);

      await axios.put(`/menugallery/${menuItemData.menuId}`, formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });

      fetchMenuItems(); // Refresh menu items after update
      handleModalClose();

      toast.success('Menu item updated successfully');
    } catch (error) {
      console.error('Error updating menu item:', error);
      toast.error('Failed to update menu item');
    }
  };

  const handleDeleteMenuItem = async () => {
    try {
      await axios.delete(`/menugallery/${deleteMenuItemId}`);

      fetchMenuItems(); // Refresh menu items after deletion

      toast.success('Menu item deleted successfully');

      setDeleteConfirmationModal(false);
      setDeleteMenuItemId('');
    } catch (error) {
      console.error('Error deleting menu item:', error);
      toast.error('Failed to delete menu item');
    }
  };

  const handleEditButtonClick = (menuItem) => {
    setMenuItemData({
      menuId: menuItem.menu_id,
      menuName: menuItem.menu_name,
      menuImage: menuItem.menu_image_url ? menuItem.menu_image_url : null,
      currentImage: menuItem.menu_image_url ? menuItem.menu_image_url : null,
    });
    handleModalOpen();
  };
  

  const handleDeleteButtonClick = (menuItem) => {
    setDeleteMenuItemId(menuItem.menu_id);
    setDeleteConfirmationModal(true);
  };

  const handleCloseDeleteConfirmationModal = () => {
    setDeleteConfirmationModal(false);
    setDeleteMenuItemId('');
  };

  return (
    <MainWrapperContainer>
      <div className='row' style={{ borderBottom: '2px solid green' }}>
        <div className="col-12 d-flex align-items-center w-100" style={{ justifyContent: 'space-between' }}>
          <div className=' d-flex align-items-center' style={{ overflowY: 'hidden' }}>
            <h5 className='p-2 ms-3 goBack__btn'><a href="/admin/home" style={{ overflowY: 'hidden' }}><i className="fa-solid fa-angles-left pe-2" style={{ position: 'relative', top: '3px', overflowY: 'hidden' }}></i>Go Back</a></h5>
            <h5 className='p-3  text-uppercase' style={{ fontWeight: "800" }}>Menu Gallery</h5>
          </div>
          <div className='pe-5' style={{ justifySelf: 'flex-end' }}>
            <button onClick={handleModalOpen} className='px-2' style={{ fontSize: '30px', textDecoration: 'none', color: '#fff', background: '#0CA91C', borderRadius: '100px' }}><i class="fa-solid fa-plus" style={{ position: 'relative', top: '3px' }}></i></button>
          </div>
        </div>
      </div>
      <Modal show={showModal} onHide={handleModalClose}>
        <Modal.Header className='bg-warning' closeButton>
          <Modal.Title style={{fontSize:'20px',fontWeight:'900'}}>{menuItemData.menuId ? 'Edit' : 'Add'} Menu Item</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form>
            <Form.Group>
              <Form.Label>Menu Name</Form.Label>
              <Form.Control
                type="text"
                name="menuName"
                value={menuItemData.menuName}
                onChange={handleInputChange}
              />
            </Form.Group>
            <Form.Group>
              <Form.Label>Menu Image</Form.Label>
              {menuItemData.currentImage && (
                <div style={{ marginBottom: '10px' }}>
                  <img
                    src={menuItemData.currentImage}
                    alt="Current Menu Image"
                    style={{ width: '300px', height: '200px' }}
                  />
                </div>
              )}
              <Form.Control
                type="file"
                accept="image/*"
                name="menuImage"
                onChange={handleInputChange}
              />
            </Form.Group>
          </Form>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleModalClose}>
            Cancel
          </Button>
          <Button className='px-5' variant="success" onClick={menuItemData.menuId ? handleEditMenuItem : handleCreateMenuItem}>
            {menuItemData.menuId ? 'Update' : 'Add'}
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal show={deleteConfirmationModal} onHide={handleCloseDeleteConfirmationModal}>
        <Modal.Header className='bg-warning' closeButton>
          <Modal.Title style={{fontSize:'20px',fontWeight:'900'}}>Confirm Deletion</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <p>Are you sure you want to delete this menu item?</p>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleCloseDeleteConfirmationModal}>
            Cancel
          </Button>
          <Button variant="danger" onClick={handleDeleteMenuItem}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>
      <div className="container pe-5">
        <div className="row mb-4">
          {menuItems.map((menuItem) => (
            <div className="col-12 addItmesCard" key={menuItem.menu_id}>
              <div className="row">
                <div className="col-md-8 p-0" style={{ display: 'flex', alignItems: 'center' }}>
                  {menuItem.menu_image_url && (
                    <img
                      className='p-0 m-0'
                      src={`http://localhost:5000/uploads/${encodeURIComponent(
                        menuItem.menu_image_url
                      )}`}
                      style={{ width: '50%', height: '80px', objectFit: 'cover', objectPosition: 'center' }}
                      alt="Announcement"
                    />
                  )}
                  <p className='ps-4' style={{ width: '50%' }}>{menuItem.menu_name}</p>
                </div>
                <div className="col-md-4" style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end', gap: '20px' }}>
                  <button className='edit-delete-btn bg-success' onClick={() => handleEditButtonClick(menuItem)}>
                    <i className="fa-solid fa-pencil"></i>
                  </button>
                  <button onClick={() => handleDeleteButtonClick(menuItem)} className='edit-delete-btn bg-danger'>
                    <i class="fa-solid fa-trash-can"></i>
                  </button>
                </div>
              </div>
            </div>
          ))}

        </div>

      </div>
      <ToastContainer position="bottom-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
              theme="colored" />
    </MainWrapperContainer>
  );
};

export default CurryMenu;
