import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Modal, Button, Form } from 'react-bootstrap';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer';
import './Home.css';

axios.defaults.baseURL = 'http://localhost:5000';

const MenuCoordination = () => {
  const [showModal, setShowModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [menuCoordinationList, setMenuCoordinationList] = useState([]);
  const [captainName, setCaptainName] = useState('');
  const [coordinationDay, setCoordinationDay] = useState('');
  const [profileImage, setProfileImage] = useState(null);
  const [editId, setEditId] = useState(null);
  const [deleteItemId, setDeleteItemId] = useState(null);

  useEffect(() => {
    getMenuCoordinations();
  }, []);

  const getMenuCoordinations = async () => {
    try {
      const response = await axios.get('/menucoordination');
      setMenuCoordinationList(response.data);
    } catch (error) {
      toast.error('Error fetching menu coordinations:', error);
    }
  };

  const handleCreate = async (event) => {
    event.preventDefault();

    const formData = new FormData();
    formData.append('captain_name', captainName);
    formData.append('coordination_day', coordinationDay);
    formData.append('profile_image', profileImage);

    try {
      await axios.post('/menucoordination', formData);
      getMenuCoordinations();
      setCaptainName('');
      setCoordinationDay('');
      setProfileImage(null);
      toast.success('Menu coordination added successfully');
      setShowModal(false);
    } catch (error) {
      toast.error('Error creating menu coordination:', error);
    }
  };

  const handleDelete = async (id) => {
    try {
      await axios.delete(`/menucoordination/${id}`);
      getMenuCoordinations();
      toast.success('Menu coordination deleted successfully');
      setShowDeleteModal(false);
    } catch (error) {
      toast.error('Error deleting menu coordination:', error);
    }
  };

  const handleEdit = async (event) => {
    event.preventDefault();

    const formData = new FormData();
    formData.append('captain_name', event.target.elements.captainName.value);
    formData.append('coordination_day', event.target.elements.coordinationDay.value);
    if (profileImage) {
      formData.append('profile_image', profileImage);
    }

    try {
      await axios.put(`/menucoordination/${editId}`, formData);
      getMenuCoordinations();
      setCaptainName('');
      setCoordinationDay('');
      setProfileImage(null);
      toast.success('Menu coordination updated successfully');
      setShowEditModal(false);
    } catch (error) {
      toast.error('Error updating menu coordination:', error);
    }
  };

  const handleModalOpen = () => {
    setShowModal(true);
  };

  const handleModalClose = () => {
    setShowModal(false);
  };

  const handleEditModalOpen = (coordination) => {
    setEditId(coordination.menu_coordination_id);
    setCaptainName(coordination.captain_name);
    setCoordinationDay(coordination.coordination_day);
    setShowEditModal(true);
  };

  const handleEditModalClose = () => {
    setEditId(null);
    setCaptainName('');
    setCoordinationDay('');
    setShowEditModal(false);
  };

  const handleDeleteModalOpen = (id) => {
    setDeleteItemId(id);
    setShowDeleteModal(true);
  };

  const handleDeleteModalClose = () => {
    setDeleteItemId(null);
    setShowDeleteModal(false);
  };

  const handleFileChange = (event) => {
    setProfileImage(event.target.files[0]);
  };

  return (
    <MainWrapperContainer>
        <div className='row' style={{borderBottom:'2px solid green'}}>
            <div className="col-12 d-flex align-items-center w-100" style={{justifyContent:'space-between'}}>
            <div className=' d-flex align-items-center' style={{overflowY:'hidden'}}>
              <h5 className='p-2 ms-3 goBack__btn'><a href="/admin/home" style={{overflowY:'hidden'}}><i className="fa-solid fa-angles-left pe-2" style={{position:'relative',top:'3px',overflowY:'hidden'}}></i>Go Back</a></h5>
              <h5 className='p-3  text-uppercase' style={{fontWeight:"800"}}>Menu Coordination</h5>
            </div>
              <div className='pe-5' style={{justifySelf:'flex-end'}}>
                <button  onClick={handleModalOpen} className='px-2' style={{fontSize:'30px',textDecoration:'none',color:'#fff',background:'#0CA91C',borderRadius:'100px'}}><i class="fa-solid fa-plus" style={{position:'relative',top:'3px'}}></i></button>
              </div>
            </div>
          </div>
      
        <Modal show={showModal} onHide={handleModalClose}>
          <Modal.Header className='bg-warning' closeButton>
            <Modal.Title style={{fontSize:'20px',fontWeight:'900'}}>Add Menu Coordination</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={handleCreate}>
              <Form.Group controlId="captainName">
                <Form.Label>Captain Name</Form.Label>
                <Form.Control
                  style={{ border: '1px solid #0CA91C' }}
                  type="text"
                  value={captainName}
                  onChange={(event) => setCaptainName(event.target.value)}
                  name="captainName"
                  placeholder="Enter captain name"
                  required
                />
              </Form.Group>
              <Form.Group controlId="coordinationDay">
                <Form.Label>Coordination Day</Form.Label>
                <Form.Control
                style={{ border: '1px solid #0CA91C' }}
                as="select"
                value={coordinationDay}
                onChange={(event) => setCoordinationDay(event.target.value)}
                name="coordinationDay"
                required
                >
              <option value="" disabled>----Select Day----</option>
              <option value="Monday">Monday</option>
              <option value="Tuesday">Tuesday</option>
              <option value="Wednesday">Wednesday</option>
              <option value="Thursday">Thursday</option>
              <option value="Friday">Friday</option>
              <option value="Saturday">Saturday</option>
              <option value="Sunday">Sunday</option>
            </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>Profile Image</Form.Label>
                <Form.Control
                  type="file"
                  accept=".jpg,.png,.jpeg"
                  onChange={handleFileChange}
                  required
                />
              </Form.Group>
              <div className='text-end'>
                    <Button className='mt-4 w-50' type="submit" style={{backgroundColor:'#0CA91C'}}>ADD</Button>
                </div>
            </Form>
          </Modal.Body>
        </Modal>
        <Modal show={showEditModal} onHide={handleEditModalClose}>
          <Modal.Header closeButton className='bg-warning'>
            <Modal.Title style={{fontSize:'20px',fontWeight:'900'}}>Edit Menu Coordination</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={handleEdit}>
              <Form.Group controlId="captainName">
                <Form.Label>Captain Name</Form.Label>
                <Form.Control
                  style={{ border: '1px solid #0CA91C' }}
                  type="text"
                  value={captainName}
                  onChange={(event) => setCaptainName(event.target.value)}
                  name="captainName"
                  placeholder="Enter captain name"
                  required
                />
              </Form.Group>
              <Form.Group controlId="coordinationDay">
                <Form.Label>Coordination Day</Form.Label>
                <Form.Control
                    style={{ border: '1px solid #0CA91C' }}
                    as="select"
                    value={coordinationDay}
                    onChange={(event) => setCoordinationDay(event.target.value)}
                    name="coordinationDay"
                    required
                    >
              <option value="" disabled>----Select Day----</option>
              <option value="Monday">Monday</option>
              <option value="Tuesday">Tuesday</option>
              <option value="Wednesday">Wednesday</option>
              <option value="Thursday">Thursday</option>
              <option value="Friday">Friday</option>
              <option value="Saturday">Saturday</option>
              <option value="Sunday">Sunday</option>
            </Form.Control>
              </Form.Group>
              <Form.Group>
                <Form.Label>New Profile Image</Form.Label>
                <Form.Control
                  type="file"
                  accept=".jpg,.png,.jpeg"
                  onChange={handleFileChange}
                />
              </Form.Group>
              <div className='text-end'>
                <Button className='mt-4 w-50' type="submit" style={{backgroundColor:'#0CA91C'}}>Update</Button>
                </div>
            </Form>
          </Modal.Body>
        </Modal>
        <Modal show={showDeleteModal} onHide={handleDeleteModalClose}>
          <Modal.Header closeButton className='bg-warning'>
            <Modal.Title style={{fontSize:'20px',fontWeight:'900'}}>Delete Coordination</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>Are you sure you want to delete this coordination?</p>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleDeleteModalClose}>
              Cancel
            </Button>
            <Button
              variant="danger"
              onClick={() => handleDelete(deleteItemId)}
            >
              Delete
            </Button>
          </Modal.Footer>
        </Modal>   
        <div className="container pe-5">
                <div className="row mb-4">
                {menuCoordinationList.map((coordination) => (
                  <div className="col-12 addItmesCard"key={coordination.menu_coordination_id}>
                    <div className="row">
                      <div className="col-md-8 p-0" style={{ display: 'flex', alignItems: 'center' }}>
                        {coordination.profile_image_url && (
                          <img
                            className='p-0 m-0'
                            src={`http://localhost:5000/uploads/${encodeURIComponent(
                              coordination.profile_image_url
                            )}`}
                            style={{ width: '50%', height: '80px', objectFit: 'cover', objectPosition: 'center' }}
                            alt="profile"
                          />
                        )}
                        <p className='ps-4' style={{ width: '25%' }}>{coordination.coordination_day}</p>
                        <p className='ps-4' style={{ width: '25%' }}>{coordination.captain_name}</p>
                      </div>
                      <div className="col-md-4" style={{ display: 'flex', alignItems: 'center', justifyContent: 'flex-end', gap: '20px' }}>
                        <button className='edit-delete-btn bg-success' onClick={() => handleEditModalOpen(coordination)}>
                          <i className="fa-solid fa-pencil"></i>
                        </button>
                        <button onClick={() => handleDeleteModalOpen(coordination.menu_coordination_id)} className='edit-delete-btn bg-danger'>
                          <i class="fa-solid fa-trash-can"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                ))}
                </div>
            </div>
            <ToastContainer position="bottom-right"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
              theme="colored" />
     
    </MainWrapperContainer>
  );
};

export default MenuCoordination;
