import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { Modal, Button, Form } from 'react-bootstrap';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer';
import './Home.css';

axios.defaults.baseURL = 'http://localhost:5000';

const Rules = () => {
  const [showModal, setShowModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [ruleTitle, setRuleTitle] = useState('');
  const [ruleDescription, setRuleDescription] = useState('');
  const [currentRule, setCurrentRule] = useState(null);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);

  // Fetch rules from the server
  const [rules, setRules] = useState([]);
  useEffect(() => {
    fetchRules();
  }, []);

  const fetchRules = async () => {
    try {
      const response = await axios.get('/rules');
      setRules(response.data);
    } catch (error) {
      console.error('Error fetching rules:', error);
    }
  };

  const handleFormSubmit = async (event) => {
    event.preventDefault();

    try {
      await axios.post('/rules', {
        rule_title: ruleTitle,
        rule_description: ruleDescription,
      });

      setRuleTitle('');
      setRuleDescription('');
      setShowModal(false);

      fetchRules();

      toast.success('Rule added successfully');
    } catch (error) {
      console.error('Error creating rule:', error);
      setShowModal(false);
      toast.error('An error occurred while creating the rule');
    }
  };

  const handleEditFormSubmit = async (event) => {
    event.preventDefault();

    try {
      const { rule_id } = currentRule;

      await axios.put(`/rules/${rule_id}`, {
        rule_title: ruleTitle,
        rule_description: ruleDescription,
      });

      setRuleTitle('');
      setRuleDescription('');
      setShowEditModal(false);

      fetchRules();

      toast.success('Rule updated successfully');
    } catch (error) {
      console.error('Error updating rule:', error);
      toast.error('An error occurred while updating the rule');
    }
  };

  const handleDeleteConfirm = async () => {
    try {
      const { rule_id } = currentRule;

      await axios.delete(`/rules/${rule_id}`);

      setDeleteModalOpen(false);

      fetchRules();

      toast.success('Rule deleted successfully');
    } catch (error) {
      console.error('Error deleting rule:', error);
      toast.error('An error occurred while deleting the rule');
    }
  };

  const handleDeleteCancel = () => {
    setDeleteModalOpen(false);
  };

  const handleModalOpen = () => {
    setShowModal(true);
  };

  const handleModalClose = () => {
    setShowModal(false);
  };

  const handleEditModalClose = () => {
    setShowEditModal(false);
  };

  const handleEditClick = (rule) => {
    setCurrentRule(rule);
    setRuleTitle(rule.rule_title);
    setRuleDescription(rule.rule_description);
    setShowEditModal(true);
  };

  const handleDeleteClick = (rule) => {
    setCurrentRule(rule);
    setDeleteModalOpen(true);
  };

  return (
    <MainWrapperContainer>
      <div className='row' style={{ borderBottom: '2px solid green' }}>
        <div className="col-12 d-flex align-items-center w-100" style={{ justifyContent: 'space-between' }}>
          <div className=' d-flex align-items-center' style={{ overflowY: 'hidden' }}>
            <h5 className='p-2 ms-3 goBack__btn'><a href="/admin/home" style={{ overflowY: 'hidden' }}><i className="fa-solid fa-angles-left pe-2" style={{ position: 'relative', top: '3px', overflowY: 'hidden' }}></i>Go Back</a></h5>
            <h5 className='p-3  text-uppercase' style={{ fontWeight: "800" }}>Rules and Regulations</h5>
          </div>
          <div className='pe-5' style={{ justifySelf: 'flex-end' }}>
            <button onClick={handleModalOpen} className='px-2' style={{ fontSize: '30px', textDecoration: 'none', color: '#fff', background: '#0CA91C', borderRadius: '100px' }}><i className="fa-solid fa-plus" style={{ position: 'relative', top: '3px' }}></i></button>
          </div>
        </div>
      </div>
      {/* display rules */}
      <div className='row' style={{ padding: '20px' }}>
        <div className="col-12">
            <div className="table-responsive" style={{ boxShadow: '0px 2px 4px rgb(0 0 0 / 8%)' }}>
            <table className="table table-hover">
                <thead>
                <tr>
                    <th className="col">Rule Title</th>
                    <th className="col w-50">Rule Descriptions</th>
                    <th className="col">Actions</th>
                </tr>
                </thead>
                <tbody>
                {rules.map((rule) => (
                    <tr key={rule.rule_id}>
                    <td className="col">{rule.rule_title}</td>
                    <td className="col">{rule.rule_description}</td>
                    <td className="col">
                        <button onClick={() => handleEditClick(rule)} className='btn btn-success me-3'><i className="fa-solid fa-pencil"></i></button>
                        <button onClick={() => handleDeleteClick(rule)}className='btn btn-danger'><i className="fa-solid fa-trash"></i></button>
                    </td>
                    </tr>
                ))}
                </tbody>
            </table>
            </div>
        </div>
        </div>

   
    

      {/* Add Rule Modal */}
      <Modal show={showModal} onHide={handleModalClose}>
        <Modal.Header closeButton className='bg-warning'>
          <Modal.Title style={{fontSize:'20px',fontWeight:'900'}}>Add Rules & Regulations</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={handleFormSubmit}>
            <Form.Group controlId="ruleTitle">
              <Form.Label>Title</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter rule title"
                value={ruleTitle}
                onChange={(event) => setRuleTitle(event.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="ruleDescription">
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={10}
                placeholder="Enter rule description"
                value={ruleDescription}
                onChange={(event) => setRuleDescription(event.target.value)}
              />
            </Form.Group>
            <div className='text-end'>
                     <Button className='mt-4 w-50' type="submit" style={{backgroundColor:'#0CA91C'}}>ADD</Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      {/* Edit Rule Modal */}
      {currentRule && (
        <Modal show={showEditModal} onHide={handleEditModalClose}>
          <Modal.Header closeButton className='bg-warning'>
            <Modal.Title style={{fontSize:'20px',fontWeight:'900'}}>Edit Rules & Regulations</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Form onSubmit={handleEditFormSubmit}>
              <Form.Group controlId="ruleTitle">
                <Form.Label>Title</Form.Label>
                <Form.Control
                  type="text"
                  placeholder="Enter rule title"
                  value={ruleTitle}
                  onChange={(event) => setRuleTitle(event.target.value)}
                />
              </Form.Group>
              <Form.Group controlId="ruleDescription">
                <Form.Label>Description</Form.Label>
                <Form.Control
                  as="textarea"
                  rows={10}
                  placeholder="Enter rule description"
                  value={ruleDescription}
                  onChange={(event) => setRuleDescription(event.target.value)}
                />
              </Form.Group>
              <div className='text-end'>
                     <Button className='mt-4 w-50' type="submit" style={{backgroundColor:'#0CA91C'}}>UPDATE</Button>
                </div>
            </Form>
          </Modal.Body>
        </Modal>
      )}

      {/* Delete Rule Confirmation Modal */}
      {currentRule && (
        <Modal show={deleteModalOpen} onHide={handleDeleteCancel}>
          <Modal.Header closeButton className='bg-warning'>
            <Modal.Title style={{fontSize:'20px',fontWeight:'900'}}>Delete Rule</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            Are you sure you want to delete the rule <strong>{currentRule.rule_title}</strong>?
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleDeleteCancel}>
              Cancel
            </Button>
            <Button variant="danger" onClick={handleDeleteConfirm}>
              Delete
            </Button>
          </Modal.Footer>
        </Modal>
      )}
    <ToastContainer position="bottom-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored" />
    </MainWrapperContainer>
  );
};

export default Rules;
