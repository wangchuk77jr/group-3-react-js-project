import React, { useState, useEffect } from 'react';
import axios from 'axios';
import MainWrapperContainer from '../../Component/Home/MainWrapperContainer';
import { Modal, Button, Form } from 'react-bootstrap';
import { toast, ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const MenuPlan = () => {
  const [menuPlans, setMenuPlans] = useState([]);
  const [menuDay, setMenuDay] = useState('');
  const [menuBf, setMenuBf] = useState('');
  const [menuLunch, setMenuLunch] = useState('');
  const [menuDinner, setMenuDinner] = useState('');
  const [editingMenuPlan, setEditingMenuPlan] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [showDeleteModal, setShowDeleteModal] = useState(false);
  const [deletingMenuPlanId, setDeletingMenuPlanId] = useState(null);

  useEffect(() => {
    fetchMenuPlans();
  }, []);

  const fetchMenuPlans = async () => {
    try {
      const response = await axios.get('/menuplan');
      setMenuPlans(response.data);
    } catch (error) {
      toast.error('Error fetching menu plans');
    }
  };

  const addMenuPlan = async (e) => {
    e.preventDefault();
  
    try {
      const newMenuPlan = {
        menu_day: menuDay,
        menu_bf: menuBf,
        menu_lunch: menuLunch,
        menu_dinner: menuDinner,
      };
      const response = await axios.post('/menuplan', newMenuPlan);
      setMenuPlans([...menuPlans, response.data]);
      setMenuDay('');
      setMenuBf('');
      setMenuLunch('');
      setMenuDinner('');
      setShowModal(false);
  
      toast.success(`Menu plan "${response.data.menu_day}" added successfully!`);
    } catch (error) {
      console.error('Error adding menu plan:', error);
      setShowModal(false);
      toast.error(`Menu plan for ${menuDay} already existed!`);
    }
  };

  const deleteMenuPlan = async (id) => {
    try {
      await axios.delete(`/menuplan/${id}`);
      const updatedMenuPlans = menuPlans.filter((plan) => plan.menuplan_id !== id);
      setMenuPlans(updatedMenuPlans);
      setShowDeleteModal(false);
      setDeletingMenuPlanId(null);
      toast.success('Menu plan deleted successfully!');
    } catch (error) {
      console.error('Error deleting menu plan:', error);
      toast.error('Failed to delete menu plan.');
      setShowModal(false);
    }
  };

  const editMenuPlan = (plan) => {
    setEditingMenuPlan(plan);
    setMenuDay(plan.menu_day);
    setMenuBf(plan.menu_bf);
    setMenuLunch(plan.menu_lunch);
    setMenuDinner(plan.menu_dinner);
    setShowModal(true);
  };

  const updateMenuPlan = async (e) => {
    e.preventDefault();
  
    try {
      const updatedMenuPlan = {
        menu_day: menuDay,
        menu_bf: menuBf,
        menu_lunch: menuLunch,
        menu_dinner: menuDinner,
      };
      const response = await axios.put(`/menuplan/${editingMenuPlan.menuplan_id}`, updatedMenuPlan);
      const updatedMenuPlans = menuPlans.map((plan) =>
        plan.menuplan_id === response.data.menuplan_id ? response.data : plan
      );
      setMenuPlans(updatedMenuPlans);
      setEditingMenuPlan(null);
      setMenuDay('');
      setMenuBf('');
      setMenuLunch('');
      setMenuDinner('');
      setShowModal(false);
  
      toast.success(`Menu plan "${response.data.menu_day}" updated successfully!`);
    } catch (error) {
      console.error('Error updating menu plan:', error);
      setShowModal(false);
      toast.error('Failed to update menu plan.');
    }
  };

  const confirmDelete = (id) => {
    setShowDeleteModal(true);
    setDeletingMenuPlanId(id);
  };

  const cancelDelete = () => {
    setShowDeleteModal(false);
    setDeletingMenuPlanId(null);
  };

  return (
    <MainWrapperContainer>
      <div className='row' style={{ borderBottom: '2px solid green' }}>
        <div className="col-12 d-flex align-items-center w-100" style={{ justifyContent: 'space-between' }}>
          <div className=' d-flex align-items-center' style={{ overflowY: 'hidden' }}>
            <h5 className='p-3  text-uppercase' style={{ fontWeight: "800" }}>Meal Plan</h5>
          </div>
          <div className='pe-5' style={{ justifySelf: 'flex-end' }}>
            <button onClick={() => setShowModal(true)} className='px-2' style={{ fontSize: '30px', textDecoration: 'none', color: '#fff', background: '#0CA91C', borderRadius: '100px' }}><i className="fa-solid fa-plus" style={{ position: 'relative', top: '3px' }}></i></button>
          </div>
        </div>
      </div>
      <Modal show={showModal} onHide={() => setShowModal(false)}>
        <Modal.Header className='bg-warning' closeButton>
          <Modal.Title style={{fontSize:'20px',fontWeight:'900'}}>{editingMenuPlan ? 'Edit Menu Plan' : 'Add Menu Plan'}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <Form onSubmit={editingMenuPlan ? updateMenuPlan : addMenuPlan}>
            <Form.Group controlId="formMenuDay">
              <Form.Label>Menu Day</Form.Label>
              <Form.Control
                required
                as="select"
                value={menuDay}
                onChange={(e) => setMenuDay(e.target.value)}
              >
                <option disabled value="">----Select Day----</option>
                <option value="Monday">Monday</option>
                <option value="Tuesday">Tuesday</option>
                <option value="Wednesday">Wednesday</option>
                <option value="Thursday">Thursday</option>
                <option value="Friday">Friday</option>
                <option value="Saturday">Saturday</option>
                <option value="Sunday">Sunday</option>
              </Form.Control>
            </Form.Group>
            <Form.Group controlId="formMenuBf">
              <Form.Label>Breakfast</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="Enter breakfast menu"
                value={menuBf}
                onChange={(e) => setMenuBf(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="formMenuLunch">
              <Form.Label>Lunch</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="Enter lunch menu"
                value={menuLunch}
                onChange={(e) => setMenuLunch(e.target.value)}
              />
            </Form.Group>
            <Form.Group controlId="formMenuDinner">
              <Form.Label>Dinner</Form.Label>
              <Form.Control
                required
                type="text"
                placeholder="Enter dinner menu"
                value={menuDinner}
                onChange={(e) => setMenuDinner(e.target.value)}
              />
            </Form.Group>
           
            <div className='text-end mt-4'>
            <Button className='px-5' style={{paddingInline:'20px'}} variant="success" type="submit">
              {editingMenuPlan ? 'Update' : 'Add'}
            </Button>
            </div>
          </Form>
        </Modal.Body>
      </Modal>

      <Modal show={showDeleteModal} onHide={cancelDelete}>
        <Modal.Header className='bg-warning' closeButton>
          <Modal.Title style={{fontSize:'20px',fontWeight:'900'}}>Delete Menu Plan</Modal.Title>
        </Modal.Header>
        <Modal.Body>Are you sure you want to delete this menu plan?</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={cancelDelete}>
            Cancel
          </Button>
          <Button variant="danger" onClick={() => deleteMenuPlan(deletingMenuPlanId)}>
            Delete
          </Button>
        </Modal.Footer>
      </Modal>

      <div className='row' style={{ padding: '20px' }}>
        <div className="col-12">
          <div className="table-responsive" style={{ boxShadow: '0px 2px 4px rgb(0 0 0 / 8%)' }}>
            <table className="table table-hover">
              <thead>
                <tr>
                  <th>Day</th>
                  <th>Breakfast</th>
                  <th>Lunch</th>
                  <th>Dinner</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
                {menuPlans.map((plan) => (
                  <tr key={plan.menuplan_id}>
                    <td>{plan.menu_day}</td>
                    <td>{plan.menu_bf}</td>
                    <td>{plan.menu_lunch}</td>
                    <td>{plan.menu_dinner}</td>
                    <td>
                      <button onClick={() => editMenuPlan(plan)} className='btn btn-success me-3'><i className="fa-solid fa-pencil"></i></button>
                      <button onClick={() => confirmDelete(plan.menuplan_id)} className='btn btn-danger'><i className="fa-solid fa-trash"></i></button>
                    </td>
                  </tr>
                ))}
              </tbody>
            </table>
          </div>
        </div>
      </div>
      <ToastContainer position="bottom-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored" 
        style={{ width: "400px",padding:'10px' }}
        />
    </MainWrapperContainer>
  );
};

export default MenuPlan;
