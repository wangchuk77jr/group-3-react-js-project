import React, { useState } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Navbar from './Components/Layout/Header/Navbar';
import Home from './Components/Pages/Home/Home';
import About from './Components/Pages/About/About';
import Contact from './Components/Pages/Contact/Contact';
import CleaningSchedule from './Components/Pages/CleaningSchedule/CleaningSchedule';
import Feedback from './Components/Pages/Feedback/Feedback';
import GroceryItem from './Components/Pages/GroceryItems/GroceryItem';
import MenuPlan from './Components/Pages/MenuPlan/MenuPlan';
import Stipend from './Components/Pages/Stipend/Stipend';
import Login from './Components/Pages/LoginSignUp/Login';
import SignUp from './Components/Pages/LoginSignUp/SignUp';
import Footer from './Components/Layout/Footer/Footer';
import NotFound from './Components/Pages/NotFound/NotFound';
import GoToTopButton from './Components/Common/GoToTopButton';

const App = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(sessionStorage.getItem('token')? true : false);

  const handleLoginSuccess = () => {
    setIsLoggedIn(true);
  };

  return (
    <Router>
      <div>
        <Navbar isLoggedIn={isLoggedIn} />
        <Switch>
          <Route exact path='/' component={Home} />
          <Route path="/about" component={About} />
          <Route path="/contact" component={Contact} />
          <Route path="/cleaning-schedule" component={CleaningSchedule} />
          <Route path="/feedback" component={Feedback} />
          <Route path="/grocery-items" component={GroceryItem} />
          <Route path="/menu-plan" component={MenuPlan} />
          <Route path="/stipend" component={Stipend} />
          <Route
            path="/login"
            render={(props) => <Login {...props} onLoginSuccess={handleLoginSuccess} />}
          />
          <Route path="/signup" component={SignUp} />
          <Route path="/*" component={NotFound} />
        </Switch>
        <GoToTopButton />
        <Footer />
      </div>
    </Router>
  );
};

export default App;




// import React from 'react';
// import { BrowserRouter as Router, Switch, Route} from 'react-router-dom';
// import Navbar from './Components/Layout/Header/Navbar';
// import Home from './Components/Pages/Home/Home';
// import About from './Components/Pages/About/About';
// import Contact from './Components/Pages/Contact/Contact';
// import CleaningSchedule from './Components/Pages/CleaningSchedule/CleaningSchedule';
// import Feedback from './Components/Pages/Feedback/Feedback';
// import GroceryItem from './Components/Pages/GroceryItems/GroceryItem';
// import MenuPlan from './Components/Pages/MenuPlan/MenuPlan';
// import Stipend from './Components/Pages/Stipend/Stipend';
// import Login from './Components/Pages/LoginSignUp/Login';
// import SignUp from './Components/Pages/LoginSignUp/SignUp';
// import Footer from './Components/Layout/Footer/Footer';
// import NotFound from './Components/Pages/NotFound/NotFound';
// import GoToTopButton from './Components/Common/GoToTopButton';

// const App = () => {
//   return (
//     <Router>
//       <div>
//         <Navbar/>
//         <Switch>
//           <Route exact path='/' component={Home}/>
//           <Route path="/about" component={About} />
//           <Route path="/contact" component={Contact}/>
//           <Route path="/cleaning-schedule" component={CleaningSchedule}/>
//           <Route path="/feedback" component={Feedback}/>
//           <Route path="/grocery-items" component={GroceryItem}/>
//           <Route path="/menu-plan" component={MenuPlan}/>
//           <Route path="/stipend" component={Stipend}/>
//           <Route path="/login" component={Login}/>
//           <Route path="/signup" component={SignUp}/>
//           <Route path="/*" component={NotFound}/>
//         </Switch>
//         <GoToTopButton/>
//         <Footer/>
//       </div>
//     </Router>
//   );
// };

// export default App;

