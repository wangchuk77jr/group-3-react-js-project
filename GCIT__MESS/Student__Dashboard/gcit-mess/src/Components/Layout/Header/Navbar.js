import React from 'react';
import { Link } from 'react-router-dom';
import logo from '../../../Assets/Images/gm.png';
import { toast } from 'react-toastify';
import './navbar.css';
const logout = async e => {
  e.preventDefault();
  try {
    sessionStorage.removeItem('token')
    toast.success("Logout successfully");
    window.location.assign("http://localhost:3000")


  } catch (err) {
    console.error(err.message);
  }
};
const Navbar = ({ isLoggedIn }) => {
  return (
    <div className='navbar-section'>
      <nav className="navbar navbar-expand-lg">
        <div className="container px-md-5">
          <Link className="navbar-brand" to="/">
             <img src={logo} alt="GCIT MESS Logo" width={150} height={60}/>
          </Link>
          <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavDropdown">
            <ul className="navbar-nav ms-auto d-md-flex gap-md-2">
              <li className="nav-item hover-effect">
                <Link className="nav-link" to="/menu-plan">Menu Plan</Link>
              </li>
              <li className="nav-item hover-effect">
                <Link className="nav-link" to="/grocery-items">Grocery Item</Link>
              </li>
              <li className="nav-item hover-effect">
                <Link className="nav-link" to="/feedback">Feedback</Link>
              </li>
              <li className="nav-item dropdown">
                <Link className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                  Others
                </Link>
                <ul className="dropdown-menu">
                  <li><Link className="dropdown-item" to="/contact">Contact</Link></li>
                  <li><Link className="dropdown-item" to="/about">About</Link></li>
                  <li><Link className="dropdown-item" to="/stipend">About Stipend</Link></li>
                  <li><Link className="dropdown-item" to="/cleaning-schedule">Cleaning Schedule</Link></li>
                </ul>
              </li>
              <li className="nav-item text-center nav-link">
                {isLoggedIn ? (
                  <div className="ms-md">
                      <button id='logoutBtn'  data-bs-toggle="modal" data-bs-target="#exampleModal" >
                      Log out
                      </button>
                      <div className="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div className="modal-dialog">
                          <div className="modal-content">
                            <div className="modal-header">
                              <h5 className="modal-title ms-4" id="exampleModalLabel" style={{color:'#000'}}>Logout Confirmation</h5>
                              <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                            </div>
                            <div className="modal-body" style={{color:'#000',fontSize:'16px',fontWeight:'400'}}>
                              Are <span className='text-lowercase'> you sure you want to logout from GCIT Mess?</span>
                            </div>
                            <div className="modal-footer">
                              <button type="button" className="btn btn-secondary" data-bs-dismiss="modal">Cancel</button>
                              <button type="button" className="btn" style={{background:'rgb(252,152,69)',width:'200px',color:'#fff'}} onClick={logout}>Confirm</button>
                            </div>
                          </div>
                        </div>
                      </div>
                      {console.log(isLoggedIn)}
                    </div>
                    
                ) : (
                  <Link className="ms-md login-btn px-4 py-2" to="/login">Login</Link>
                )}
              </li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;





// import React from 'react';
// import { Link } from 'react-router-dom';
// import logo from '../../../Assets/Images/gm.png'
// import './navbar.css'
// const Navbar = () => {
//   return (
//     <div className='navbar-section'>
//       <nav className="navbar navbar-expand-lg">
//         <div className="container px-md-5">
//           <Link className="navbar-brand" to="/">
//              <img src={logo} alt="GCIT MESS Logo" width={150} height={60}/>
//           </Link>
//           <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
//             <span className="navbar-toggler-icon"></span>
//           </button>
//           <div className="collapse navbar-collapse" id="navbarNavDropdown">
//             <ul className="navbar-nav ms-auto d-md-flex gap-md-2">
//               <li className="nav-item hover-effect">
//                 <Link className="nav-link " to="/menu-plan">Menu Plan</Link>
//               </li>
//               <li className="nav-item hover-effect">
//                 <Link className="nav-link" to="/grocery-items">Grocery Item</Link>
//               </li>
//               <li className="nav-item hover-effect">
//                 <Link className="nav-link" to="/feedback">Feedback</Link>
//               </li>
//               <li className="nav-item dropdown">
//                 <Link className="nav-link dropdown-toggle" role="button" data-bs-toggle="dropdown" aria-expanded="false">
//                   Others
//                 </Link>
//                 <ul className="dropdown-menu">
//                   <li><Link className="dropdown-item" to="/contact">Contact</Link></li>
//                   <li><Link className="dropdown-item" to="/about">About</Link></li>
//                   <li><Link className="dropdown-item" to="/stipend">About Stipend</Link></li>
//                   <li><Link className="dropdown-item" to="/cleaning-schedule">Cleaning Schedule</Link></li>
//                 </ul>
//               </li>
//               <li className="nav-item text-center nav-link ">
//                 <Link className="ms-md- login-btn px-4 py-2 " to="/login">Login</Link>
//               </li>
//             </ul>
//           </div>
//         </div>
//       </nav>
//     </div>
//   );
// };

// export default Navbar;

