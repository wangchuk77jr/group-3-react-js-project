import React from 'react'
import './footer.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRightLong,faMessage,faPhone} from '@fortawesome/free-solid-svg-icons'
const Footer = () => {
  return (
    <>
    <section className="footer">
    <footer>
    <div className="container-lg px-md-5">
      <div className="row pb-5 pt-3">
            <div className="col-xl-4 col-md-4 col-sm-6 mb-3">
                <h6 className=" text-capitilize pb-1">About</h6>
                <p style={{width: '80%',fontSize: '15px',lineHeight: '2rem',color:'#fff'}}>
                We provide clean, comfortable, and diverse dining with nutritious and tasty 
                food options while maintaining high hygiene and safety standards.
                 We strive to continuously improve our service based on student feedback.
                </p>
            </div>
            <div className="col-xl-4 col-md-4 col-sm-6 mb-3">
                <h6 className="text-capitalize pb-1 ">Quick Links</h6>
                <div className="d-flex flex-column flex-wrap gap-2">
                    <div>
                        <span> <FontAwesomeIcon icon={faArrowRightLong} style={{color:'#fff'}}/></span>
                        <a href="/about" className='ps-2'>About</a>
                    </div>
                    <div>
                    <span> <FontAwesomeIcon icon={faArrowRightLong} style={{color:'#fff'}}/></span>
                        <a href="/contact" className='ps-2'>Contact</a>
                    </div>
                    <div>
                        <span> <FontAwesomeIcon icon={faArrowRightLong} style={{color:'#fff'}}/></span>
                        <a href="/feedback" className='ps-2'>Feedback</a>
                    </div>
                    <div>
                        <span> <FontAwesomeIcon icon={faArrowRightLong} style={{color:'#fff'}}/></span>
                        <a href="/menu-plan" className='ps-2'>Menu Plan</a>
                    </div>
                    <div>
                        <span> <FontAwesomeIcon icon={faArrowRightLong} style={{color:'#fff'}}/></span>
                        <a href="/stipend" className='ps-2'>Stipend</a>
                    </div>
                    <div>
                        <span> <FontAwesomeIcon icon={faArrowRightLong} style={{color:'#fff'}}/></span>
                        <a href="/cleaning-schedule" className='ps-2'>Dinning Cleaning</a>
                    </div>
                    <div>
                        <span> <FontAwesomeIcon icon={faArrowRightLong} style={{color:'#fff'}}/></span>
                        <a href="/grocery-items" className='ps-2'>Groceries Item</a>
                    </div>
                </div>
            </div>
            <div className="col-xl-4 col-md-4 col-sm-6 mb-3">
                <h6 className="text-capitalize pb-1">Contact Mess Incharge</h6>
                <div className="d-flex flex-column flex-wrap gap-2">
                    <div>
                        <span style={{fontSize: '16px',color:"#fff"}}>Ms. Sangay Lhaden</span>
                    </div>
                    <div>
                        <span> <FontAwesomeIcon icon={faPhone} style={{color:'#fff'}}/></span>
                        <a className='ps-2' href="tel:+97517337528">+975 17337538</a>
                    </div>
                    <div>
                        <span> <FontAwesomeIcon icon={faMessage} style={{color:'#fff'}}/></span>
                        <a className='ps-2' href="mailto:sangay.gcit@rub.edu.bt">sangay.gcit@rub.edu.bt</a>
                    </div>
                </div>
            </div>
            <div className="row pt-5">
                <hr style={{height: '2px',backgroundColor: '#eee',border:"0px"}}/>
                <p className="text-center pt-3" style={{fontSize: '16px',color:'#fff'}}>Copyright © 2023 | GCIT MESS | All Rights Reserved</p>
            </div>
        </div>
    </div>
    </footer>
    <div className='chatGPT'></div>
  </section>
  </>
  )
}

export default Footer