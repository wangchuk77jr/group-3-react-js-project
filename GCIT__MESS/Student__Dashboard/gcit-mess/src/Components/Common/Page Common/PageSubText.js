import React from 'react'
const PageSubText = ({PageSubText}) => {
  return (
    <>
        <p style={{width:'60%'}}>
            {PageSubText}
        </p>
    </>
  )
}

export default PageSubText