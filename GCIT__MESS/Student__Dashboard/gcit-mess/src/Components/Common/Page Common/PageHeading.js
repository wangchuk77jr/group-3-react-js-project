import React from 'react'

const PageHeading = ({PageHeading}) => {
  return (
    <>
       <h5 className='text-uppercase mt-4' style={{fontWeight:'900'}}>{PageHeading}</h5>
    </>
  )
}

export default PageHeading