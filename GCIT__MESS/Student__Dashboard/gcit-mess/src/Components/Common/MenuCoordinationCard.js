import React from 'react'
import './MenuCoordinationCard.css'

const MenuCoordinationCard = ({profile_img,name,day}) => {
  return (
    <div class="coordination-card">
        <div class="coordination-card-img">
            <img src = {profile_img} alt={profile_img}/>
        </div>
        <div class="card-content pt-2">
         <h5 className='text-capitalize' style={{fontSize:'17px',fontWeight:'900'}}>{day}</h5>
         <h6 className='text-capitalize' >{name}</h6>
        </div>
    </div>
  )
}

export default MenuCoordinationCard