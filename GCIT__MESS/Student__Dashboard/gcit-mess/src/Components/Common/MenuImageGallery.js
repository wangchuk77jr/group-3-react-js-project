import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './menuGallery.css';

const MenuImageGallery = () => {
  const [menuItems, setMenuItems] = useState([]);
  const [loadedItemCount, setLoadedItemCount] = useState(8);
  const originalItemCount = 8;

  // Fetch all menu items on component mount
  useEffect(() => {
    fetchMenuItems();
  }, []);

  const fetchMenuItems = async () => {
    try {
      const response = await axios.get('/menugallery');
      setMenuItems(response.data);
    } catch (error) {
      console.error('Error fetching menu items:', error);
    }
  };

  const handleLoadMore = () => {
    setLoadedItemCount(menuItems.length);
  };

  const handleLoadLess = () => {
    setLoadedItemCount(originalItemCount);
  };

  return (
    <div className='row'>
      {menuItems.slice(0, loadedItemCount).map((menuItem) => (
        <div className='col-md-3 mt-4' key={menuItem.menu_id}>
          <div className='menu-item'>
            <img
              src={`http://localhost:5000/uploads/${encodeURIComponent(
                menuItem.menu_image_url
              )}`}
              alt={menuItem.menu_name}
              className='menu-image'
            />
            <h6 className='menu-name'>{menuItem.menu_name}</h6>
          </div>
        </div>
      ))}
      {loadedItemCount < menuItems.length && (
        <div className='col-12 text-center mt-5 mb-4'>
          <button className='btn w-25 p-3' style={{backgroundColor:'#FC9845',fontWeight:'800',color:'#eee'}} onClick={handleLoadMore}>
            Load More
          </button>
        </div>
      )}
      {loadedItemCount > originalItemCount && (
        <div className='col-12 text-center mt-5 mb-4'>
          <button className='btn w-25 p-3' style={{backgroundColor:'#FC9845',fontWeight:'800',color:'#eee'}} onClick={handleLoadLess}>
            Load Less
          </button>
        </div>
      )}
    </div>
  );
};

export default MenuImageGallery;
