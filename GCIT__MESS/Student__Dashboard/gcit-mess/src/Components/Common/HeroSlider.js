import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './slider.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faRightLong, faLeftLong } from '@fortawesome/free-solid-svg-icons'

axios.defaults.baseURL = 'http://localhost:5000';

const HeroSlider = () => {
  const [announcements, setAnnouncements] = useState([]);

  // Fetch announcements from the server
  useEffect(() => {
    fetchAnnouncements();
  }, []);

  const fetchAnnouncements = async () => {
    try {
      const response = await axios.get('/announcements');
      setAnnouncements(response.data);
    } catch (error) {
      console.error('Error fetching announcements:', error);
    }
  };

  return (
    <>
      <div id="myCarousel" className="carousel slide position-relative pt-4 pe-1" data-bs-ride="carousel" data-bs-interval="3000">
        <div className="carousel-inner pb-3">
          {announcements.map((announcement, index) => (
            <div  className={`carousel-item ${index === 0 ? 'active' : ''}`} key={announcement.announcement_id} >
              <div className="row">
                <div className="col-md-6 d-flex align-items-center">
                {announcement.description.length>100 ? (
                  <h2 className='hero-heading__2'>{announcement.description}</h2>
                ) : (
                  <h2 className="hero-heading">{announcement.description}</h2>
                 )}
                </div>
                <div className="col-md-6" style={{ height: '65vh' }}>
                  {announcement.image_url && (
                    <img
                      className='SliderImage'
                      src={`http://localhost:5000/uploads/${encodeURIComponent(
                        announcement.image_url
                      )}`}
                      alt="Slide 1" style={{ objectFit: 'cover',objectPosition:'center', width: '100%', height: '100%', borderRadius: '5px' }}
                    />
                  )}
                </div>
              </div>
            </div>
          ))}
        </div>

        <ol className="carousel-indicators mt-5">
          <li className='slider-btn mx-3'>
            <a className="carousel-control" href="#myCarousel" role="button" data-bs-slide="prev">
              <FontAwesomeIcon icon={faLeftLong} />
              <span className="visually-hidden">Previous</span>
            </a>
          </li>
          {announcements.map((_, index) => (
            <li key={index} data-bs-target="#myCarousel" data-bs-slide-to={index} className={index === 0 ? 'active dot' : 'dot'}></li>
          ))}
          <li className='slider-btn mx-3'>
            <a className="carousel-control" href="#myCarousel" role="button" data-bs-slide="next">
              <FontAwesomeIcon icon={faRightLong} />
              <span className="visually-hidden">Next</span>
            </a>
          </li>
        </ol>
      </div>
    </>
  )
}

export default HeroSlider;
