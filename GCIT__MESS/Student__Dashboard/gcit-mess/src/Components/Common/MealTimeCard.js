import React from 'react';
import './MealTimeCard.css';

const MealTimeCard = ({ timing_start, timing_end, time, bgImg }) => {
  const cardStyle = {
    backgroundImage: bgImg,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    objectFit: 'cover',
    objectPosition: 'center',
  };

  return (
    <div className="col-md-4">
      <div className="card" style={cardStyle}>
        <div className="card-body">
          <div>
            <h5 className="card-title">
              <span>{timing_start}</span> To <span>{timing_end}</span>
            </h5>
            <h5>{time}</h5>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MealTimeCard;
