import React from 'react'
import './Heading.css'
const Heading = ({title}) => {
  return (
    <>
    <h3 className='Heading'>{title}</h3>
    </>
  )
}

export default Heading