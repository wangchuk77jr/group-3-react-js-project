import signUpImage from './SignUp.png';
import React, { useState } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import { useHistory } from 'react-router-dom';
import 'react-toastify/dist/ReactToastify.css';
import './login.css';

const API_URL = 'http://localhost:5000';

const SignUp = () => {
  const history = useHistory();
  const [username, setUsername] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');

  const handleRegister = async (e) => {
    e.preventDefault();

    if (password !== confirmPassword) {
      toast.error('Passwords do not match');
      return;
    }

    try {
      const response = await axios.post(`${API_URL}/auth/register`, {
        username,
        email,
        password,
      });
      toast.success(response.data.message);
      history.push('/login?message=registered'); // Redirect to login page with message
    } catch (error) {
      toast.error(error.response?.data?.message || 'Registration failed');
    }
  };

  return (
    <div className="container px-md-5 mt-2">
      <div className="row mb-5" style={{ backgroundColor: '#fff',borderRadius:'10px' }}>
        <div className="col-md-6">
          <div className="p-5">
            <h4
              style={{
                textTransform: 'uppercase',
                fontWeight: '900',
                borderBottom: '2px solid orange',
                paddingBottom: '10px',
                paddingTop: '5px',
              }}
            >
              Sign up to GCIT Mess
            </h4>
            <form onSubmit={handleRegister}>
              <div className="input-container mt-3">
                <label htmlFor="">Your Name:</label>
                <input
                  required
                  type="text"
                  value={username}
                  onChange={(e) => setUsername(e.target.value)}
                />
              </div>
              <div className="input-container mt-3">
                <label htmlFor="">Email:</label>
                <input
                  required
                  type="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
              <div className="input-container mt-3">
                <label htmlFor="">Password:</label>
                <input
                  required
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
              <div className="input-container mt-3">
                <label htmlFor="">Confirm Password:</label>
                <input
                  required
                  type="password"
                  value={confirmPassword}
                  onChange={(e) => setConfirmPassword(e.target.value)}
                />
              </div>
              <div className="mt-3 text-center">
                <button
                  style={{
                    width: '60%',
                    height: '40px',
                    borderRadius: '100px',
                    backgroundColor: '#FC9845',
                    border: 'none',
                    color: '#fff',
                    fontWeight: '900',
                  }}
                  type="submit"
                >
                  Sign Up
                </button>
              </div>
              <div className="text-center mt-3">
                <h6>
                  Already have an account?{' '}
                  <span>
                    <a href="/login">Sign In</a>
                  </span>
                </h6>
              </div>
            </form>
            <ToastContainer
            position="top-center"
            autoClose={false}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
            theme="colored"
            style={{width:'600px'}}
          />

          </div>
        </div>
        <div className="col-md-6">
          <img
            src={signUpImage}
            alt=""
            style={{
              width: '100%',
              height: '100%',
              objectFit: 'contain',
              objectPosition: 'center',
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default SignUp;
