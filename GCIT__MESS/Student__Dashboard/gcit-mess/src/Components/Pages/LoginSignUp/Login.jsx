import React, { useState, useEffect } from 'react';
import axios from 'axios';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import { Link, Redirect } from 'react-router-dom';
import './login.css';
import loginImage from './login.png';

const API_URL = 'http://localhost:5000';

const login = async (email, password) => {
  try {
    const response = await axios.post(`${API_URL}/auth/login`, { email, password });

  
    if (response.data.token) {
      sessionStorage.setItem("token", response.data.token);
      toast.success("Logged in Successfully");
    } else {
      sessionStorage.setItem("token", null);
  
    }
    sessionStorage.setItem("user_id", response.data.userId)
    sessionStorage.setItem('user_email', response.data.email)
    return response.data;
  } catch (error) {
    throw new Error(error.response.data.message);
  }
};

const Login = ({ isLoggedIn, onLoginSuccess }) => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [redirectToHome, setRedirectToHome] = useState(false);

  useEffect(() => {
    const params = new URLSearchParams(window.location.search);
    const registrationMessage = params.get('message');

    if (registrationMessage === 'registered') {
      toast.success('Successfully registered');
    }

    if (isLoggedIn) {
      setRedirectToHome(true);
    }
  }, [isLoggedIn]);

  const handleLogin = async (e) => {
    e.preventDefault();

    try {
      const response = await login(email, password);
      toast.success(response.message);

      // Call the onLoginSuccess callback function
      onLoginSuccess();

      // Set redirectToHome to true to trigger the redirect
      setRedirectToHome(true);
    } catch (error) {
      toast.error(error.message);
    }
  };

  if (redirectToHome) {
    return <Redirect to="/" />;
  }

  return (
    <div className="container px-md-5 mt-2">
      <div className="row mb-5" style={{ backgroundColor: '#fff', borderRadius: '10px' }}>
        <div className="col-md-6">
          <div className="p-5">
            <h4
              style={{
                textTransform: 'uppercase',
                fontWeight: '900',
                borderBottom: '2px solid orange',
                paddingBottom: '10px',
                paddingTop: '5px',
              }}
            >
              Login to GCIT Mess
            </h4>
            <form onSubmit={handleLogin}>
              <div className="input-container mt-4">
                <label htmlFor="">Email:</label>
                <input
                  required
                  type="email"
                  value={email}
                  onChange={(e) => setEmail(e.target.value)}
                />
              </div>
              <div className="input-container mt-3 mb-2">
                <label htmlFor="">Password:</label>
                <input
                  required
                  type="password"
                  value={password}
                  onChange={(e) => setPassword(e.target.value)}
                />
              </div>
              <div style={{ textAlign: 'right', marginTop: '10px' }}>
                <h6>
                  <a href="/">Forgot Password?</a>
                </h6>
              </div>
              <div style={{ textAlign: 'center', marginTop: '20px' }}>
                <button
                  style={{
                    width: '60%',
                    height: '40px',
                    borderRadius: '100px',
                    backgroundColor: '#FC9845',
                    border: 'none',
                    color: '#fff',
                    fontWeight: '900',
                  }}
                  type="submit"
                >
                  Login
                </button>
              </div>
              <div style={{ textAlign: 'center', marginTop: '30px' }}>
                <h6>
                  Don't have an account? <span><Link to="/signup">Sign Up</Link></span>
                </h6>
              </div>
            </form>
            <ToastContainer
              position="top-center"
              autoClose={5000}
              hideProgressBar={false}
              newestOnTop={false}
              closeOnClick
              rtl={false}
              pauseOnFocusLoss
              draggable
              pauseOnHover
              theme="colored"
            />
          </div>
        </div>
        <div className="col-md-6">
          <img
            src={loginImage}
            alt=""
            style={{
              width: '100%',
              height: '100%',
              objectFit: 'contain',
              objectPosition: 'center',
            }}
          />
        </div>
      </div>
    </div>
  );
};

export default Login;



// import React, { useState, useEffect } from 'react';
// import axios from 'axios';
// import { ToastContainer, toast } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';
// import './login.css';
// import loginImage from './login.png';

// const API_URL = 'http://localhost:5000';

// const login = async (email, password) => {
//   try {
//     const response = await axios.post(`${API_URL}/auth/login`, { email, password });
//     return response.data;
//   } catch (error) {
//     throw new Error(error.response.data.message);
//   }
// };

// const Login = () => {
//   const [email, setEmail] = useState('');
//   const [password, setPassword] = useState('');

//   useEffect(() => {
//     const params = new URLSearchParams(window.location.search);
//     const registrationMessage = params.get('message');

//     if (registrationMessage === 'registered') {
//       toast.success('Successfully registered');
//     }
//   }, []);

//   const handleLogin = async (e) => {
//     e.preventDefault();

//     try {
//       const response = await login(email, password);
//       toast.success(response.message);
//     } catch (error) {
//       toast.error(error.message);
//     }
//   };

//   return (
//     <div className="container px-md-5 mt-2">
//       <div className="row mb-5" style={{ backgroundColor: '#fff',borderRadius:'10px' }}>
//         <div className="col-md-6">
//           <div className="p-5">
//             <h4
//               style={{
//                 textTransform: 'uppercase',
//                 fontWeight: '900',
//                 borderBottom: '2px solid orange',
//                 paddingBottom: '10px',
//                 paddingTop: '5px',
//               }}
//             >
//               Login to GCIT Mess
//             </h4>
//             <form onSubmit={handleLogin}>
//               <div className="input-container mt-4">
//                 <label htmlFor="">Email:</label>
//                 <input
//                   required
//                   type="email"
//                   value={email}
//                   onChange={(e) => setEmail(e.target.value)}
//                 />
//               </div>
//               <div className="input-container mt-3 mb-2">
//                 <label htmlFor="">Password:</label>
//                 <input
//                   required
//                   type="password"
//                   value={password}
//                   onChange={(e) => setPassword(e.target.value)}
//                 />
//               </div>
//               <div style={{ textAlign: 'right', marginTop: '10px' }}>
//                 <h6>
//                   <a href="/">Forgot Password?</a>
//                 </h6>
//               </div>
//               <div style={{ textAlign: 'center', marginTop: '20px' }}>
//                 <button
//                   style={{
//                     width: '60%',
//                     height: '40px',
//                     borderRadius: '100px',
//                     backgroundColor: '#FC9845',
//                     border: 'none',
//                     color: '#fff',
//                     fontWeight: '900',
//                   }}
//                   type="submit"
//                 >
//                   Login
//                 </button>
//               </div>
//               <div style={{ textAlign: 'center', marginTop: '30px' }}>
//                 <h6>
//                   Don't have an account? <span><a href="/signup">Sign Up</a></span>
//                 </h6>
//               </div>
//             </form>
//             <ToastContainer
//               position="top-center"
//               autoClose={5000}
//               hideProgressBar={false}
//               newestOnTop={false}
//               closeOnClick
//               rtl={false}
//               pauseOnFocusLoss
//               draggable
//               pauseOnHover
//               theme="colored"
//               />
//           </div>
//         </div>
//         <div className="col-md-6">
//           <img
//             src={loginImage}
//             alt=""
//             style={{
//               width: '100%',
//               height: '100%',
//               objectFit: 'contain',
//               objectPosition: 'center',
//             }}
//           />
//         </div>
//       </div>
//     </div>
//   );
// };

// export default Login;

