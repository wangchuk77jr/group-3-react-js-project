import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PageHeading from '../../Common/Page Common/PageHeading'
import about from './about.jpg'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRightLong } from '@fortawesome/free-solid-svg-icons'

axios.defaults.baseURL = 'http://localhost:5000';

const About = () => {
  const [contacts, setContacts] = useState([]);
  const [cooks, setCooks] = useState([]);
  useEffect(() => {
    fetchCooks();
  }, []);

  const fetchCooks = async () => {
    try {
      const response = await axios.get('/cooks');
      setCooks(response.data);
    } catch (error) {
      alert.error('Error fetching cooks:', error);
    }
  };


  useEffect(() => {
    fetchContacts();
  }, []);

  const fetchContacts = async () => {
    try {
      const response = await axios.get('/api/contacts');
      setContacts(response.data);
    } catch (error) {
      console.error('Error fetching contacts:', error);
    }
  };
  return (
    <div className='container px-md-5'>
    <div className="row mt-4 mb-3">
     <div className="col-md-6">
       <PageHeading PageHeading='About gcit mess'/>
       <p className='pt-2' style={{width:'90%',lineHeight:'33px'}}>
         Our mission is to provide high-quality food
         and dining services to the students of 
         Gyalposhing College of Technology. 
         We strive to create a welcoming and 
         inclusive environment that promotes
         healthy eating habits and fosters 
         social interaction among our community 
         members.
       </p>
       <p className='mt-2' style={{width:'90%',lineHeight:'33px'}}>
       Our team of skilled cooks are dedicated 
       to preparing each dish with care and 
       attention to detail, and to continually 
       improving and expanding our 
       food quality.
       </p>
     </div>
     <div className="col-md-6">
       <img className='mt-2' src={about} alt='gcit-mess' style={{width:"100%" ,height:'370px',objectFit:'cover'}}/>
     </div>
    </div>
    <div className="row">
     <div className='py-3'> <PageHeading PageHeading='MEET OUR MESS COMMITTEE MEMBERS'/></div>
     {contacts.map((contact) => (
       <div className="col-md-3 mt-2">
         <div className='border' style={{borderRadius:'10px',background:'rgb(252,152,69)'}}>
            {contact.profile_image_url  && (
                          <img
                            className='p-0 m-0'
                            src={`http://localhost:5000/uploads/${encodeURIComponent(
                              contact.profile_image_url
                            )}`}
                            style={{width:'100%',height:'350px',objectFit:"cover"}}
                            alt="contact"
                          />
                  )}
            <p className='pt-2 ps-3 text-capitalize pb-0 mb-2' style={{color:'#eee',fontWeight:'500'}}><span className='pe-2'><FontAwesomeIcon icon={faArrowRightLong} color='#eee'/></span>{contact.name}</p>
         </div>
       </div>
        ))}
    </div>
    <div className="row mb-5 pb-5">
     <div className='py-3'> <PageHeading PageHeading='MEET OUR SKILLED COOKS'/></div>
     {cooks.map((cook) => (
       <div className="col-md-3 mt-2" key={cook.cook_id}> 
         <div className='border' style={{borderRadius:'10px',background:'rgb(252,152,69)'}}>
                          <img
                            className='p-0 m-0'
                            src={`http://localhost:5000/uploads/${cook.cooks_image_url}`}
                            style={{width:'100%',height:'350px',objectFit:"cover"}}
                            alt="contact"
                          />
            <p className='pt-2 ps-3 text-capitalize pb-0 mb-2' style={{color:'#eee',fontWeight:'500'}}><span className='pe-2'><FontAwesomeIcon icon={faArrowRightLong} color='#eee'/></span>{cook.name}</p>
         </div>
       </div>
        ))}
    </div>
 </div>
  )
}

export default About