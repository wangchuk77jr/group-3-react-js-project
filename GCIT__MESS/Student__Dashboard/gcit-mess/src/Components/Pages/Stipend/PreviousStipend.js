import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './Stipend.css'
const PreviousStipend = () => {
  const [stipends, setStipends] = useState([]);
  useEffect(() => {
    fetchStipends();
  }, []);
  const fetchStipends = async () => {
    try {
      const response = await axios.get('/stipend');
      const stipendsData = response.data;
  
      if (stipendsData.length > 1) {
        const otherStipends = stipendsData.slice(1);
        setStipends(otherStipends);
      } else {
        setStipends([]);
      }
    } catch (error) {
      console.error('Error fetching stipends:', error);
    }
  };
  const getFormattedMonth = (dateString) => {
    const date = new Date(dateString);
    const month = date.toLocaleString('default', { month: 'long' });
    const year = date.getFullYear();
    return `${month} ${year}`;
  };
  
  return (
    <div className='row mb-5'>
    <h6 className='text-uppercase' style={{ fontWeight: '900',}}>Previous Month</h6>
    {stipends.map((stipend, index) => (
      <div className='col-md-6' key={stipend.stipend_id}>
          <h6 className='text-uppercase mt-2' style={{ fontWeight: '500', color: '#00000' }}>{getFormattedMonth(stipend.month)}</h6>
        <div className='row mb-3'>
          <div className='col-md-6'>
            <div className='CurrentMonthCard'>
              <h1 style={{fontSize:'30px'}}>{stipend.money_used}</h1>
              <h6 style={{fontSize:'17px'}}>Total Money Used</h6>
            </div>
          </div>
          <div className='col-md-6'>
            <div className='CurrentMonthCard'>
              <h1 style={{fontSize:'30px'}}>{stipend.money_left}</h1>
              <h6 style={{fontSize:'17px'}}>Total Money left</h6>
            </div>
          </div>
        </div>
      </div>
    ))}
  </div>
  )
}

export default PreviousStipend