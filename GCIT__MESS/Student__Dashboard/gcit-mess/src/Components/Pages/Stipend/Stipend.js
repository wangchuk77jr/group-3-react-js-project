import React from 'react'
import PageHeading from '../../Common/Page Common/PageHeading'
import CurrentStipendCard from './CurrentStipendCard'
import PreviousStipend from './PreviousStipend'

const Stipend = () => {
  return (
    <div className='container px-md-5'>
    <div className='row'>
      <PageHeading PageHeading='This month Stipend'/>
      <p style={{width:'50%',paddingTop:'10px'}}>
      In order to provide transparency, the total stipend for a month 
      will be shown here, both spent and leftovers.
      </p>
    </div>
      <CurrentStipendCard/>
      <hr className='mt-5' style={{height:'10px',borderWidth:'3px',borderColor:'green'}}/>
      <PreviousStipend/>
  </div>
  )
}

export default Stipend