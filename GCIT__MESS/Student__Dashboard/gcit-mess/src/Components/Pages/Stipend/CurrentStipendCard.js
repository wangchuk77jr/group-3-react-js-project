import React, { useState, useEffect } from 'react';
import axios from 'axios';
import './Stipend.css'
const CurrentStipendCard = ({money,money_type}) => {
  const [stipends, setStipends] = useState([]);
  useEffect(() => {
    fetchStipends();
  }, []);

  const fetchStipends = async () => {
    try {
      const response = await axios.get('/stipend');
      const stipendsData = response.data;
  
      if (stipendsData.length > 0) {
        const firstStipend = stipendsData[0];
        setStipends([firstStipend]);
      } else {
        setStipends([]);
      }
    } catch (error) {
      console.error('Error fetching stipends:', error);
    }
  };
  const getFormattedMonth = (dateString) => {
    const date = new Date(dateString);
    const month = date.toLocaleString('default', { month: 'long' });
    const year = date.getFullYear();
    return `${month} ${year}`;
  };
  return (
    <div>
    {stipends.map((stipend) => (
      <div className='row' key={stipend.stipend_id}>
      <h6 className='text-uppercase mb-3' style={{fontWeight:'900',color:'#038C11'}}>Last Month ---{getFormattedMonth(stipend.month)}</h6>
        <div className='col-md-4'>
        <div className='CurrentMonthCard'>
          <h1>{stipend.money_used}</h1>
          <h6>Total Money Used</h6>
        </div>
        </div>
        <div className='col-md-4'>
        <div className='CurrentMonthCard'>
          <h1>{stipend.money_left}</h1>
          <h6>Total Money left</h6>
        </div>
        </div>
        <div className='col-md-8 mt-2'>
          <div style={{backgroundColor:'rgb(91,147,72)',border:'1px solid rgb(250, 246, 5)',borderRadius:'10px'}}>
            <p className='p-3' style={{color:'#fff',lineHeight:'25px'}}>The total amount of money each person will receive from the stipend for the month of {getFormattedMonth(stipend.month)} is <span style={{fontWeight:'900',}}>NU.{stipend.money_left/400}</span> </p>
          </div>
        </div>
      </div>
    ))}
  </div>
  )
}

export default CurrentStipendCard