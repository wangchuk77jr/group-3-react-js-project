import React from 'react'

const NotFound = () => {
  return (
    <div>
      <img width={1000} height={1000} src='https://img.freepik.com/free-vector/error-404-concept-landing-page_52683-10029.jpg?w=826&t=st=1683085224~exp=1683085824~hmac=07bf18bc21eaaa033b979e169b500085f99395485c433202bee461a41dc054b5' alt='not found'/>
    </div>
  )
}

export default NotFound