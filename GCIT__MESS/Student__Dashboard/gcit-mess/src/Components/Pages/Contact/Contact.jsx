import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PageHeading from '../../Common/Page Common/PageHeading';
import './contact.css';
import ContactCard from './ContactCard';

const Contact = () => {
  const [contacts, setContacts] = useState([]);

  useEffect(() => {
    fetchContacts();
  }, []);

  const fetchContacts = async () => {
    try {
      const response = await axios.get('/api/contacts');
      setContacts(response.data);
    } catch (error) {
      console.error('Error fetching contacts:', error);
    }
  };

  return (
    <div className='container px-md-5 mb-5 pb-5'>
      <div className="row text-center">
        <div className='text-center mt-3'>
          <PageHeading PageHeading='Get in touch with us' />
        </div>
        <div style={{display:'flex',flexWrap:'wrap',gap:"10px",justifyContent:"center",marginTop:'20px',marginBottom:"20px"}}>
          <div style={{width:"300px",height:'130px',background:"#FFFFFFFF",borderRadius:"10px",display:'flex',flexDirection:'column',justifyContent:"center",alignItems:'center',gap:"10px"}}>
          <i class="fa-solid fa-mobile" style={{fontSize:'40px'}}></i>
             <p><a style={{textDecoration:'none',color:'#000'}} href="tel:17337528">+975 77867005</a></p>
          </div>
          <div style={{width:"300px",height:'130px',background:"#FFFFFFFF",borderRadius:"10px",display:'flex',flexDirection:'column',justifyContent:"center",alignItems:'center',gap:"10px"}}>
          <i class="fa-solid fa-map-location-dot" style={{fontSize:'40px'}}></i>
            <p>Gyalposhing,Monger</p>
          </div>
          <div style={{width:"300px",height:'130px',background:"#FFFFFFFF",borderRadius:"10px",display:'flex',flexDirection:'column',justifyContent:"center",alignItems:'center',gap:"10px"}}>
          <i class="fa-solid fa-inbox" style={{fontSize:'40px'}}></i>
              <p> <a style={{textDecoration:'none',color:'#000'}} href="mailto: abc@example.com">gcitmess@rub.edu.bt</a></p>
          </div>

        </div>
      </div>
      <div className="row">
        <PageHeading PageHeading='Meet our coordinators:' />
      </div>
      <div className="row px-2 mt-2 mb-5">
        {contacts.map((contact) => (
          <ContactCard
            key={contact.id}
            profile_img={`http://localhost:5000/uploads/${encodeURIComponent(contact.profile_image_url)}`}
            pnumber={contact.phone_no}
            name={contact.name}
            role={contact.role}
          />
        ))}
      </div>
    </div>
  );
};

export default Contact;
