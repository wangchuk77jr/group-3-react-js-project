import React from 'react'
import './contact.css'

const ContactCard = ({profile_img,pnumber,role,name}) => {
  return (
    <>
   <div className='col-md-3 p-1'>
  <div className='item-card mt-1'>
    <div className='item-image'>
    <div style={{ width: '100%', height: '350px', overflow: 'hidden' }}>
          <img src={profile_img} alt='items-img' style={{ width: '100%', height: '100%', objectFit: 'cover',objectPosition:'center' }} />
        </div>
      <div className='item-details'>
        <p className='items-list pt-1'><span>Name:</span><span className='ps-2'>{name}</span></p>
        <p className='items-list pt-2'><span>Phone Number:</span><span className='ps-2'>{pnumber}</span></p>
        <p className='items-list pt-2'><span>Role:</span><span className='ps-2'>{role}</span></p>
      </div>
    </div>
  </div>
</div>

    </>
  )
}

export default ContactCard