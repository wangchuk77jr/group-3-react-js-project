import React, { useState, useEffect } from 'react';
import axios from 'axios';
import MealTimeCard from '../../Common/MealTimeCard';
import Heading from '../../Common/Heading';

const MealTime = () => {
  const [mealTimings, setMealTimings] = useState([]);

  const formatTime = (timeString) => {
    const time = new Date(`1970-01-01T${timeString}`);
    return time.toLocaleTimeString([], { hour: 'numeric', minute: '2-digit' });
  };

  useEffect(() => {
    fetchMealTimings();
  }, []);

  const fetchMealTimings = async () => {
    try {
      const response = await axios.get('/meal-timings');
      setMealTimings(response.data);
    } catch (error) {
      console.error('Error fetching meal timings:', error);
    }
  };

  const renderMealCards = () => {
    return mealTimings.map((meal) => {
      const bgImg = getBackgroundImage(meal.meal_choice);
      const timingStart = formatTime(meal.meal_time);
      const timingEnd = formatTime(meal.meal_time_end);

      return (
        <MealTimeCard
          key={meal.id}
          timing_start={timingStart}
          timing_end={timingEnd}
          time={meal.meal_choice}
          bgImg={bgImg}
        />
      );
    });
  };

  const getBackgroundImage = (mealChoice) => {
    switch (mealChoice) {
      case 'Breakfast':
        return 'url("https://img.freepik.com/free-photo/sunset-saucer-caffeine-liquid-hot_1203-5658.jpg?w=1380&t=st=1682024715~exp=1682025315~hmac=9b90e315ea3a9e68878bc707f5876b77cbc1ea4c7d6bf22dcaeec5ea4f65383a")';
      case 'Lunch':
        return 'url("https://img.freepik.com/free-photo/cloud-blue-sky_1232-3108.jpg?w=1380&t=st=1686776342~exp=1686776942~hmac=6e945f659a6206d1ba47015d09b75a93fc66125259ef7297233b0f5384461e23")';
      case 'Dinner':
        return 'url("https://img.freepik.com/free-photo/horizontal-view-sunrise-monument-valley-usa_268835-2845.jpg?w=1380&t=st=1682025796~exp=1682026396~hmac=7f32fbcdc8e3e12a91699d30129ce71c1faf3b1bbf5df1770f63b684b8a69aa2")';
      default:
        return '';
    }
  };

  return (
    <>
      <div className='container px-md-5'>
        <Heading title='Meal Timing' />
        <div className="row pt-3">
          {renderMealCards()}
        </div>
      </div>
    </>
  );
};

export default MealTime;
