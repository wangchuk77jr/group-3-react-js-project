import Heading from '../../Common/Heading'
import RuleCard from '../../Common/RuleCard'
import React, { useState, useEffect } from 'react';
import axios from 'axios';
axios.defaults.baseURL = 'http://localhost:5000';

const Rules = () => {
   // Fetch rules from the server
   const [rules, setRules] = useState([]);
   useEffect(() => {
     fetchRules();
   }, []);
 
   const fetchRules = async () => {
     try {
       const response = await axios.get('/rules');
       setRules(response.data);
     } catch (error) {
       console.error('Error fetching rules:', error);
     }
   };
  return (
    <>
        <div className='container px-md-5'>
          <Heading title ='RULES AND REGULATIONS'/>
          </div>
          <div className='container px-md-5 pt-3'>
            <div className='row gap-3 px-md-2'>
              {rules.map((rule,index)=>(
                  <RuleCard key={rule.rule_id} number= {Number(`${index}`)+1} ruleTitle={rule.rule_title} ruleContent={rule.rule_description}/>
              ))}
              </div>
        </div>
    </>
  )
}

export default Rules