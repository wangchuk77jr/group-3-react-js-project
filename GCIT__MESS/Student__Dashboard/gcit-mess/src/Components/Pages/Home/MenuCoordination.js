import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Heading from '../../Common/Heading'
import MenuCoordinationCard from '../../Common/MenuCoordinationCard'
import './MenuCoordination.css'

const MenuCoordination = () => {
  const [menuCoordinationList, setMenuCoordinationList] = useState([]);
  useEffect(() => {
    getMenuCoordinations();
  }, []);

  const getMenuCoordinations = async () => {
    try {
      const response = await axios.get('/menucoordination');
      setMenuCoordinationList(response.data);
    } catch (error) {
      alert.error('Error fetching menu coordinations:', error);
    }
  };
  return (
    <>
    <div className='container px-md-5'>
      <Heading title='Menu Coordination day'/>
        <div className='row pt-3'>
            <p  style={{width:'85%',lineHeight:'30px'}}>
        The Menu Coordination Day at GCIT's mess is a day of delicious 
        planning and organizing! The team in charge ensures a balanced and
        nutritious menu, taking into consideration dietary needs and preferences.
        They coordinate with suppliers to bring healthy and delicious food options
        that cater to the students' tastes. This day ensures that students have access
        to a variety of mouth-watering food options that meet their dietary needs and preferences.
            </p>
        </div>
        <div class="row card-container px-2">  
        {menuCoordinationList.map((coordination) => (
              <MenuCoordinationCard profile_img={`http://localhost:5000/uploads/${encodeURIComponent(coordination.profile_image_url)}`} day={coordination.captain_name} name={coordination.coordination_day} />
        ))}
        </div>
 
    </div>
    </>
  )
}

export default MenuCoordination