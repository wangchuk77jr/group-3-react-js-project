
import React, { useState } from 'react';
import PageHeading from '../../Common/Page Common/PageHeading'
import './Feedback.css'
import feedback from '../../../Assets/Images/Home/feedback.png'
import axios from 'axios';
import { toast,ToastContainer } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

const Feedback = () => {
  const [mealTime, setMealTime] = useState('');
  const [message, setMessage] = useState('');
  const [email, setEmail] = useState(sessionStorage.getItem('user_email'));
  const [feedbackCount, setFeedbackCount] = useState(0);
  const [feedbackData, setFeedbackData] = useState([]); // Array of existing feedback entries

  const currentDate = new Date().toLocaleString([], {
    dateStyle: 'short',
    timeStyle: 'short',
  });

  const handleSubmit = async (e) => {
    e.preventDefault();
    if (!email) {
      toast.error('You are not logged in. Please log in to submit feedback!');
      return;
    }

    // Check if the user has already submitted feedback for the selected meal time on the current date
    const existingFeedback = feedbackData.find(
      (feedback) =>
        feedback.email === email &&
        feedback.meal_time === mealTime &&
        new Date(feedback.date_time).toLocaleDateString() ===
          new Date().toLocaleDateString()
    );

    if (existingFeedback) {
      toast.error(`You have already submitted feedback for ${mealTime.toLowerCase()} today.`);
      return;
    }

    // Check if the user has already submitted three feedback entries on the current date
    const userFeedbackCount = feedbackData.filter(
      (feedback) =>
        feedback.email === email &&
        new Date(feedback.date_time).toLocaleDateString() ===
          new Date().toLocaleDateString()
    ).length;

    if (userFeedbackCount >= 3) {
      toast.error('You have already submitted feedback for three meals today.');
      return;
    }

    try {
      const response = await axios.post('http://localhost:5000/feedback', {
        meal_time: mealTime,
        date_time: currentDate,
        message: message,
        email: email,
      });

      if (response.status === 200) {
        console.log('Feedback submitted successfully');
        // Reset form fields
        setMealTime('');
        setMessage('');
        setFeedbackCount(feedbackCount + 1);
        setFeedbackData([
          ...feedbackData,
          {
            email: email,
            meal_time: mealTime,
            date_time: currentDate,
            message: message,
          },
        ]);
        toast.success('Feedback submitted successfully');
      } else {
        console.error('Failed to submit feedback');
        toast.error('Failed to submit feedback');
      }
    } catch (error) {
      console.error('Error submitting feedback:', error);
      toast.error('Error submitting feedback');
    }
  };

  return (
    <div className="container px-md-5 mt-4 mb-5">
      <div className="row">
        <p style={{ width: '50%' }}>
          Provide helpful and constructive feedback on your food experiences. Keep in mind to provide feedback according to
          your meal timing, and your feedback will be recorded in our system.
        </p>
      </div>
      <div className="row">
        <PageHeading PageHeading="Give Your Feedback" />
      </div>
      <div className="row px-md-0 mt-2" style={{ marginBottom: '100px' }}>
        <div className="col-md-7">
          <div>
            <form className="feedbackform" onSubmit={handleSubmit}>
              <div className="w-100">
                <select
                  className="w-100 input pe-2"
                  required
                  value={mealTime}
                  onChange={(e) => setMealTime(e.target.value)}
                >
                  <option value="">--- Select The Meal Time --- </option>
                  <option>Breakfast</option>
                  <option>Lunch</option>
                  <option>Dinner</option>
                </select>
              </div>
              <div className="w-100">
                <input
                  hidden
                  type="text"
                  required
                  className="w-100 input"
                  placeholder="Date and Time"
                  value={currentDate}
                  readOnly
                />
              </div>
              <div className="w-100">
                <input
                  hidden
                  type="text"
                  required
                  className="w-100 input"
                  placeholder="Email"
                  value={email}
                  readOnly
                />
              </div>
              <div className="w-100">
                <textarea
                  rows="8"
                  required
                  className="w-100 input"
                  placeholder="Your Message"
                  value={message}
                  onChange={(e) => setMessage(e.target.value)}
                />
              </div>
              <div className="w-50 mt-1">
                <input type="submit" value="Submit" className="w-100 submit-btn" />
              </div>
            </form>
          </div>
        </div>
        <div className="col-md-5">
          <img
            src={feedback}
            alt="feedback.png"
            style={{ width: '100%', height: '100%', objectFit: 'cover' }}
          />
        </div>
      </div>
      <ToastContainer
        position="top-center"
        autoClose={8000}
        hideProgressBar={false}
        newestOnTop={false}
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
        theme="colored"
        style={{ width: '500px' }}
      />
    </div>
  );
};

export default Feedback;









































// import React, { useState, useEffect } from 'react';
// import PageHeading from '../../Common/Page Common/PageHeading';
// import './Feedback.css';
// import feedback from '../../../Assets/Images/Home/feedback.png';
// import axios from 'axios';
// import { toast, ToastContainer } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';

// const Feedback = () => {
//   const [mealTime, setMealTime] = useState('');
//   const [message, setMessage] = useState('');
//   const [email, setEmail] = useState(sessionStorage.getItem('user_email'));
//   const [submittedMealTimes, setSubmittedMealTimes] = useState([]);
//   const [feedbackData, setFeedbackData] = useState([]);

//   const currentDate = new Date().toLocaleString([], {
//     dateStyle: 'short',
//     timeStyle: 'short',
//   });

//   useEffect(() => {
//     fetchFeedback();
//   }, []);

//   const fetchFeedback = async () => {
//     try {
//       const response = await axios.get('http://localhost:5000/feedback');
//       setFeedbackData(response.data);
//     } catch (error) {
//       console.error('Error retrieving feedback:', error);
//       toast.error('An error occurred while retrieving feedback.');
//     }
//   };

//   useEffect(() => {
//     fetchSubmittedMealTimes();
//   }, []);

//   const fetchSubmittedMealTimes = async () => {
//     try {
//       const response = await axios.get('http://localhost:5000/feedback', {
//         params: {
//           email: email,
//         },
//       });

//       if (response.status === 200) {
//         const mealTimes = response.data.map((feedback) => feedback.meal_time);
//         setSubmittedMealTimes(mealTimes);
//       } else {
//         console.error('Failed to fetch submitted meal times');
//       }
//     } catch (error) {
//       console.error('Error fetching submitted meal times:', error);
//     }
//   };

//   const handleSubmit = async (e) => {
//     e.preventDefault();
//     if (sessionStorage.getItem('token')) {
//       if (submittedMealTimes.length === 4) {
//         toast.error('You have reached the maximum limit of feedback submissions for the day.');
//         return;
//       }

//       if (submittedMealTimes.includes(mealTime)) {
//         toast.error(`You have already submitted feedback for ${mealTime}`);
//         return;
//       }

//       try {
//         const response = await axios.post('http://localhost:5000/feedback', {
//           meal_time: mealTime,
//           date_time: currentDate,
//           message: message,
//           email: email,
//         });

//         if (response.status === 200) {
//           console.log('Feedback submitted successfully');
//           // Reset form fields
//           setMealTime('');
//           setMessage('');
//           setSubmittedMealTimes([...submittedMealTimes, mealTime]);
//           toast.success('Feedback submitted successfully');
//         } else {
//           console.error('Failed to submit feedback');
//           toast.error('Failed to submit feedback');
//         }
//       } catch (error) {
//         console.error('Error submitting feedback:', error);
//         toast.error('Error submitting feedback');
//       }
//     } else {
//       toast.error('You are not logged in. Please log in to submit feedback!');
//     }
//   };

//   const renderMealTimeOptions = () => {
//     return (
//       <select
//         className="w-100 input pe-2"
//         required
//         value={mealTime}
//         onChange={(e) => setMealTime(e.target.value)}
//       >
//         <option value="">--- Select The Meal Time ---</option>
//         <option>Breakfast</option>
//         <option>Lunch</option>
//         <option>Dinner</option>
//       </select>
//     );
//   };

//   return (
//     <div className="container px-md-5 mt-4 mb-5">
//       <div className="row">
//         <p style={{ width: '50%' }}>
//           Provide helpful and constructive feedback on your food experiences. Keep in mind that you can provide feedback for each meal time only once per day.
//         </p>
//       </div>
//       <div className="row">
//         <PageHeading PageHeading="Give Your Feedback" />
//       </div>
//       <div className="row px-md-0 mt-2" style={{ marginBottom: '100px' }}>
//         <div className="col-md-7">
//           <div>
//             <form className="feedbackform" onSubmit={handleSubmit}>
//               <div className="w-100">{renderMealTimeOptions()}</div>
//               <div className="w-100">
//                 <input
//                   hidden
//                   type="text"
//                   required
//                   className="w-100 input"
//                   placeholder="Date and Time"
//                   value={currentDate}
//                   readOnly
//                 />
//               </div>
//               <div className="w-100">
//                 <input
//                   hidden
//                   type="text"
//                   required
//                   className="w-100 input"
//                   placeholder="Email"
//                   value={email}
//                   readOnly
//                 />
//               </div>
//               <div className="w-100">
//                 <textarea
//                   rows="8"
//                   required
//                   className="w-100 input"
//                   placeholder="Your Message"
//                   value={message}
//                   onChange={(e) => setMessage(e.target.value)}
//                 />
//               </div>
//               <div className="w-50 mt-1">
//                 <input type="submit" value="Submit" className="w-100 submit-btn" />
//               </div>
//             </form>
//           </div>
//         </div>
//         <div className="col-md-5">
//           <img
//             src={feedback}
//             alt="feedback.png"
//             style={{ width: '100%', height: '100%', objectFit: 'cover' }}
//           />
//         </div>
//       </div>
//       <ToastContainer
//         position="top-center"
//         autoClose={8000}
//         hideProgressBar={false}
//         newestOnTop={false}
//         closeOnClick
//         rtl={false}
//         pauseOnFocusLoss
//         draggable
//         pauseOnHover
//         theme="colored"
//         style={{ width: '500px' }}
//       />
//     </div>
//   );
// };

// export default Feedback;
























// import React, { useState } from 'react';
// import PageHeading from '../../Common/Page Common/PageHeading'
// import './Feedback.css'
// import feedback from '../../../Assets/Images/Home/feedback.png'
// import axios from 'axios';
// import { toast,ToastContainer } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';

// const Feedback = () => {
//   const [mealTime, setMealTime] = useState('');
//   const [message, setMessage] = useState('');
//   const [email, setEmail] = useState(sessionStorage.getItem('user_email'))

//   const currentDate = new Date().toLocaleString([], {
//     dateStyle: 'short',
//     timeStyle: 'short',
//   });

//   const handleSubmit = async (e) => {
//     e.preventDefault();
//     if(sessionStorage.getItem('token')){
//       try {

//         const response = await axios.post('http://localhost:5000/feedback', {
//           meal_time: mealTime,
//           date_time: currentDate,
//           message: message,
//           email:email,
//         });
  
//         if (response.status === 200) {
//           console.log('Feedback submitted successfully');
//           // Reset form fields
//           setMealTime('');
//           setMessage('');
//           toast.success('Feedback submitted successfully');
//         } else {
//           console.error('Failed to submit feedback');
//           toast.error('Failed to submit feedback');
//         }
//       } catch (error) {
//         console.error('Error submitting feedback:', error);
//         toast.error('Error submitting feedback');
//       }
//     }else{
//       toast.error("You are not logged in. Please log in to submit feedback!");
//     }
    
//   };

//   return (
//     <div className="container px-md-5 mt-4 mb-5">
//       <div className="row">
//         <p style={{ width: '50%' }}>
//           Provide helpful and constructive feedback on your food experiences.
//           Keep on note that provide the feedback according to your meal timing
//           and your feedback will be recorded in our system.
//         </p>
//       </div>
//       <div className="row">
//         <PageHeading PageHeading="Give Your Feedback" />
//       </div>
//       <div
//         className="row px-md-0 mt-2"
//         style={{ marginBottom: '100px' }}
//       >
//         <div className="col-md-7">
//           <div>
//             <form className="feedbackform" onSubmit={handleSubmit}>
//               <div className="w-100">
//                 <select
//                   className="w-100 input pe-2"
//                   required
//                   value={mealTime}
//                   onChange={(e) => setMealTime(e.target.value)}
//                 >
//                   <option value="">--- Select The Meal Time --- </option>
//                   <option>Breakfast</option>
//                   <option>Lunch</option>
//                   <option>Dinner</option>
//                 </select>
//               </div>
//               <div className="w-100">
//                 <input
//                   hidden
//                   type="text"
//                   required
//                   className="w-100 input"
//                   placeholder="Date and Time"
//                   value={currentDate}
//                   readOnly
//                 />
//               </div>
//               <div className="w-100">
//                 <input
//                   hidden
//                   type="text"
//                   required
//                   className="w-100 input"
//                   placeholder="Email"
//                   value={email}
//                   readOnly
//                 />
//               </div>
//               <div className="w-100">
//                 <textarea
//                   rows="8"
//                   required
//                   className="w-100 input"
//                   placeholder="Your Message"
//                   value={message}
//                   onChange={(e) => setMessage(e.target.value)}
//                 />
//               </div>
//               <div className="w-50 mt-1">
//                 <input
//                   type="submit"
//                   value="Submit"
//                   className="w-100 submit-btn"
//                 />
//               </div>
//             </form>
//           </div>
//         </div>
//         <div className="col-md-5">
//           <img
//             src={feedback}
//             alt="feedback.png"
//             style={{ width: '100%', height: '100%', objectFit: 'cover' }}
//           />
//         </div>
//       </div>
//       <ToastContainer
//               position="top-center"
//               autoClose={8000}
//               hideProgressBar={false}
//               newestOnTop={false}
//               closeOnClick
//               rtl={false}
//               pauseOnFocusLoss
//               draggable
//               pauseOnHover
//               theme="colored"
//               style={{width:'500px'}}
//               />
//     </div>
//   );
// };

// export default Feedback;



























// import React, { useState, useEffect } from 'react';
// import PageHeading from '../../Common/Page Common/PageHeading';
// import './Feedback.css';
// import feedback from '../../../Assets/Images/Home/feedback.png';
// import axios from 'axios';
// import { toast, ToastContainer } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';

// const Feedback = () => {
//   const [mealTime, setMealTime] = useState('');
//   const [message, setMessage] = useState('');
//   const [email, setEmail] = useState(sessionStorage.getItem('user_email'));
//   const [submittedMealTimes, setSubmittedMealTimes] = useState([]);
//   const [feedbackData, setFeedbackData] = useState([]);

//   const currentDate = new Date().toLocaleString([], {
//     dateStyle: 'short',
//     timeStyle: 'short',
//   });

//   useEffect(() => {
//     fetchFeedback();
//   }, []);

//   const fetchFeedback = async () => {
//     try {
//       const response = await axios.get('http://localhost:5000/feedback');
//       setFeedbackData(response.data);
//     } catch (error) {
//       console.error('Error retrieving feedback:', error);
//       toast.error('An error occurred while retrieving feedback.');
//     }
//   };

//   useEffect(() => {
//     fetchSubmittedMealTimes();
//   }, []);

//   const fetchSubmittedMealTimes = async () => {
//     try {
//       const response = await axios.get('http://localhost:5000/feedback', {
//         params: {
//           email: email,
//         },
//       });

//       if (response.status === 200) {
//         const mealTimes = response.data.map((feedback) => feedback.meal_time);
//         setSubmittedMealTimes(mealTimes);
//       } else {
//         console.error('Failed to fetch submitted meal times');
//       }
//     } catch (error) {
//       console.error('Error fetching submitted meal times:', error);
//     }
//   };

//   const handleSubmit = async (e) => {
//     e.preventDefault();
//     if (sessionStorage.getItem('token')) {
//       if (submittedMealTimes.includes(mealTime)) {
//         toast.error(`You have already submitted feedback for ${mealTime}`);
//         return;
//       }

//       try {
//         const response = await axios.post('http://localhost:5000/feedback', {
//           meal_time: mealTime,
//           date_time: currentDate,
//           message: message,
//           email: email,
//         });

//         if (response.status === 200) {
//           console.log('Feedback submitted successfully');
//           // Reset form fields
//           setMealTime('');
//           setMessage('');
//           setSubmittedMealTimes([...submittedMealTimes, mealTime]);
//           toast.success('Feedback submitted successfully');
//         } else {
//           console.error('Failed to submit feedback');
//           toast.error('Failed to submit feedback');
//         }
//       } catch (error) {
//         console.error('Error submitting feedback:', error);
//         toast.error('Error submitting feedback');
//       }
//     } else {
//       toast.error('You are not logged in. Please log in to submit feedback!');
//     }
//   };

//   return (
//     <div className="container px-md-5 mt-4 mb-5">
//       <div className="row">
//         <p style={{ width: '50%' }}>
//           Provide helpful and constructive feedback on your food experiences.
//           Keep in mind that you can provide feedback according to your meal
//           timing, and your feedback will be recorded in our system.
//         </p>
//       </div>
//       <div className="row">
//         <PageHeading PageHeading="Give Your Feedback" />
//       </div>
//       <div className="row px-md-0 mt-2" style={{ marginBottom: '100px' }}>
//         <div className="col-md-7">
//           <div>
//             <form className="feedbackform" onSubmit={handleSubmit}>
//               <div className="w-100">
//                 <select
//                   className="w-100 input pe-2"
//                   required
//                   value={mealTime}
//                   onChange={(e) => setMealTime(e.target.value)}
//                 >
//                   <option value="">--- Select The Meal Time ---</option>
//                   <option disabled={submittedMealTimes.includes('Breakfast')}>
//                     Breakfast
//                   </option>
//                   <option disabled={submittedMealTimes.includes('Lunch')}>
//                     Lunch
//                   </option>
//                   <option disabled={submittedMealTimes.includes('Dinner')}>
//                     Dinner
//                   </option>
//                 </select>
//               </div>
//               <div className="w-100">
//                 <input
//                   hidden
//                   type="text"
//                   required
//                   className="w-100 input"
//                   placeholder="Date and Time"
//                   value={currentDate}
//                   readOnly
//                 />
//               </div>
//               <div className="w-100">
//                 <input
//                   hidden
//                   type="text"
//                   required
//                   className="w-100 input"
//                   placeholder="Email"
//                   value={email}
//                   readOnly
//                 />
//               </div>
//               <div className="w-100">
//                 <textarea
//                   rows="8"
//                   required
//                   className="w-100 input"
//                   placeholder="Your Message"
//                   value={message}
//                   onChange={(e) => setMessage(e.target.value)}
//                 />
//               </div>
//               <div className="w-50 mt-1">
//                 <input
//                   type="submit"
//                   value="Submit"
//                   className="w-100 submit-btn"
//                 />
//               </div>
//             </form>
//           </div>
//         </div>
//         <div className="col-md-5">
//           <img
//             src={feedback}
//             alt="feedback.png"
//             style={{ width: '100%', height: '100%', objectFit: 'cover' }}
//           />
//         </div>
//       </div>
//       <ToastContainer
//         position="top-center"
//         autoClose={8000}
//         hideProgressBar={false}
//         newestOnTop={false}
//         closeOnClick
//         rtl={false}
//         pauseOnFocusLoss
//         draggable
//         pauseOnHover
//         theme="colored"
//         style={{ width: '500px' }}
//       />
//     </div>
//   );
// };

// export default Feedback;




// import React, { useState, useEffect } from 'react';
// import PageHeading from '../../Common/Page Common/PageHeading';
// import './Feedback.css';
// import feedback from '../../../Assets/Images/Home/feedback.png';
// import axios from 'axios';
// import { toast, ToastContainer } from 'react-toastify';
// import 'react-toastify/dist/ReactToastify.css';

// const Feedback = () => {
//   const [mealTime, setMealTime] = useState('');
//   const [message, setMessage] = useState('');
//   const [email, setEmail] = useState(sessionStorage.getItem('user_email'));
//   const [submittedMealTimes, setSubmittedMealTimes] = useState([]);
//   const [feedbackData, setFeedbackData] = useState([]);

//   const currentDate = new Date().toLocaleString([], {
//     dateStyle: 'short',
//     timeStyle: 'short',
//   });

//   useEffect(() => {
//     fetchFeedback();
//   }, []);

//   const fetchFeedback = async () => {
//     try {
//       const response = await axios.get('http://localhost:5000/feedback');
//       setFeedbackData(response.data);
//     } catch (error) {
//       console.error('Error retrieving feedback:', error);
//       toast.error('An error occurred while retrieving feedback.');
//     }
//   };

//   useEffect(() => {
//     fetchSubmittedMealTimes();
//   }, []);

//   const fetchSubmittedMealTimes = async () => {
//     try {
//       const response = await axios.get('http://localhost:5000/feedback', {
//         params: {
//           email: email,
//         },
//       });

//       if (response.status === 200) {
//         const mealTimes = response.data.map((feedback) => feedback.meal_time);
//         setSubmittedMealTimes(mealTimes);
//       } else {
//         console.error('Failed to fetch submitted meal times');
//       }
//     } catch (error) {
//       console.error('Error fetching submitted meal times:', error);
//     }
//   };

//   const handleSubmit = async (e) => {
//     e.preventDefault();
//     if (sessionStorage.getItem('token')) {
//       if (submittedMealTimes.length === 3) {
//         toast.error('You have reached the maximum limit of feedback submissions for the day.');
//         return;
//       }

//       if (submittedMealTimes.includes(mealTime)) {
//         toast.error(`You have already submitted feedback for ${mealTime}`);
//         return;
//       }

//       try {
//         const response = await axios.post('http://localhost:5000/feedback', {
//           meal_time: mealTime,
//           date_time: currentDate,
//           message: message,
//           email: email,
//         });

//         if (response.status === 200) {
//           console.log('Feedback submitted successfully');
//           // Reset form fields
//           setMealTime('');
//           setMessage('');
//           setSubmittedMealTimes([...submittedMealTimes, mealTime]);
//           toast.success('Feedback submitted successfully');
//         } else {
//           console.error('Failed to submit feedback');
//           toast.error('Failed to submit feedback');
//         }
//       } catch (error) {
//         console.error('Error submitting feedback:', error);
//         toast.error('Error submitting feedback');
//       }
//     } else {
//       toast.error('You are not logged in. Please log in to submit feedback!');
//     }
//   };

//   const renderMealTimeOptions = () => {
//     const disabledOptions = ['Breakfast', 'Lunch', 'Dinner'].filter((option) =>
//       submittedMealTimes.includes(option)
//     );

//     return (
//       <select
//         className="w-100 input pe-2"
//         required
//         value={mealTime}
//         onChange={(e) => setMealTime(e.target.value)}
//       >
//         <option value="">--- Select The Meal Time ---</option>
//         <option disabled={disabledOptions.includes('Breakfast')}>Breakfast</option>
//         <option disabled={disabledOptions.includes('Lunch')}>Lunch</option>
//         <option disabled={disabledOptions.includes('Dinner')}>Dinner</option>
//       </select>
//     );
//   };

//   return (
//     <div className="container px-md-5 mt-4 mb-5">
//       <div className="row">
//         <p style={{ width: '50%' }}>
//           Provide helpful and constructive feedback on your food experiences. Keep in mind that you can provide feedback for each meal time only once per day.
//         </p>
//       </div>
//       <div className="row">
//         <PageHeading PageHeading="Give Your Feedback" />
//       </div>
//       <div className="row px-md-0 mt-2" style={{ marginBottom: '100px' }}>
//         <div className="col-md-7">
//           <div>
//             <form className="feedbackform" onSubmit={handleSubmit}>
//               <div className="w-100">{renderMealTimeOptions()}</div>
//               <div className="w-100">
//                 <input
//                   hidden
//                   type="text"
//                   required
//                   className="w-100 input"
//                   placeholder="Date and Time"
//                   value={currentDate}
//                   readOnly
//                 />
//               </div>
//               <div className="w-100">
//                 <input
//                   hidden
//                   type="text"
//                   required
//                   className="w-100 input"
//                   placeholder="Email"
//                   value={email}
//                   readOnly
//                 />
//               </div>
//               <div className="w-100">
//                 <textarea
//                   rows="8"
//                   required
//                   className="w-100 input"
//                   placeholder="Your Message"
//                   value={message}
//                   onChange={(e) => setMessage(e.target.value)}
//                 />
//               </div>
//               <div className="w-50 mt-1">
//                 <input type="submit" value="Submit" className="w-100 submit-btn" />
//               </div>
//             </form>
//           </div>
//         </div>
//         <div className="col-md-5">
//           <img
//             src={feedback}
//             alt="feedback.png"
//             style={{ width: '100%', height: '100%', objectFit: 'cover' }}
//           />
//         </div>
//       </div>
//       <ToastContainer
//         position="top-center"
//         autoClose={8000}
//         hideProgressBar={false}
//         newestOnTop={false}
//         closeOnClick
//         rtl={false}
//         pauseOnFocusLoss
//         draggable
//         pauseOnHover
//         theme="colored"
//         style={{ width: '500px' }}
//       />
//     </div>
//   );
// };

// export default Feedback;





