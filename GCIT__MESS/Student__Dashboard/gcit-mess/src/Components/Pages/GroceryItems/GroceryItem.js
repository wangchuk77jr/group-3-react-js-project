import React, { useState, useEffect } from 'react';
import axios from 'axios';
import ItemsCard from './ItemsCard';
import PageHeading from '../../Common/Page Common/PageHeading';
import PageSubText from '../../Common/Page Common/PageSubText';

const GroceryItem = () => {
  const [items, setItems] = useState([]);

  useEffect(() => {
    // Fetch all items on component mount
    fetchItems();
  }, []);

  const fetchItems = async () => {
    try {
      const response = await axios.get('/itemInfo');
      setItems(response.data);
    } catch (error) {
      console.error('Error fetching items:', error);
    }
  };

  const SubText =
    "Every grocery item purchased for this month is available with its information. You can speak with the product's provider for more information to ensure openness.";

  const formatDateString = (dateString) => {
    const date = new Date(dateString);
    const options = { month: 'long', year: 'numeric' };
    return date.toLocaleDateString('en-US', options);
  };

  // Group items by month
  const itemsByMonth = items.reduce((acc, item) => {
    const month = formatDateString(item.date);
    if (!acc[month]) {
      acc[month] = [];
    }
    acc[month].push(item);
    return acc;
  }, {});

  const sortedMonths = Object.entries(itemsByMonth).sort(
    ([monthA], [monthB]) => new Date(monthB) - new Date(monthA)
  );

  const formatDateMonthString = (dateString) => {
    const date = new Date(dateString);
    return date.toLocaleDateString('en-US');
  };

  const calculateTotalMoneySum = (items) => {
    return items.reduce((sum, item) => sum + Number(item.amount*item.total_money), 0);
  };

  return (
    <div className='container px-md-5' style={{marginBottom:"150px"}}>
      <PageHeading PageHeading='Grocery Items' />
      <PageSubText PageSubText={SubText} />
      {sortedMonths.map(([month, items], index) => {
        const totalMoneySum = calculateTotalMoneySum(items);
        const isLatestMonth = index === 0; // Check if it is the latest month
        const isCurrentMonth = new Date(month) >= new Date().setDate(1); // Check if it is the current month
        return (
          <div key={month} className='mb-5 mt-2 '>
            <h6 className='text-capitalize' style={{
              fontWeight: (isLatestMonth || isCurrentMonth) ? 'bold' : 'bold',
              color: (isLatestMonth || isCurrentMonth) ? 'green' : 'inherit',
              borderBottom: (isLatestMonth || isCurrentMonth) ? '2px solid green' : '2px solid green',
              width: '400px',
              paddingBottom: '10px',
              fontSize:'18px'
            }}>
            {isLatestMonth ? (
        <span>Current Month: {month}</span>
      ) : (
        <span>Month: {month}</span>
      )}
            
            </h6>
            <div className='row'>
              {items.map((item) => (
                <ItemsCard
                  key={item.id}
                  item_img={`http://localhost:5000/uploads/${encodeURIComponent(
                    item.item_image_url
                  )}`}
                  Item_name={item.item_name}
                  Amount={item.amount}
                  Total_Money={item.total_money}
                  Supplier_Name={item.supplier_name}
                  Phone_No={item.phone_no}
                  date={formatDateMonthString(item.date)}
                  TotalMoney ={item.amount*item.total_money}
                />
              ))}
            </div>
            <div className='col-12 mt-3'>
              <div className='w-10 pb-0' style={{borderRadius:'10px',background:'rgb(255,127,24)',border:'1px solid green'}}>
                <p className='p-3 mb-0' style={{color:"#fff",fontWeight:'600'}}>The total amount of money used to buy grocery items for the month of <span style={{textDecoration:'underline',fontWeight:'900'}}>{month} is NU.{totalMoneySum}</span> </p>
              </div>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default GroceryItem;


