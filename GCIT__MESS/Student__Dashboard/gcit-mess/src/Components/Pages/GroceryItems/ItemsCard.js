import React from 'react'
import './GroceryItem.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faArrowRightLong } from '@fortawesome/free-solid-svg-icons'

const ItemsCard = ({item_img,Item_name,Amount,Total_Money,Supplier_Name,Phone_No,date,TotalMoney}) => {
  return (
    <>
        <div className='col-md-4 p-1'>
            <div className='item-card mt-2'>
                <img src={item_img} alt='items-img' style={{width:'100%',objectFit:'cover',height:'150px',borderTopLeftRadius:'10px',borderTopRightRadius:'10px'}}/>
                <div className='p-4' style={{display:'flex',flexDirection:'column',justifyContent:'center',alignItems:'flex-start'}}>
                    <p className='items-list'> <span><FontAwesomeIcon icon={faArrowRightLong} color='#000000c5'/></span><span className='ps-2'>Item name: {Item_name}</span></p>
                    <p className='items-list pt-1'> <span><FontAwesomeIcon icon={faArrowRightLong} color='#000000c5'/></span><span className='ps-2'>Amount: {Amount}kg</span></p>
                    <p className='items-list pt-1'> <span><FontAwesomeIcon icon={faArrowRightLong} color='#000000c5'/></span><span className='ps-2'>Price Per Kg: Nu.{Total_Money}</span></p>
                    <p className='items-list pt-1'> <span><FontAwesomeIcon icon={faArrowRightLong} color='#000000c5'/></span><span className='ps-2'>Supplier Name: {Supplier_Name}</span></p>
                    <p className='items-list pt-1'> <span><FontAwesomeIcon icon={faArrowRightLong} color='#000000c5'/></span><span className='ps-2'>Phone No: {Phone_No}</span></p>
                    <p className='items-list pt-1'> <span><FontAwesomeIcon icon={faArrowRightLong} color='#000000c5'/></span><span className='ps-2'>Date: {date}</span></p>
                    <p className='items-list pt-1'> <span><FontAwesomeIcon icon={faArrowRightLong} color='#000000c5'/></span><span className='ps-2'>Total Cost: <span style={{fontWeight:'700'}}>NU.{TotalMoney}</span> </span></p>    
                </div>
            </div>
        </div>
    </>
  )
}

export default ItemsCard