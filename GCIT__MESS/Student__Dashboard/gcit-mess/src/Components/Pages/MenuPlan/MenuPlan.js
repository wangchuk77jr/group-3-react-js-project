import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PageHeading from '../../Common/Page Common/PageHeading'
import PageSubText from '../../Common/Page Common/PageSubText'
const MenuPlan = () => {
  const [menuPlans, setMenuPlans] = useState([]);
  const Heading = 'MENU PLAN';
  const  text ="The GCIT mess will make an effort to fulfill the menu plan, but we must first check the availability of vegetables."
  useEffect(() => {
    fetchMenuPlans();
  }, []);

  const fetchMenuPlans = async () => {
    try {
      const response = await axios.get('/menuplan');
      setMenuPlans(response.data);
    } catch (error) {
      alert.error('Error fetching menu plans');
    }
  };

  return (
    <div className='container vh-100 px-md-5 mb-5'>
      <PageHeading PageHeading={Heading}/>
      <PageSubText PageSubText ={text}/>
        <table class="table mt-3 text-center table-bordered" style={{borderColor:'#903604'}}>
            <thead>
              <tr style={{backgroundColor:'#FE8642'}}>
                <th className='text-uppercase text-white' scope="col">Day</th>
                <th className='text-uppercase text-white' scope="col">Break Fast</th>
                <th className='text-uppercase text-white' scope="col">Lunch</th>
                <th className='text-uppercase text-white' scope="col">Dinner</th>
              </tr>
            </thead>
            <tbody>
             
            {menuPlans.map((plan) => (
                  <tr key={plan.menuplan_id}>
                    <td>{plan.menu_day}</td>
                    <td>{plan.menu_bf}</td>
                    <td>{plan.menu_lunch}</td>
                    <td>{plan.menu_dinner}</td>
                  </tr>
                ))}
            </tbody>
          </table>
    </div>
  )
}

export default MenuPlan