import React, { useState, useEffect } from 'react';
import axios from 'axios';
import PageHeading from '../../Common/Page Common/PageHeading'
import CleaningBox from './CleaningBox'
import './Cleaning.css'
axios.defaults.baseURL = 'http://localhost:5000';
const CleaningSchedule = () => {
  const [cleaningSchedule, setCleaningSchedule] = useState([]);
  useEffect(() => {
    fetchCleaningSchedule();
  }, []);

  const fetchCleaningSchedule = async () => {
    try {
      const response = await axios.get('/cleaning-schedule');
      setCleaningSchedule(response.data);
    } catch (error) {
      console.error('Error fetching cleaning schedule:', error);
    }
  };
  const formatDateString = (dateString) => {
    const date = new Date(dateString);
    return date.toLocaleDateString('en-US');
  };
  return (
    <div className='container px-md-5 vh-100'>
      <div className='row'>
          <PageHeading PageHeading='DINNING CLEANING SCHEDULE'/>
          <p style={{width:"50%",paddingTop:'10px',lineHeight:'30px'}}>
          Be sure to clean the dining hall once a week in accordance with the dining 
          cleaning schedule. The dining hall will need to be cleaned twice as a 
          punishment for everyone who doesn't clean.
          </p>
      </div>
      <div className='row'>
      {cleaningSchedule.map((row) => (
          <CleaningBox key={row.s_id} time={row.status} date ={formatDateString(row.date)} Class={row.course} year={row.year}/>
        
        ))}
      
      
      </div>
    </div>
  )
}

export default CleaningSchedule