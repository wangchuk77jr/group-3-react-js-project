import React from 'react'
import './Cleaning.css'
import downArrow from '../../../Assets/down-arrow.png'
const CleaningBox = ({time,date,Class,year}) => {
  return (
    <div className='col-md-4'>
      <div className='cleaning-container'>
               <div className='cleaning-box mb-0 pb-0'>
                  <h6 className='text-center text-uppercase pt-2 text-white'>{time}</h6>
               </div>
                 <img className='arrow-img' src={downArrow} alt='down-arrow'/>
                <div id='content'>
                  <h6 className='pt-2'>Date: <span style={{fontWeight:'900'}}>{date}</span></h6>
                  <h6>Class: <span style={{fontWeight:'900'}}>{Class}</span></h6>
                  <h6>Year: <span style={{fontWeight:'900'}}>{year}</span></h6>
                </div>
            </div>
    </div>
  )
}

export default CleaningBox