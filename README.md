<H1>GCIT MESS MANAGEMENT SYSTEM</H1>

<P>
GCIT (Gyalposhing College of Information Technology) is located in Mongar, and offers undergraduate courses in technology and computer science. Like many educational institutions, the college has a mess facility that provides food to students.
In the current situation, managing the mess operations at GCIT was a cumbersome task that required significant manual effort. The mess committee had to maintain records of daily menu planning, inventory management, and billing, which often resulted in errors and inaccuracies. Additionally, managing payments from a large number of students and staff was a challenging and time-consuming task.
To address these challenges, we decided to implement a mess management system web application. The system was designed to automate many of the mess operations and provide real-time updates to students and management. The new system allowed the mess committee to plan menus easily, manage inventory, and generate bills automatically, streamlining mess operations and reducing errors.

</p>
<h1>Aim:</h1>
<p>
To build a responsive web application that allows students of GCIT  college to see what is for a meal beforehand and stay informed about other mess-related issues.
</p>
<h1>Technology Used:</h1>
<ul>
    <h4>Front-End:</h4>
    <li>React</li>
    <li>Boostrap</li>
    <li>JavaScript</li>
    <li>Boostrap</li>
    <li>HTML5 & CSS3</li>
</ul>
<ul>
    <h4>Back-End:</h4>
    <li>Node Js</li>
    <li>Express JS</li>
    <li>Postgres SQL as Database</li>
    <li>Java Script</li>
</ul>


<h1>Deployement:</h1>
<ul>
    <h4>Front-End:</h4>
    <li>Front-end Is deploed in render</li>
    <a>link----</a>
</ul>
<ul>
    <h4>Back-End:</h4>
    <li>Back-end Is deploed in render</li>
    <a>Link---</a>
    
</ul>

